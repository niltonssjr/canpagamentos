<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => ['auth:api'],'namespace'=>'admin\Api'],function(){

	$this->get('/user',function(Request $request){return json_encode(auth()->user());});
	$this->get("/faturas","ApiFaturaController@index")->name('api.fatura.list');
	$this->get("/faturas/{id}","ApiFaturaController@show")->name('api.fatura.detail');
	$this->post("/faturas","ApiFaturaController@insert")->name('api.fatura.insert');
	$this->delete("/faturas/{id}","ApiFaturaController@cancel")->name('api.fatura.cancel');
	$this->get("/clientes","ApiClienteController@index")->name('apli.cliente.list');
	$this->get("/clientes/{id}","ApiClienteController@show")->name('apli.cliente.detail');
	$this->post("/clientes","ApiClienteController@insert")->name('apli.cliente.insert');
	$this->put("/clientes/{id}","ApiClienteController@update")->name('apli.cliente.update');
	$this->delete("/clientes/{id}","ApiClienteController@delete")->name('apli.cliente.update');

});

Route::get('/bazinga',function(Request $request){
	return json_encode($request);
});
