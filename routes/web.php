<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()
    			->route('dashboard');
});

Auth::routes();

Route::get('/home', 'Admin\Can\GeralController@dashboard')->name('home');

Route::group(['middleware' => ['auth'],'namespace'=>'admin','prefix'=>'admin'],function(){
	$this->get('/undercon','UnderconController@index')->name('undercon');
	$this->get('/indisponivel','Can\GeralController@indisponivel')->name('indisponivel');
	$this->get('/can/faturas','Can\FaturasController@index')->name('can.faturas');
	$this->get('/can/fatura/{id}','Can\FaturasController@faturaDetalhe')->name('can.faturaDetalhe');
	$this->get('/can/faturaCancel/{id}','Can\FaturasController@faturaCancel')->name('can.faturaCancel');
	$this->get('/can/customers','Can\CustomerController@listar')->name('can.customers.listar');
	$this->get('/cus/faturas','Can\FaturasController@index')->name('cus.faturas');
	$this->get('/cus/novafatura','Can\FaturasController@novaFatura')->name('cus.novafatura');
	$this->post('/cus/novafatura','Can\FaturasController@novaFaturaStore')->name('cus.novafatura.store')->middleware('csrf');
	$this->get('/cus/clientes','Can\ClientesController@index')->name('cus.clientes');
	$this->get('/cus/cliente/{id}','Can\ClientesController@clienteDetalhe')->name('cus.clienteDetalhe');
	$this->get('/cus/clienteId/{id}','Can\ClientesController@clientePorId')->name('cus.clienteId');
	$this->get('/cus/clienteDelete/{id}','Can\ClientesController@clienteDeletar')->name('cus.clienteDelete');
	$this->get('/cus/novocliente','Can\ClientesController@clienteNovo')->name('cus.cliente.novo');
	$this->post('/cus/novocliente','Can\ClientesController@clienteNovoStore')->name('cus.cliente.novo.store')->middleware('csrf');
	$this->get('/cus/atualizacliente/{id}','Can\ClientesController@clienteAtualiza')->name('cus.clienteAtualiza');
	$this->post('/cus/atualizacliente','Can\ClientesController@clienteAtualizaStore')->name('cus.clienteAtualiza.store')->middleware('csrf');
	$this->get('/dashboard','Can\GeralController@dashboard')->name('dashboard');
	$this->get('/withdraw','Can\GeralController@withdraw')->name('withdraw');
	$this->post('/withdraw','Can\GeralController@withdrawStore')->name('withdraw.store')->middleware('csrf');
	$this->get('/relatoriofaturas','Can\FaturasController@relatorioFaturas')->name('can.faturas.report');
	$this->post('/relatoriofaturas','Can\FaturasController@relatorioFaturas')->name('can.faturas.report.filter')->middleware('csrf');
	$this->get('/relatoriofinanceiro','Can\GeralController@relatorioFinanceiro')->name('can.relatoriofinanceiro');
	$this->get('/transferencias','Can\GeralController@transferencias')->name('transferencias');
	$this->get('/cus/clienteEmail/{email}','Can\ClientesController@clienteEmail')->name('cus.clienteEmail');
	$this->get('/cus/pesquisaCliente/{argumento}','Can\ClientesController@pesquisaCliente')->name('cus.pesquisaCliente');
	$this->get('/can/subcontanova','Can\SubContasController@subContaNova')->name('can.subcontanova');
	$this->post('/can/subcontanova','Can\SubContasController@subContaNovaStore')->name('can.subcontanova.store')->middleware('csrf');
	$this->get('/can/subcontas','Can\SubContasController@index')->name('can.subcontas');
	$this->get('/can/subconta/detalhe/{id}','Can\SubContasController@subContaDetalhe')->name('can.subconta.detalhe');
	$this->get('/can/subconta/inativar/{id}','Can\SubContasController@subContaInativar')->name('can.subconta.inativar');
	$this->get('/can/subconta/validar/{id}','Can\SubContasController@validar')->name('can.subconta.validar');	
	$this->post('/can/subconta/validar/','Can\SubContasController@validarStore')->name('can.subconta.validar.store')->middleware('csrf');
	$this->get('/can/subconta/configurar/{id}','Can\SubContasController@configurar')->name('can.subconta.configurar');
	$this->post('/can/subconta/configurar','Can\SubContasController@configurarStore')->name('can.subconta.configurar.store')->middleware('csrf');
	$this->get('/config-gerais','Can\GeralController@configGeral')->name('can.config-geral');
	$this->post('/config-gerais','Can\GeralController@configGeralStore')->name('can.config-geral.store')->middleware('csrf');
	$this->get('/config-bancos','Can\GeralController@configBancos')->name('can.config-bancos');
	$this->post('/config-bancos','Can\GeralController@configBancosStore')->name('can.config-bancos.store')->middleware('csrf');
	$this->get('/password','Can\GeralController@password')->name('can.password');
	$this->post('/password','Can\GeralController@passwordStore')->name('can.password.store')->middleware('csrf');
	$this->get('/userapitoken','Can\ApiController@userApiToken')->name('can.userapitoken.show');
	$this->post('/userapitoken','Can\ApiController@userApiTokenGenerate')->name('can.userapitoken.generate')->middleware('csrf');
	$this->get('/adminapitoken/{id}','Can\ApiController@adminApiToken')->name('can.adminapitoken.show');	
});



