<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Rules\cnpjRule;
use App\Rules\cpfRule;
use App\Rules\uniqueEmailForFinalCustomer;

class clienteFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [
            "cus_customer_name.required" => 'Informe o nome do cliente',
            "cus_customer_email.required" => 'Informe o e-mail do cliente',
            "cus_customer_phone_prefix.required" => 'Informe o prefixo do telefone',
            "cus_customer_phone_number.required" => 'Informe o número do telefone do cliente',
            "cus_customer_cnpj.required_if" => 'Informe o CNPJ do cliente',
            "cus_customer_cpf.required_if" => 'Informe o CPF do cliente',
            "cus_customer_address_zip_code.required" => 'Informe o CEP do cliente',
            "cus_customer_address_street.required" => 'Informe o endereço do cliente',            
            "cus_customer_address_district.required" => 'Informe o bairro do cliente',
            "cus_customer_address_city.required" => 'Informe a cidade do cliente',
            "cus_customer_address_state.required" => 'Informe a UF do cliente'
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "cus_customer_name" => 'required',
            "cus_customer_email" => 'required',
            "cus_customer_phone_prefix" => 'required',
            "cus_customer_phone_number" => 'required',
            "cus_customer_cnpj" => ['required_if:cus_customer_person_type,jur',new cnpjRule],
            "cus_customer_cpf" => ['required_if:cus_customer_person_type,fis',new cpfRule],
            "cus_customer_address_zip_code" => 'required',
            "cus_customer_address_street" => 'required',            
            "cus_customer_address_district" => 'required',
            "cus_customer_address_city" => 'required',
            "cus_customer_address_state" => 'required'
        ];
    }
}

