<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Rules\bankAg;
use App\Rules\bankcc;

class BankAccountFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function messages(){
        return[
            'subconta_bank_ag.required'     => 'Informe a agência bancária do cliente',
            'subconta_bank_cc.required'     => 'Informe o número da conta do cliente'
        ];
    }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [            
            'subconta_bank_ag'  => ['required',new bankAg($this->subconta_bank)],
            'subconta_bank_cc'  => ['required',new bankcc($this->subconta_bank)]
        ];
    }
}
