<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Rules\cnpjRule;
use App\Rules\cpfRule;
use App\Rules\bankAg;
use App\Rules\bankcc;

class SubAcctValidationFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function messages(){
        return[
            'subconta_business_type.required'   => 'Informe uma descrição resumida do negócio do cliente',
            'subconta_CPF.required_if'  => 'Informe o CPF do cliente',
            'subconta_nome.required_if' => 'Informe o nome do cliente',
            'subconta_CNPJ.required_if' => 'Informe o CNPJ da empresa',
            'subconta_company_name.required_if' => 'Informe a razão social da empresa',
            'subconta_address_zip_code.required'    => 'Informe o CEP do cliente',
            'subconta_address_city.required' => 'Informe a cidade do cliente',
            'subconta_address_state.required'    => 'Informe a UF do cliente',
            'subconta_phone_number.required'     => 'Informe o telefone do cliente',
            'subconta_responsible_CPF.required_if' => 'Informe o CPF do responsável pela empresa',
            'subconta_responsible_name.required_if' => 'Informe o nome do responsável pela empresa',
            'subconta_bank_ag.required'     => 'Informe a agência bancária do cliente',
            'subconta_bank_cc.required'     => 'Informe o número da conta do cliente'
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [            
            'subconta_business_type' => 'required',
            'subconta_CPF'  => ['required_if:subconta_person_type,fis', new cpfRule],
            'subconta_nome' => 'required_if:subconta_person_type,fis',
            'subconta_CNPJ'  => ['required_if:subconta_person_type,jur',new cnpjRule],
            'subconta_company_name' => 'required_if:subconta_person_type,jur',
            'subconta_address_zip_code' => 'required',
            'subconta_address_city' => 'required',
            'subconta_address_state'    => 'required',
            'subconta_phone_number' => 'required',
            'subconta_responsible_CPF'  => ['required_if:subconta_person_type,jur',new cpfRule],
            'subconta_responsible_name' => 'required_if:subconta_person_type,jur',
            'subconta_bank_ag'  => ['required',new bankAg($this->subconta_bank)],
            'subconta_bank_cc'  => ['required',new bankcc($this->subconta_bank)]
        ];
    }
}
