<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Rules\cnpjRule;
use App\Rules\cpfRule;


class faturaFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function messages(){
        return [
            'cus_fatura_vencimento.required'               => 'Informe o vencimento da fatura',
            'cus_fatura_vencimento.after'                  => 'A data de vencimento deve ser futura',
            'cus_fatura_pagador_email.required'            => 'Informe o e-mail do pagador',
            'cus_fatura_pagador_email.email'               => 'Informe um e-mail válido',
            'cus_fatura_pagador_cnpj.required_if'          => 'Informe o CNPJ do pagador',
            'cus_fatura_pagador_cpf.required_if'           => 'Informe o CPF do pagador',
            'cus_fatura_pagador_nome.required'             => 'Informe o nome do pagador',
            'cus_fatura_pagador_address_street.required'   => 'Informe o endereço do pagador',
            'cus_fatura_pagador_address_number.required'   => 'Informe o número do endereço do pagador',
            'cus_fatura_pagador_address_city.required'     => 'Informe a cidade do pagador',
            'cus_fatura_pagador_address_state.required'    => 'Informe o estado do endereço do pagador',
            'cus_fatura_pagador_address_zip_code.required' => 'Informe o CEP do pagador'
        ];
    }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'cus_fatura_vencimento'             => 'required|date_format:"d/m/Y"|after:today',          
            'cus_fatura_pagador_email'          => 'required|email',
            'cus_fatura_pagador_nome'           => 'required',
            'cus_fatura_pagador_cnpj'           => ['required_if:cus_fatura_person_type,jur', new cnpjRule],
            'cus_fatura_pagador_cpf'           => ['required_if:cus_fatura_person_type,fis', new cpfRule],
            'cus_fatura_pagador_address_zip_code' => 'required',
            'cus_fatura_pagador_address_street' => 'required',
            'cus_fatura_pagador_address_number' => 'required',
            'cus_fatura_pagador_address_city'   => 'required',
            'cus_fatura_pagador_address_state'  => 'required'

        ];
    }
}
