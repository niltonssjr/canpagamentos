<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SubAccountConfigFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function messages(){
        return[
            'settings_notification_email.required_with' => 'Informe o e-mail que irá receber as informações sobre os pagamentos',
            'settings_commission_bank_slip_value.required' => 'Informe a taxa de comissão sobre os boletos',            
        ];
    }
    public function rules()
    {
        return [
            'settings_notification_email' => 'required_with:settings_receive_notification_emails',
            'settings_commission_bank_slip_value' => 'required'
        ];
    }
}
