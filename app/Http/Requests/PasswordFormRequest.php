<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PasswordFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function messages(){
        return [
            'pw_password.required' => 'Informe a nova senha',
            'pw_password.confirmed' => 'A confirmação de senha não confere com a senha informada',
            'pw_password_confirmation.required' => 'Informe a confirmação da nova senha'
        ];
    }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'pw_password' => 'required|confirmed',
            'pw_password_confirmation'  => 'required'
        ];
    }
}
