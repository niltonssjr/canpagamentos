<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Rules\cnpjRule;
use App\Rules\cpfRule;
use App\Rules\uniqueEmailForFinalCustomer;

class clienteApiFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [
            "customer_name.required" => 'Informe o nome do cliente',
            "customer_email.required" => 'Informe o e-mail do cliente',
            "customer_phone_prefix.required" => 'Informe o prefixo do telefone',
            "customer_phone_number.required" => 'Informe o número do telefone do cliente',
            "customer_person_type.required" => "Informe se o cliente é pessoa física ou jurídica",
            "customer_person_type.in" => "Informe (fis) para pessoa física e (jur) para pessoa jurídica",
            "customer_cnpj.required_if" => 'Informe o CNPJ do cliente',
            "customer_cpf.required_if" => 'Informe o CPF do cliente',
            "customer_address_zip_code.required" => 'Informe o CEP do cliente',
            "customer_address_street.required" => 'Informe o endereço do cliente',            
            "customer_address_number.required" => 'Informe o número do endereço do cliente',            
            "customer_address_district.required" => 'Informe o bairro do cliente',
            "customer_address_city.required" => 'Informe a cidade do cliente',
            "customer_address_state.required" => 'Informe a UF do cliente',
            "customer_phone_prefix.required_with" => "Informe o prefixo do número de telefone",
            "customer_phone_number.required_with" => "Informe o número de telefone do cliente"
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "customer_name" => 'required',
            "customer_email" => 'required',
            "customer_phone_prefix" => 'required',
            "customer_phone_number" => 'required',
            "customer_person_type" => "required|in:fis,jur",
            "customer_cnpj" => ['required_if:customer_person_type,jur',new cnpjRule],
            "customer_cpf" => ['required_if:customer_person_type,fis',new cpfRule],
            "customer_address_zip_code" => 'required',
            "customer_address_street" => 'required',  
            "customer_address_number" => 'required',
            "customer_address_district" => 'required',
            "customer_address_city" => 'required',
            "customer_address_state" => 'required',
            "customer_phone_prefix" => "required_with:customer_phone_number",
            "customer_phone_number" => "required_with:customer_phone_prefix"
        ];
    }
}

