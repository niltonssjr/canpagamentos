<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class novaSubContaFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'subconta_nome' => 'required',
            'subconta_email' => 'required|unique:users,email'
        ];
    }

    public function messages(){
        return [
            'subconta_nome.required' => 'Informe o nome da sub-conta',
            'subconta_email.required' => 'Informe o e-mail da sub-conta',
            'subconta_email.unique' => 'Este e-mail já está sendo utilizado'
        ];
    }
}
