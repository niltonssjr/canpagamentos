<?php

namespace App\Http\Controllers\Admin\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\StdClasses\IuguApi;
use App\StdClasses\StdFunctions;
use App\Rules\IuguDate;
use App\Rules\cnpjRule;
use App\Rules\cpfRule;

class ApiFaturaController extends Controller
{
    public function index(Request $request){    	

 		$std_messages = [
            'limite.numeric' => 'O limite informado não é válido',          
            'limite.gt' => 'O número de faturas pesquisado não pode ser zero',          
            'inicio.numeric' => 'O índice da primeira fatura é inválido',          
            'status.in' => 'O status escolhido para a seleção é inválido'
        ];

        $std_rules = [
            'limite' => 'nullable|numeric',          
            'inicio' => 'nullable|numeric',          
            'criada_desde' => ['nullable',new IuguDate],
            'criada_ate' => ['nullable',new IuguDate],
            'pagas_desde' => ['nullable',new IuguDate],
            'pagas_ate'  => ['nullable',new IuguDate],
            'status' => ['nullable','in:pendente,paga,cancelada,expirada']
        ];

        $request->validate($std_rules,$std_messages);    	

        $status_table = [
        	'pendente' => 'pending',
        	'paga' => 'paid',
        	'cancelada' => 'canceled',
        	'expirada' => 'expired'
        ];

        $parms = [];
        if (isset($request->limite)) $parms['limit'] = $request->limite;
        if (isset($request->inicio)) $parms['start'] = $request->inicio;
        if (isset($request->criada_desde)) $parms['created_at_from'] = $request->criada_desde;
        if (isset($request->criada_ate)) $parms['created_at_to'] = $request->criada_ate;
        if (isset($request->pagas_desde)) $parms['paid_at_from'] = $request->pagas_desde;
        if (isset($request->pagas_ate)) $parms['paid_at_to'] = $request->pagas_ate;
        if (isset($request->cliente_id)) $parms['customer_id'] = $request->cliente_id;
        if (isset($request->status)) $parms['status_filter'] = $status_table[$request->status];
        
        

    	$iugu = new IuguApi;
    	$faturas = $iugu->faturas($parms);      	  	    	

    	return response(json_encode($faturas),200)->header('Content-Type','application/json');
    }

    public function show($id=2){

        $iugu = new IuguApi;
        $fatura = $iugu->faturaDetalhe($id);

        if (isset($fatura->e404)){
            return response(json_encode(["erro"=>"fatura não encontrada"]),404)->header('Content-Type','application/json');
        }

        $fatura->status = StdFunctions::fatura_traduz_status($fatura->status);
        $fatura->payment_method = StdFunctions::fatura_traduz_payment_method($fatura->payment_method);
        unset($fatura->secure_url);
        unset($fatura->bank_slip->barcode);

        return response(json_encode($fatura),200)->header("Content-Type","application/json");

    }

    public function insert(Request $request){

        if (!StdFunctions::ApiIsActive()){
            return responsse(json_encode([['erro'=>'Não autorizado']]),403)->header("Content-Type",'application/json');
        }                

        $messages = [
            'fatura_vencimento.required'               => 'Informe o vencimento da fatura',
            'fatura_vencimento.after'                  => 'A data de vencimento deve ser futura',
            'fatura_pagador_email.required'            => 'Informe o e-mail do pagador',
            'fatura_pagador_email.email'               => 'Informe um e-mail válido',
            'fatura_pagador_cnpj.required_if'          => 'Informe o CNPJ do pagador',
            'fatura_pagador_cpf.required_if'           => 'Informe o CPF do pagador',
            'fatura_pagador_nome.required'             => 'Informe o nome do pagador',
            'fatura_person_type.required'               => 'Informe se o cliente é pessoa física ou jurídica',
            'fatura_person_type.in'                     => 'Informe (fis) para pessoa física ou (jur) para pessoa jurídica',
            'fatura_pagador_address_street.required'   => 'Informe o endereço do pagador',
            'fatura_pagador_address_number.required'   => 'Informe o número do endereço do pagador',
            'fatura_pagador_address_city.required'     => 'Informe a cidade do pagador',
            'fatura_pagador_address_state.required'    => 'Informe o estado do endereço do pagador',
            'fatura_pagador_address_zip_code.required' => 'Informe o CEP do pagador'
        ];

        $rules = [
            'fatura_vencimento'             => 'required|date_format:"d/m/Y"|after:today',          
            'fatura_pagador_email'          => 'required|email',
            'fatura_pagador_nome'           => 'required',
            'fatura_person_type'            =>  'required|in:"fis","jur"',
            'fatura_pagador_cnpj'           => ['required_if:fatura_person_type,jur', new cnpjRule],
            'fatura_pagador_cpf'           => ['required_if:fatura_person_type,fis', new cpfRule],
            'fatura_pagador_address_zip_code' => 'required',
            'fatura_pagador_address_street' => 'required',
            'fatura_pagador_address_number' => 'required',
            'fatura_pagador_address_city'   => 'required',
            'fatura_pagador_address_state'  => 'required'
        ];

        $request->validate($rules,$messages);

        //Envia os dados da request para um array, para envio par a API de inclusão
        $dados_fatura = [];
        $dados_request = $request->all();
        foreach($dados_request as $key => $value){
            $dados_fatura['cus_'.$key] = $value;
        }

        // return response(json_encode($request->all()))->header('Content-Type','application/json');

        //verifica quantos itens vieram na fatura
        if (!isset($request->fatura_itens) || !is_array($request->fatura_itens) || count($request->fatura_itens)==0){
            $retorno = [
                'message' => 'The given data was invalid',
                'errors' => ['fatura_itens'=>'Informe os itens que compoem a fatura']
            ];
            return response(json_encode($retorno),422)->header('Content-Type','application/json');
        }

        $num_items = count($request->fatura_itens);
        $items_errors = [];

        //prefixos padrão dos campos
        $field_desc = "cus_fatura_desc_";
        $field_qtd = "cus_fatura_qtd_";
        $field_valor = "cus_fatura_valor_";

        for ($ind=1;$ind<=$num_items;$ind++){

            $item = $request->fatura_itens[$ind-1];
            
            if (!isset($item['fatura_desc']) || $item['fatura_desc'] == "") $items_errors["item $ind - Descrição"]='Descrição do item não informada';

            if (!isset($item['fatura_qtd']) || $item['fatura_qtd'] == 0){
                $items_errors["item $ind - Quantidade"]='Quantidade do item não informada';                
            } else {
                if (!preg_match("/[0-9]/",$item['fatura_qtd'])) $items_errors["item $ind - Quantidade"]='Quantidade informada não é válida';    
            }
            if (!isset($item['fatura_valor'])){
                $items_errors["item $ind - Valor"]='Valor do item não informado';  
            } else {
                if (!preg_match("/^[0-9]+(\,[0-9]{1,2})?$/",$item['fatura_valor'])) $items_errors["item $ind - Valor"]='Valor do item não é válido';    
            }
            

            $dados_fatura[$field_desc.$ind] = (isset($item['fatura_desc']))? $item['fatura_desc'] : "";
            $dados_fatura[$field_qtd.$ind] = (isset($item['fatura_qtd']))? $item['fatura_qtd'] : "";
            $dados_fatura[$field_qtd.$ind] = preg_replace("/[^0-9]/","",$dados_fatura[$field_qtd.$ind]);
            $dados_fatura[$field_valor.$ind] = (isset($item['fatura_valor']))? $item['fatura_valor'] : "";

        }        

        if (count($items_errors)>0){
            $retorno = [
                'message' => 'The given data was invalid',
                'errors' => $items_errors
            ];
            return response(json_encode($retorno),422)->header('Content-Type','application/json');
        }        

        // return response(json_encode($dados_fatura))->header('Content-Type','application/json');
        $iugu = new IuguApi;
        $retorno_iugu = $iugu->faturaIncluir((object)$dados_fatura);

        if (is_array($retorno_iugu)){
            if (isset($retorno_iugu['erro'])){
                return response(json_encode($retorno_iugu),422)->header('Content-Type','application/json');                
            }
        }

        $retorno = [
            'fatura_id' => $retorno_iugu
        ];

        return response(json_encode($retorno),201)->header('Content-Type','application/json');
    }

    public function cancel($id=2){

        if (!StdFunctions::ApiIsActive()){
            return responsse(json_encode([['erro'=>'Não autorizado']]),403)->header("Content-Type",'application/json');
        }           

        $iugu = new IuguApi;
        $fatura = $iugu->faturaCancel($id);       
        
        if (isset($fatura->e404)){
            return response(json_encode(["erro"=>"fatura não encontrada"]),404)->header('Content-Type','application/json');
        }

        $fatura->status = StdFunctions::fatura_traduz_status($fatura->status);
        $fatura->payment_method = StdFunctions::fatura_traduz_payment_method($fatura->payment_method);
        unset($fatura->secure_url);
        unset($fatura->bank_slip->barcode);

        return response(json_encode($fatura),200)->header("Content-Type","application/json");

    }

}
