<?php

namespace App\Http\Controllers\Admin\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\StdClasses\IuguApi;
use App\StdClasses\StdFunctions;
use App\Rules\IuguDate;
use App\Rules\cnpjRule;
use App\Rules\cpfRule;
use App\Http\Requests\clienteApiFormRequest;
use App\Models\finalCustomer;

class ApiClienteController extends Controller
{

    public function index(Request $request){

 		$std_messages = [
            'limite.numeric' => 'O limite informado não é válido',                      
            'inicio.numeric' => 'O índice do primeiro cliente é inválido',                      
        ];

        $std_rules = [
            'limite' => 'nullable|numeric',          
            'inicio' => 'nullable|numeric',          
            'criado_desde' => ['nullable',new IuguDate],
            'criado_ate' => ['nullable',new IuguDate],
            'texto_geral' => 'nullable'
        ];

        $request->validate($std_rules,$std_messages);    	

        $parms = [];
        if (isset($request->limite)) $parms['limit'] = $request->limite;
        if (isset($request->inicio)) $parms['start'] = $request->inicio;
        if (isset($request->criado_desde)) $parms['created_at_from'] = $request->criada_desde;
        if (isset($request->criado_ate)) $parms['created_at_to'] = $request->criada_ate;
        if (isset($request->texto_geral)) $parms['query'] = $request->texto_geral;        

    	$iugu = new IuguApi;
    	$clientes = $iugu->clientes($parms);

    	return response(json_encode($clientes),200)->header('Content-Type','application/json');
    }

    public function show($id=2){

        $iugu = new IuguApi;
        $cliente = $iugu->clienteDetalhe($id);

        if (isset($cliente->e404)){
            return response(json_encode(["erro"=>"cliente não encontrado"]),404)->header('Content-Type','application/json');
        }

        return response(json_encode($cliente),200)->header("Content-Type","application/json");

    }

    public function insert(clienteApiFormRequest $request){

        if (!StdFunctions::ApiIsActive()){
            return response(json_encode([['erro'=>'Não autorizado']]),403)->header("Content-Type",'application/json');
        }    

        $parent_account_id = auth()->user()->customer()->first()->account_id;

        // $finalCustomer = finalCustomer::where([
        //             ['email','=',$request->customer_email],
        //             ['parent_account_id','=',$parent_account_id]
        //     ])->first();
        
        // //verifica se o e-mail já está em uso
        // if ($finalCustomer){
        //     $retorno = [
        //         'message' => 'The given data was invalid',
        //         'errors' => ['customer_email'=>'O e-mail já está em uso por outro cliente']
        //     ];
        //     return response(json_encode($retorno),422)->header('Content-Type','application/json');

        // }        

        $phone = preg_replace("/[^0-9]/","",$request->customer_phone_number);
        if (!preg_match("/[0-9]{8,9}/",$phone)){
            $retorno = [
                'message' => 'The given data was invalid',
                'errors' => ['customer_phone_number'=>'O número de telefone informado não é válido']
            ];
            return response(json_encode($retorno),422)->header('Content-Type','application/json');
        }

    	$cliente = [];
    	$cliente['email'] = $request->customer_email;
    	$cliente['name'] = $request->customer_name;
    	$cliente['phone'] = preg_replace("/[^0-9]/","",$request->customer_phone_number);
    	$cliente['phone_prefix'] = $request->customer_phone_prefix;
    	$cliente['cpf_cnpj'] = ($request->customer_person_type == 'fis')? $request->customer_cpf : $request->customer_cnpj;
    	$cliente['zip_code'] = $request->customer_address_zip_code;
    	$cliente['number'] = $request->customer_address_number;
    	$cliente['street'] = $request->customer_address_street;
    	$cliente['city'] = $request->customer_address_city;
    	$cliente['state'] = $request->customer_address_state;
    	$cliente['district'] = $request->customer_address_district;
    	$cliente['complement'] = $request->customer_address_complement;

    	$iugu = new IuguApi;
    	$cliente_id = $iugu->clienteNovoStore($cliente);

    	return response(json_encode(['cliente_id'=>$cliente_id]),201)->header("Content-Type","application/json");
    }

    public function update(clienteApiFormRequest $request,$id=2){

        if (!StdFunctions::ApiIsActive()){
            return responsse(json_encode([['erro'=>'Não autorizado']]),403)->header("Content-Type",'application/json');
        }       

        $parent_account_id = auth()->user()->customer()->first()->account_id;

        // $finalCustomer = finalCustomer::where([
        //             ['email','=',$request->customer_email],
        //             ['parent_account_id','=',$parent_account_id]
        //     ])->first();
        
        // //verifica se o e-mail já está em uso, e se estiver, verifica se é a mesma conta que está sendo atualizada
        // if ($finalCustomer){
        //     if ($finalCustomer->account_id != $id){
        //     $retorno = [
        //         'message' => 'The given data was invalid',
        //         'errors' => ['customer_email'=>'O e-mail já está em uso por outro cliente']
        //     ];
        //     return response(json_encode($retorno),422)->header('Content-Type','application/json');
        //     }
        // }        

        $phone = preg_replace("/[^0-9]/","",$request->customer_phone_number);
        if (!preg_match("/[0-9]{8,9}/",$phone)){
            $retorno = [
                'message' => 'The given data was invalid',
                'errors' => ['customer_phone_number'=>'O número de telefone informado não é válido']
            ];
            return response(json_encode($retorno),422)->header('Content-Type','application/json');
        }
        
        $cliente = [];
        $cliente['email'] = $request->customer_email;
        $cliente['name'] = $request->customer_name;
        $cliente['phone'] = preg_replace("/[^0-9]/","",$request->customer_phone_number);
        $cliente['phone_prefix'] = $request->customer_phone_prefix;
        $cliente['cpf_cnpj'] = ($request->customer_person_type == 'fis')? $request->customer_cpf : $request->customer_cnpj;
        $cliente['zip_code'] = $request->customer_address_zip_code;
        $cliente['number'] = $request->customer_address_number;
        $cliente['street'] = $request->customer_address_street;
        $cliente['city'] = $request->customer_address_city;
        $cliente['state'] = $request->customer_address_state;
        $cliente['district'] = $request->customer_address_district;
        $cliente['complement'] = $request->customer_address_complement;
        $cliente['account_id'] = $id;

        $iugu = new IuguApi;
        $cliente_id = $iugu->clienteAtualizaStore($cliente);

        if (isset($cliente->e404)){
            return response(json_encode(["erro"=>"cliente não encontrado"]),404)->header('Content-Type','application/json');
        }

        return response(json_encode(['cliente_id'=>$cliente_id]),200)->header("Content-Type","application/json");

    }

    public function delete($id=2){

        if (!StdFunctions::ApiIsActive()){
            return responsse(json_encode([['erro'=>'Não autorizado']]),403)->header("Content-Type",'application/json');
        }  

        $iugu = new IuguApi;
        $retorno = $iugu->clienteDeletar($id);              

        if (isset($retorno->e404)){
            return response(json_encode(["erro"=>"cliente não encontrado"]),404)->header('Content-Type','application/json');
        }

        if (isset($retorno->errors)){
            return response(json_encode(["erro"=>$retorno->errors]),422)->header('Content-Type','application/json');
        }
        
        return response(json_encode(["removido"=>"cliente excluído"]),200)->header('Content-Type','application/json');

    }        

}
