<?php

namespace App\Http\Controllers\Admin\Can;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\novaSubContaFormRequest;
use App\Http\Requests\SubAcctValidationFormRequest;
use App\Http\Requests\SubAccountConfigFormRequest;
use App\User;
use App\Models\Customer;
use App\StdClasses\IuguApi;
use Gate;
use App\StdClasses\StdFunctions;


class SubContasController extends Controller
{

	public function index(){

        StdFunctions::allowAdmin();

		$titulos = [
			'id',
			'Nome',
			'Status',
			'Ações'
		];

		$iugu = new IuguApi;
		$subcontas = $iugu->subcontas_listar();		
		
		return view ('admin.can.subContas',compact('subcontas','titulos'));

	}

    public function subContaNova(){

        StdFunctions::allowAdmin();
        
    	return view ('admin.can.subContaNova');

    }

    public function subContaNovaStore(novaSubContaFormRequest $request, User $user){

        StdFunctions::allowAdmin();
    	$user->insere($request);

    	return redirect()->route('can.subcontas');

    }

    public function subContaDetalhe($id){

        StdFunctions::allowAdmin();
        
    	$iugu = new IuguApi;
    	$dashboard = $iugu->dashboard($id);    	    	
	       
    	// dd($dashboard);
    	return view('admin.can.subContaDetalhe',compact('dashboard'));
    					
    }

    public function subContaInativar($id){

        StdFunctions::allowAdmin();                

        $customer = Customer::where('account_id',$id)->first();

        if (!$customer){
            $result = User::inativar_apenas_iugu($id);                        
            return redirect()->route('can.subcontas')->with(['error'=>$result->erro]);
        }

        $user = User::where('id',$customer->user_id)->first();

        $user->inativar();
        
        return redirect()->route('can.subcontas');
                        
    }

    public function validar($id){

        StdFunctions::allowAdmin();
    	return view('admin.can.subContaValidar',compact('id'));
    }

    public function validarStore(SubAcctValidationFormRequest $request){
    	
        StdFunctions::allowAdmin();
    	$request->session()->forget('error');
    	$request->session()->forget('success');

    	$iugu = new IuguApi;
    	$retorno = $iugu->subconta_validar($request);
    	
    	if (isset($retorno->erro)){
    		session(['error'=>$retorno->erro]);
    		return redirect()->back();
    	}

    	session(['success'=>'Verificação de conta enviada com sucesso']);
    	
    	return redirect()
    				->route('can.subcontas');

    }

    public function configurar($id){

        StdFunctions::allowAdmin();

        $iugu = new IuguApi;
        $dashboard = $iugu->dashboard($id);             

        $settings = (object)[
            'account_id' => $id,
            'auto_withdraw' => isset($dashboard->raw_auto_withdraw)? $dashboard->raw_auto_withdraw : false,
            'receive_notification_emails' => isset($dashboard->raw_payment_email_notification)? $dashboard->raw_payment_email_notification : false,
            'notification_email' => isset($dashboard->payment_email_notification_receiver)? $dashboard->payment_email_notification_receiver : null,
            'commission_cc_value' => isset($dashboard->credit_card_fee)? StdFunctions::valor_em_float($dashboard->credit_card_fee):0,
            'commission_cc_percentage' => 0,
            'commission_bank_slip_value' => isset($dashboard->bank_slip_fee)? StdFunctions::valor_em_float($dashboard->bank_slip_fee):0,
            'commission_bank_slip_percentage' => isset($dashboard->bank_slip_percent)? $dashboard->bank_slip_percent:0,
        ];

        return view ('admin.can.subContaConfigurar',compact('settings'));
    }

    public function configurarStore(SubAccountConfigFormRequest $Request){

        $config = [            
            'account_id' => $Request->settings_account_id,
            'auto_withdraw' => isset($Request->settings_auto_withdraw),
            'payment_email_notification' => isset($Request->settings_receive_notification_emails),
            'payment_email_notification_receiver' => isset($Request->settings_receive_notification_emails) ? $Request->settings_notification_email : "",
            'commissions' => [
                'cents' => 1                
            ],
        ];

        if (isset($Request->settings_commission_cc_value)) $config['commissions']['credit_card_cents'] = StdFunctions::valor_em_centavos($Request->settings_commission_cc_value);        
        
        if (isset($Request->settings_commission_bank_slip_value)) $config['commissions']['bank_slip_cents'] = StdFunctions::valor_em_centavos($Request->settings_commission_bank_slip_value);

        $iugu = new IuguApi;
        $iugu->general_settings_update($config);
        return redirect()->route('can.subcontas')->with(['success'=>'Configurações atualizadas']);

    }
}
