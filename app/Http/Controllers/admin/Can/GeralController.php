<?php

namespace App\Http\Controllers\Admin\Can;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\StdClasses\IuguApi;
use App\StdClasses\StdFunctions;
use App\Http\Requests\withdrawFormRequest;
use App\Http\Requests\BankAccountFormRequest;
use App\Http\Requests\PasswordFormRequest;
use App\User;

class GeralController extends Controller
{
    public function dashboard(){
    	$iugu = new IuguApi;
    	$dashboard = $iugu->dashboard();
    	return view ('admin.can.dashboard',compact('dashboard'));

    }

    public function withdraw(){

        StdFunctions::DenyInactive();

    	$iugu = new IuguApi;
    	$dashboard = $iugu->dashboard();
    	$balance = $dashboard->balance;

    	return view ('admin.can.withdraw',compact('balance'));
    }

    public function withdrawStore(withdrawFormRequest $request){

        StdFunctions::DenyInactive();

        $iugu = new IuguApi;
        $dashboard = $iugu->dashboard();
        
        $balance = StdFunctions::valor_em_float($dashboard->balance);
        $withdraw_request = StdFunctions::valor_em_float($request->withdraw_value);

        if ($withdraw_request > $balance){
            return redirect()
                        ->back()
                        ->with(['error'=>'O saldo é insuficiente para a operação']);            
        }
        
    	$iugu = new IuguApi;
    	$return = $iugu->withdraw($request);

    	if (!isset($return->erro)){
    		return redirect()
    					->route('withdraw')
    					->with(['success'=>'Solicitação de saque efetuada']);
    	}

    	return redirect()
    					->back()
    					->with(['error'=>'Solicitação de saque não efetuada']);
    }

    public function relatorioFinanceiro(){

        $iugu = new IuguApi;
        $linhas = $iugu->relatorioFinanceiro();

        $titulos = [
            'Data',            
            'Cliente',
            'Descrição',
            'Valor',
            'Saldo'
        ];

        return view ('admin.can.relFinanceiro',compact('titulos','linhas'));
    }

    public function transferencias(){

        $iugu = new IuguApi;
        $linhas = $iugu->transferencias();

        $titulos = [
            'Data',
            'Solicitante',
            'Valor',
            'Banco',
            'Agencia',
            'Conta',
            'Status',
        ];

        return view ('admin.can.relTransferencias',compact('titulos','linhas'));

    }

    public function configGeral(){

        $iugu = new IuguApi;
        $dashboard = $iugu->dashboard();
        
        $settings = (object)[
            'auto_withdraw' => $dashboard->raw_auto_withdraw,
            'receive_notification_emails' => $dashboard->raw_payment_email_notification,
            'notification_email' => $dashboard->payment_email_notification_receiver
        ];

        return view ('admin.can.configGerais',compact('settings'));
    }

    public function configGeralStore(Request $Request){
        
        StdFunctions::DenyInactive();

        $config = [
            'auto_withdraw' => isset($Request->config_gerais_saque),
            'payment_email_notification' => isset($Request->config_gerais_receber_emails),
            'payment_email_notification_receiver' => isset($Request->config_gerais_email) ? $Request->config_gerais_email : ""
        ];

        $iugu = new IuguApi;
        $iugu->general_settings_update($config);
        return redirect()->route('can.config-geral')->with(['success'=>'Configurações atualizadas']);

    }

    public function configBancos(){

        $iugu = new IuguApi;
        $dashboard = $iugu->dashboard();
        unset($iugu);

        $iugu = new IuguApi;
        $raw_changes = $iugu->bank_change_requests();

        $changes = [];

        foreach($raw_changes as $change){
            $line_change = [
                'data' => StdFunctions::format_datetime($change->created_at),
                'banco' => $change->bank,
                'agencia' => $change->agency,
                'conta' => $change->account,                
                'status' => StdFunctions::status_verif_account($change->status),
                'feedback' => $change->feedback,
            ];
            $changes[] = $line_change;
        }

        $config = (object)[
            'banco' => $dashboard->bank,
            'agencia' => $dashboard->bank_ag,
            'conta' => $dashboard->bank_cc,            
        ];

        $titulos = ['Data','Banco','Agência','Conta','Status','Observação'];
        $linhas = $changes;

        return view('admin.can.configBanco',compact('config','titulos','linhas'));

    }

    public function configBancosStore(BankAccountFormRequest $Request){
        
        StdFunctions::DenyInactive();

        $bank_data = (object)[
            'agency' => $Request->subconta_bank_ag,
            'account' => $Request->subconta_bank_cc,
            'account_type' => ($Request->subconta_bank_type =="Corrente")? "cc" : "cp",
            'bank' => $Request->subconta_bank
        ];
        
        $iugu = new IuguApi;
        $return = $iugu->bank_change($bank_data);

        if (isset($return->erro)){
            $error = $return->erro;
            if (isset($error->errors)) $error = $error->errors;
            return redirect()->back()->with(['error'=>$error]);
        }

        return redirect()->route('can.config-bancos')->with(['success'=>'Solicitação encaminhada']);
    }

    public function password(){
        $current_user = auth()->user()->name;
        return view ('admin.can.password',compact('current_user'));

    }

    public function passwordStore(PasswordFormRequest $Request){

        $user = auth()->user();        
        $return = $user->updatePassword($Request);
        
        if ($return){
            $status = ['success'=>'Senha alterada com sucesso'];
            return redirect()->route('can.password')->with($status);
        } else {
            $status = ['error'=>'A senha não pode ser atualizada'];
            return redirect()->back()->with($status);
        }

    }

    public function indisponivel(){
        
        return view ('admin.can.indisponivel');
    }

}
