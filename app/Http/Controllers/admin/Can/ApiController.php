<?php

namespace App\Http\Controllers\Admin\Can;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\StdClasses\StdFunctions;
use App\Models\Customer;
use DB;

class ApiController extends Controller
{

	//*
	//OBTENÇÃO E GRAVAÇÃO DO TOKEN
	//*

    public function userApiToken(){

    	$token = auth()->user()->paguecan_api_token;
    	$customer = auth()->user()->customer()->first();
    	$account_id = $customer->account_id;
    	$account_name = $customer->name;

    	return view ('admin.can.configUserApi',compact('token','account_id','account_name'));
    }

    public function userApiTokenGenerate(Request $request){

    	if (!$this->hasAuthToUpdate($request->config_api_account_id)) abort(404,'Usuário não autorizado');   		

    	$user = Customer::where('account_id',$request->config_api_account_id)->first()->user()->first();    	
      
    	//remove all older tokens
    	$old_tokens = $user->tokens;
    	foreach($old_tokens as $old_token){
    		$old_token->revoke();
    	}

    	$token = $user->createToken('paguecan_api')->accessToken;
    	$account_id = $request->account_id;
    	$user->paguecan_api_token = $token;
    	$user->save();    	
      $account_name = $user->account_name;  
      
    	
    	return view ('admin.can.configUserApi',compact('token','account_id','account_name'));
    }

   	public function adminApiToken($account){

   		if (!$this->hasAuthToUpdate($account)) abort(404,'Usuário não autorizado');   		

   		$customer_atualizado =Customer::where('account_id',$account)->first();

   		$usuario_atualizado = $customer_atualizado->user()->first();
   		$account_name = $customer_atualizado->name;

    	$token = $usuario_atualizado->paguecan_api_token;
    	$account_id = $account;

    	return view ('admin.can.configUserApi',compact('token','account_id','account_name'));

    }

    private function hasAuthToUpdate($account){

    	$current_user_account = auth()->user()->customer()->first()->account_id;

    	if ($current_user_account == $account){
    		//user trying to update his own account
    		return true;	
    	} 

    	//check if the user is an administrator
   		StdFunctions::allowAdmin();

   		//take the record related to the updated account
   		$usuario_atualizado_customer = Customer::where('account_id',$account)->first();

   		if(!$usuario_atualizado_customer) return false;

   		$usuario_corrente_customer = auth()->user()->customer()->first();

   		if ($usuario_atualizado_customer->parent_account_id != $usuario_corrente_customer->account_id){
   			return false;
   		}  

   		return true;  

    }

   
}
