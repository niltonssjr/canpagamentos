<?php

namespace App\Http\Controllers\Admin\Can;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\StdClasses\IuguApi;
use App\StdClasses\StdFunctions;
use App\Http\Requests\clienteFormRequest;
use App\Models\finalCustomer;

class ClientesController extends Controller
{
    public function index(){

    	$titulos=[
    		'Nome',
    		'CNPJ',
    		'E-mail',
    		'Cliente desde',
    		'+'
    	];

    	$iugu = new IuguApi;
    	$clientes = $iugu->clientes();
        
    	return view ('admin.cus.clientes',compact(['clientes','titulos']));

    }

    public function clienteDetalhe($id=1){

    	$iugu = new IuguApi;
    	$cliente = $iugu->clienteDetalhe($id); 

        foreach($cliente->faturas as &$fatura){
            $fatura['format_status'] = StdFunctions::fatura_format_status($fatura['status']);
        }

    	return view ('admin.cus.clienteDetalhe',compact('cliente'));

    }

    public function clienteDeletar($id=1){

        StdFunctions::DenyInactive();

        $iugu = new IuguApi;
        $retorno = $iugu->clienteDeletar($id);              

        if (isset($retorno->errors)){
            return redirect()
                        ->back()
                        ->with('error',$retorno->errors);
        }

        return redirect()->route('cus.clientes');

    }

    public function clienteNovo(){

        StdFunctions::DenyInactive();

        $customer_data = (object)[
            'name' => "",
            'email' => "",
            'phone_prefix' => "",
            'phone_number' => "",
            'cnpj' => "",
            'cpf' => "",
            'zip_code' => "",
            'street' => "",
            'number' => "",
            'others' => "",
            'district' => "",
            'city' => "",
            'state' => "",
            'account_id' => "",
            'person_type' => "fis"
        ];

    	return view ('admin.cus.clienteNovo')->with(['customer_data'=>$customer_data]);
    }

    public function clienteNovoStore(clienteFormRequest $request, finalCustomer $customer){

        StdFunctions::DenyInactive();

        $cliente = [];
        $cliente['email'] = $request->cus_customer_email;
        $cliente['name'] = $request->cus_customer_name;
        $cliente['phone'] = preg_replace("/[^0-9]/","",$request->cus_customer_phone_number);
        $cliente['phone_prefix'] = $request->cus_customer_phone_prefix;
        $cliente['cpf_cnpj'] = ($request->cus_customer_person_type == 'fis')? $request->cus_customer_cpf : $request->cus_customer_cnpj;
        $cliente['zip_code'] = $request->cus_customer_address_zip_code;
        $cliente['number'] = $request->cus_customer_address_number;
        $cliente['street'] = $request->cus_customer_address_street;
        $cliente['city'] = $request->cus_customer_address_city;
        $cliente['state'] = $request->cus_customer_address_state;
        $cliente['district'] = $request->cus_customer_address_district;
        $cliente['complement'] = $request->cus_customer_address_others;

    	$iugu = new IuguApi;
    	$cliente_id = $iugu->clienteNovoStore($cliente);        

    	return redirect()
    		->route('cus.clientes');
    }

    public function clienteAtualiza($id){

        StdFunctions::DenyInactive();

        $iugu = new IuguApi;
        $cliente = $iugu->clienteDetalhe($id); 

        $cpf_cnpj = preg_replace("/[^0-9]/","",$cliente->cpf_cnpj);

        $customer_data = (object)[
            'name' => $cliente->name,
            'email' => $cliente->email,
            'phone_prefix' => $cliente->phone_prefix,
            'phone_number' => $cliente->phone,
            'zip_code' => $cliente->address_zip_code,
            'street' => $cliente->address_street,
            'number' => $cliente->address_number,
            'others' => $cliente->address_others,
            'district' => $cliente->address_district,
            'city' => $cliente->address_city,
            'state' => $cliente->address_state,
            'account_id' => $cliente->id
        ];

        if (strlen($cpf_cnpj) ==11){
            $customer_data->cpf = $cliente->cpf_cnpj;
            $customer_data->cnpj = "";
            $customer_data->person_type = 'fis';
        } else {
            $customer_data->cpf = "";
            $customer_data->cnpj = $cliente->cpf_cnpj;
            $customer_data->person_type = 'jur';
        }

        return view ('admin.cus.clienteAtualiza')->with(['customer_data'=>$customer_data]);
    }

    public function clienteAtualizaStore(clienteFormRequest $request, finalCustomer $customer){

        StdFunctions::DenyInactive();

        $cliente = [];
        $cliente['email'] = $request->cus_customer_email;
        $cliente['name'] = $request->cus_customer_name;
        $cliente['phone'] = preg_replace("/[^0-9]/","",$request->cus_customer_phone_number);
        $cliente['phone_prefix'] = $request->cus_customer_phone_prefix;
        $cliente['cpf_cnpj'] = ($request->cus_customer_person_type == 'fis')? $request->cus_customer_cpf : $request->cus_customer_cnpj;
        $cliente['zip_code'] = $request->cus_customer_address_zip_code;
        $cliente['number'] = $request->cus_customer_address_number;
        $cliente['street'] = $request->cus_customer_address_street;
        $cliente['city'] = $request->cus_customer_address_city;
        $cliente['state'] = $request->cus_customer_address_state;
        $cliente['district'] = $request->cus_customer_address_district;
        $cliente['complement'] = $request->cus_customer_address_others;
        $cliente['account_id'] = $request->ctrl_account_id;

        $iugu = new IuguApi;
        $cliente_id = $iugu->clienteAtualizaStore($cliente);        

        return redirect()
            ->route('cus.clientes');
    }

    public function clienteEmail($email, finalCustomer $customer){
        //retorna os dados de um cliente baseado no e-mail
        
        $account_id = auth()->user()->customer()->first()->account_id;
        $cliente_db = $customer->where('email',$email)->where('parent_account_id',$account_id)->first();
        

        if (!$cliente_db) return json_encode((object)[]);        

        $iugu = new IuguApi;
        $cliente = $iugu->clienteDetalhe($cliente_db->account_id);   
        
        return json_encode($cliente);

    }

    public function pesquisaCliente($argumento){

        $iugu = new IuguApi;
        
        $clientes = $iugu->clientes(['query'=>"*".trim($argumento)."*"]);

        return json_encode($clientes);
    }

    public function clientePorId($id=1){

        $iugu = new IuguApi;
        $cliente = $iugu->clienteDetalhe($id); 

        return json_encode($cliente);

    }    
}
