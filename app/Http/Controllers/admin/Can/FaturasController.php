<?php

namespace App\Http\Controllers\Admin\Can;

use Illuminate\Http\Request;
use App\Http\Requests\faturaFormRequest;
use App\Http\Controllers\Controller;
use App\StdClasses\IuguApi;
use App\StdClasses\StdFunctions;
use App\Rules\cnpjRule;
use App\Rules\cpfRule;
use Validator;

class FaturasController extends Controller
{
    public function index(){

    	$titulos=[
    		'Data',
    		'Cliente',
    		'E-mail',
    		'Vencimento',
    		'Pagamento',
    		'Status',
    		'Valor',
    		'Taxas',
    		'+'
    	];

    	$iugu = new IuguApi;
    	$faturas = $iugu->faturas();

        foreach($faturas as &$fatura){
            $fatura['format_status'] = StdFunctions::fatura_format_status($fatura['status']);
        }

    	return view ('admin.can.faturas',compact(['faturas','titulos']));
    }

    public function faturaDetalhe($id = 2){

    	$iugu = new IuguApi;
    	$fatura = $iugu->faturaDetalhe($id);
        $fatura->status = StdFunctions::fatura_format_status($fatura->status);
        $fatura->payment_method = StdFunctions::fatura_format_payment_method($fatura->payment_method);
    	
    	return view ('admin.can.faturaDetalhe',compact('fatura'));

    }

    public function faturaCancel($id = 2){

        StdFunctions::DenyInactive();

        $iugu = new IuguApi;
        $fatura = $iugu->faturaCancel($id);
        $fatura->status = StdFunctions::fatura_format_status($fatura->status);
        $fatura->payment_method = StdFunctions::fatura_format_payment_method($fatura->payment_method);        
        
        return view ('admin.can.faturaDetalhe',compact('fatura'));

    }

    public function novaFatura(){

        StdFunctions::DenyInactive();

        return view ('admin.cus.novaFatura');
        
    }

    public function novaFaturaStore(Request $request){
        
        StdFunctions::DenyInactive();

        $items_validation = StdFunctions::invoice_items_validator($request);        

        $std_messages = [
            'cus_fatura_vencimento.required'               => 'Informe o vencimento da fatura',
            'cus_fatura_vencimento.after'                  => 'A data de vencimento deve ser futura',
            'cus_fatura_pagador_email.required'            => 'Informe o e-mail do pagador',
            'cus_fatura_pagador_email.email'               => 'Informe um e-mail válido',
            'cus_fatura_pagador_cnpj.required_if'          => 'Informe o CNPJ do pagador',
            'cus_fatura_pagador_cpf.required_if'           => 'Informe o CPF do pagador',
            'cus_fatura_pagador_nome.required'             => 'Informe o nome do pagador',
            'cus_fatura_pagador_address_street.required'   => 'Informe o endereço do pagador',
            'cus_fatura_pagador_address_number.required'   => 'Informe o número do endereço do pagador',
            'cus_fatura_pagador_address_city.required'     => 'Informe a cidade do pagador',
            'cus_fatura_pagador_address_state.required'    => 'Informe o estado do endereço do pagador',
            'cus_fatura_pagador_address_zip_code.required' => 'Informe o CEP do pagador'
        ];

        $std_rules = [
            'cus_fatura_vencimento'             => 'required|date_format:"d/m/Y"|after:today',          
            'cus_fatura_pagador_email'          => 'required|email',
            'cus_fatura_pagador_nome'           => 'required',
            'cus_fatura_pagador_cnpj'           => ['required_if:cus_fatura_person_type,jur', new cnpjRule],
            'cus_fatura_pagador_cpf'           => ['required_if:cus_fatura_person_type,fis', new cpfRule],
            'cus_fatura_pagador_address_zip_code' => 'required',
            'cus_fatura_pagador_address_street' => 'required',
            'cus_fatura_pagador_address_number' => 'required',
            'cus_fatura_pagador_address_city'   => 'required',
            'cus_fatura_pagador_address_state'  => 'required'
        ];

        $rules = array_merge($std_rules,$items_validation['rules']);
        $messages = array_merge($std_messages,$items_validation['messages']);
        
        $request->validate($rules,$messages);

        $iugu = new IuguApi;        
        $retorno = $iugu->faturaIncluir((object)$request->all());
        
        if (isset($retorno->erro)){

            foreach($retorno->erro->errors as $key=>$erros){
                $validator = Validator::make([],[]);
                $validator->getMessageBag()->add($key,implode(",",$erros));
                return redirect()->back()->withErrors($validator)->withInput();                  
            }                
        }

        $fatura_id = $retorno;

        return redirect()
                ->route('can.faturaDetalhe',['id'=>$fatura_id]);

    }

    public function relatorioFaturas(Request $request){        

        $meses_return = $this->array_meses($request);
        $meses = $meses_return['meses'];

        $anos_return = $this->array_anos($request);
        $anos = $anos_return['anos'];

        $statuses_return = $this->array_status($request);
        $statuses = $statuses_return['statuses'];

        $parms=[
            'limit' => 100,
            'start' => 0,            
            'status' => $statuses_return['status_atual'],
            'month' => $meses_return['mes_atual'],
            'year' => $anos_return['ano_atual']
        ];
        
        $iugu = new IuguApi;
        $linhas = $iugu->relFaturas($parms);        

        $titulos=[
            'Data',
            'Cliente',            
            'Vencimento',
            'Pagamento',
            'Valor pago',
            'Valor pendente',
            // 'Taxas',
            '+'
        ];        

        $return = [
            'titulos' => $titulos,
            'linhas' => $linhas, 
            'meses' => $meses,
            'anos' => $anos,
            'statuses' => $statuses
        ];

        return view('admin.can.relFaturas')->with($return);

    }

    private function array_meses($request=null){

        $meses = [
            ['num' => '01', 'nome'=> 'Janeiro', 'selected' =>" "],
            ['num' => '02', 'nome'=> 'Fevereiro', 'selected' =>" "],
            ['num' => '03', 'nome'=> 'Março', 'selected' =>" "],
            ['num' => '04', 'nome'=> 'Abril', 'selected' =>" "],
            ['num' => '05', 'nome'=> 'Maio', 'selected' =>" "],
            ['num' => '06', 'nome'=> 'Junho', 'selected' =>" "],
            ['num' => '07', 'nome'=> 'Julho', 'selected' =>" "],
            ['num' => '08', 'nome'=> 'Agosto', 'selected' =>" "],
            ['num' => '09', 'nome'=> 'Setembro', 'selected' =>" "],
            ['num' => '10', 'nome'=> 'Outubro', 'selected' =>" "],
            ['num' => '11', 'nome'=> 'Novembro', 'selected' =>" "],
            ['num' => '12', 'nome'=> 'Dezembro', 'selected' =>" "],
        ];

        $mes_atual = date_format(date_create(),"m");        

        if (is_object($request)){
            if ($request->relfatura_mes){
                $mes_atual = $request->relfatura_mes;
            }    
        }

        $meses[(int)$mes_atual - 1]['selected'] = ' SELECTED ';        

        return [
            'mes_atual' => $mes_atual,
            'meses' => $meses];
    }

    private function array_anos($request=null){

        $ano_atual = (integer)date_format(date_create(),"Y");
        $ano_limite = $ano_atual - 5;
        $anos = [];

        $ano = $ano_atual;
        while($ano > $ano_limite){
            $ano_sel = [
                'value' => $ano,
                'text' => $ano,
                'selected' => " "
            ];            
            $anos[] = $ano_sel;
            $ano--;
        }
        
        if (is_object($request)){
            if ($request->relfatura_ano){
                $ano_atual = $request->relfatura_ano;
            }    
        }

        for ($ind=0;$ind<count($anos);$ind++){
            if ($anos[$ind]['value'] == $ano_atual) $anos[$ind]['selected'] = 'SELECTED';
        }        

        return [
                'ano_atual' => $ano_atual,
                'anos' => $anos
            ];
    }    

    private function array_status($request=null){

        $status_atual = 'pending';

        $statuses = [
            ['value' => 'pending', 'text' => 'Em aberto', 'selected' => " "],
            ['value' => 'paid', 'text' => 'Pago', 'selected' => " "],
            ['value' => 'partially_paid', 'text' => 'Parcialmente pago', 'selected' => " "],
            ['value' => 'refunded', 'text' => 'Devolvido', 'selected' => " "],
            ['value' => 'expired', 'text' => 'Expirado', 'selected' => " "],
            ['value' => 'cancelled', 'text' => 'Cancelado', 'selected' => " "],
        ];

        if (is_object($request)){
            if ($request->relfatura_status){
                $status_atual = $request->relfatura_status;
            }    
        }

        for ($ind=0;$ind<count($statuses);$ind++){
            if ($statuses[$ind]['value'] == $status_atual) $statuses[$ind]['selected'] = 'SELECTED';
        }        

        return [
                'status_atual' => $status_atual,
                'statuses' => $statuses
            ];
    } 
}
