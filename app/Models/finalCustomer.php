<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class finalCustomer extends Model
{
    protected $fillable = ['account_id','email','name','parent_account_id'];
}
