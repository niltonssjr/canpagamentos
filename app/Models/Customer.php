<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Customer extends Model
{
    protected $fillable=[
    	'user_id',
    	'created_by',
    	'account_id',
    	'name',
    	'live_api_token',
    	'test_api_token',
    	'user_token',
    	'oper_mode',
    	'created_at',
    	'updated_at',
    	'comm_cents',
    	'comm_perc',
    	'cpf_cnpj',
    	'business_descr',
    	'company_name',
    	'zip_code',
    	'address_street',
    	'address_number',
    	'address_city',
    	'address_state',
    	'phone',
    	'responsible_name',
    	'responsible_cpf',
    	'account_agency',
    	'account_number',
    	'account_digit',
        'parent_account_id'
    ];

    

    public function user(){
    	return $this->belongsTo(User::class);
    }
}
