<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class bankAg implements Rule
{
    protected $bank;
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($bank)
    {
        $this->bank = $bank;

    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $value = str_replace('.',"",$value);
        $bancos_com_digito = ['001','237'];

        if (in_array($this->bank,$bancos_com_digito)){
            $padrao = "/^[0-9]{1,4}-[0-9]{1}$/";            
        } else {
            $padrao = "/^[0-9]{1,4}$/";
        }        
        
        return preg_match($padrao,$value);

    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'O código de agência informado não segue o padrão do seu banco.';
    }
}
