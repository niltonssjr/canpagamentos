<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use App\Models\finalCustomer;

class uniqueEmailForFinalCustomer implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void

     */
    private $account_id;
    public function __construct($id)
    {
        $this->account_id = $id;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        
        $parent_account_id = auth()->user()->customer()->first()->account_id;

        $finalCustomer = finalCustomer::where([
                    ['email','=',$value],
                    ['parent_account_id','=',$parent_account_id]
            ])->first();

        $mostra = [
            'parent_account_id' => $parent_account_id,
            'email' => $value
        ];

        //se a combinação não foi encontrada, significa que este e-mail não existe para o cliente
        if (!$finalCustomer) return true;

        //se a combinação foi encontrada porém o account_id é o mesmo, significa que é uma atualização
        if ($finalCustomer->account_id == $this->account_id) return true;

        return false;

    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Email já cadastrado';
    }
}
