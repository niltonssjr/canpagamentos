<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class bankcc implements Rule
{
    protected $bank;
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($bank)
    {
        $this->bank = $bank;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $value = str_replace('.',"",$value);
        
        $bank_patterns = [
            '001' => '/^[0-9]{1,8}-[0-9]{1}$/',
            '033' => '/^[0-9]{1,8}-[0-9]{1}$/',
            '104' => '/^[0-9]{10}-[0-9]{1}$/',
            '237' => '/^[0-9]{1,7}-[0-9]{1}$/',
            '341' => '/^[0-9]{1,5}-[0-9]{1}$/',
            '041' => '/^[0-9]{1,9}-[0-9]{1}$/',
            '748' => '/^[0-9]{1,6}$/',
            '756' => '/^[0-9]{1,9}-[0-9]{1}$/',
            '077' => '/^[0-9]{1,9}-[0-9]{1}$/',
            '070' => '/^[0-9]{1,9}-[0-9]{1}$/',
        ];

        $padrao = $bank_patterns[$this->bank];

        return preg_match($padrao,$value);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'O número da conta informado não segue o padrão do seu banco.';
    }
}
