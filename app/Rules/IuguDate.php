<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class IuguDate implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if (!preg_match("/^[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}-[0-9]{2}:[0-9]{2}$/",$value)) return false;

        $year = substr($value,0,4);
        $month = substr($value,5,2);
        $day = substr($value,8,2);

        if ((integer)$year < 1950 || (integer)$year > 2100) return false;
        if ((integer)$month > 12) return false;
        if ((integer)$day > 31) return false;

        $date = substr($value,0,10);
        $time = substr($value,11,8);
        $shift = substr($value,20,5);

        $evaluated_date = $date . " " . $time;

        $result_date = date_format(date_create($evaluated_date),"Y-m-d H:i:s");

        if ($result_date != $evaluated_date) return false;
        
        // ob_start();
        // print_r($result_date);
        // $ret = ob_get_contents();
        // ob_end_clean();
        // die($ret);

        if ($shift!='03:00') return false;

        return true;
        
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return "O parâmetro ':attribute' não é válido.";
    }
}
