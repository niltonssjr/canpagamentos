<?php 

namespace App\StdClasses;

use App\Models\finalCustomer;
use App\Models\Customer;
use Illuminate\Http\Request;
use Gate;

class IuguApi{
	
	private $iugu_url = 'https://api.iugu.com/v1/';	

	private function callIuguApi($type, $url, $params = array()){

		$customer_data = auth()->user()->customer()->first();
		$token_in_use = "live_api_token";

		$api_token = $customer_data->{$token_in_use};

		if ($url=="accounts/configuration"){
			if (isset($params['account_id'])){
				$received_account = $params['account_id'];
				//if the account research is not the one used by the logged user
				if ($customer_data->account_id != $received_account){
					//check if the researched account is a sub-account of the logged user
					$customer = Customer::where('account_id',$received_account)
														->first();				
					//valid sub-account of the logged user
					if ($customer){	

						// if ($customer->created_by == auth()->user()->id){
						if (Gate::allows('isAdmin')){						
							$api_token = $customer->{$token_in_use};
						}
					}
					// $api_token = "194c9e71117719e7f50f7fc00d5a11b9";
				}				
			}
		}

		//use customer token when checking customer information
		if (strpos($url,'accounts/')!==false && $type == 'GET'){

		// if ($url=='accounts/' && $type == 'GET'){

			$received_account = substr($url,9);			

			//if the account research is not the one used by the logged user
			if ($customer_data->account_id != $received_account){
				//check if the researched account is a sub-account of the logged user

				$customer = Customer::where('account_id',$received_account)
													->first();				
				//valid sub-account of the logged user
				if ($customer){								
					// if ($customer->created_by == auth()->user()->id){						
					if (Gate::allows('isAdmin')){						
						$api_token = $customer->{$token_in_use};
					}
				}
				// $api_token = "194c9e71117719e7f50f7fc00d5a11b9";
			}
		}

		//use customer user token when verifying the account
		if (strpos($url,'request_verification')!==false && $type == 'POST'){			
			$received_account = str_replace(['accounts/','/request_verification'],"",$url);			
			//if the account research is not the one used by the logged user
			if ($customer_data->account_id != $received_account){
				//check if the researched account is a sub-account of the logged user
				$customer = Customer::where('account_id',$received_account)
													->first();				
				//valid sub-account of the logged user
				if ($customer){					
					if ($customer->created_by == auth()->user()->id){
						$api_token = $customer->user_token;
					}
				}
			}			
		}

		//use customer user token when inactivating an account
		if (strpos($url,'accounts')!==false && $type == 'PUT'){		

			$received_account = str_replace(['accounts/'],"",$url);			
			//if the account research is not the one used by the logged user
			if ($customer_data->account_id != $received_account){
				//check if the researched account is a sub-account of the logged user
				$customer = Customer::where('account_id',$received_account)
													->first();				
				//valid sub-account of the logged user
				if ($customer){					
					if ($customer->created_by == auth()->user()->id){
						$api_token = $customer->user_token;
					}
				}
			}			
		}

		$http_basic_auth = base64_encode($api_token . ":");		

		$headers = [
			'Authorization: ' . $http_basic_auth,
			'Accept : application/json',
			'Accept-Charset: utf-8'
		];		

		$post = ($type=='POST');		
		
		$params_fmt = $this->arrayToParams($params);

		$curl_url = $this->iugu_url . $url;

		$process = curl_init();

		curl_setopt($process, CURLOPT_FRESH_CONNECT, true);
		curl_setopt($process, CURLOPT_HTTPHEADER,$headers);
		curl_setopt($process, CURLOPT_POST, 1);
		curl_setopt($process, CURLOPT_RETURNTRANSFER,true);
		curl_setopt($process, CURLOPT_TIMEOUT,0);
		curl_setopt($process, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		curl_setopt($process, CURLOPT_POSTFIELDS, $params_fmt);
		curl_setopt($process, CURLOPT_USERPWD, $http_basic_auth); 
		curl_setopt($process, CURLOPT_HEADER, false);		

		if ($type=='GET'){
			curl_setopt($process,CURLOPT_HTTPGET,true);
			if ($params!=[]) $curl_url.= "?".$params_fmt;
		}	

		if ($type=='PUT'){
			curl_setopt($process,CURLOPT_PUT,true);
			if ($params!=[]) $curl_url.= "?".$params_fmt;
		}	

		if ($type=='DELETE'){			
			curl_setopt($process, CURLOPT_CUSTOMREQUEST, "DELETE");
			if ($params!=[]) $curl_url.= "?".$params_fmt;
		}

		curl_setopt($process,CURLOPT_URL,$curl_url);

		$return = curl_exec($process);

		$status = curl_getinfo($process,CURLINFO_HTTP_CODE);

		curl_close($process);	

		if ($status != 200){
			if ($status==401){
				return json_encode(['erro'=>'Acesso não autorizado.']);
				// return json_encode(['erro'=>'Acesso não autorizado. Token:' .$api_token]);
			}	
			return json_encode(['erro'=>$return]);
			abort(404,"(WS$status) O acesso ao sistema do banco não pode ser concluído. Tente novamente mais tarde.");			
		}

		return $return;

	}

	public function subconta_nova($request){

		$new_account = [
			'name' => $request->subconta_nome,
			'commissions' => []
		];

		$cents = 1;		
		$credit_card_cents = StdFunctions::valor_em_centavos($request->subconta_comissao_cc_dinheiro);		
		$bank_slip_cents = StdFunctions::valor_em_centavos($request->subconta_comissao_boletos_dinheiro);		

		if ($cents > 0) $new_account['commissions']['cents'] = $cents;		
		if ($credit_card_cents > 0) $new_account['commissions']['credit_card_cents'] = $credit_card_cents;		
		if ($bank_slip_cents > 0) $new_account['commissions']['bank_slip_cents'] = $bank_slip_cents;		

        //  $new_account = [
        //     'name' => $request->subconta_nome,
        //     'commissions' => [
        //     'cents' => StdFunctions::valor_em_centavos($request->subconta_comissao_dinheiro),
        //     'percent' => StdFunctions::valor_em_float($request->subconta_comissao_percentual),
        //     'credit_card_cents' => StdFunctions::valor_em_centavos($request->subconta_comissao_cc_dinheiro),
        //     'credit_card_percent' => StdFunctions::valor_em_float($request->subconta_comissao_cc_percentual),
        //     'bank_slip_cents' => StdFunctions::valor_em_centavos($request->subconta_comissao_boletos_dinheiro),
        //     'bank_slip_percent' => StdFunctions::valor_em_float($request->subconta_comissao_boletos_percentual)
        //     ]
        // ];

        // dd($new_account);
		$result = $this->callIuguApi('POST','marketplace/create_account',$new_account);

		$new_account_return = json_decode($result);

		return $new_account_return;

	}

	public function subconta_inativar($id){

		$parms = ['name'=>'INATIVA'];
		$result = json_decode($this->callIuguApi('PUT',"accounts/{$id}",$parms));
		return $result;

	}

	public function subcontas_listar(){

		$result = json_decode($this->callIuguApi('GET','marketplace'));

		$subcontas = [];

		foreach($result->items as $item){
			$subconta = [];
			$subconta['id'] = $item->id;
			$subconta['name'] = $item->name;
			$subconta['verified'] = $this->verificada($item->verified);
			$subconta['isverified'] = $item->verified;
			
			$subcontas[] = $subconta;
		}

		return $subcontas;

	}

	public function subconta_validar($request){

		$customer = Customer::where('account_id',$request->subconta_account_id)->first();
		
		if (!$customer){
			$retorno = (object)[];
			$retorno->erro = "Cliente não encontrado na base de clientes";
			return $retorno;			
		}
		
		$customer->cpf_cnpj = ($request->subconta_person_type =='fis') ? $request->subconta_CPF : $request->subconta_CNPJ;
		$customer->company_name = ($request->subconta_person_type =='fis') ? $request->subconta_nome : $request->subconta_company_name;
		$customer->business_descr = $request->subconta_business_type;
		$customer->zip_code = $request->subconta_address_zip_code;
		$customer->address_street = $request->subconta_address_street;
		$customer->address_city = $request->subconta_address_city;
		$customer->address_state = $request->subconta_address_state;
		$customer->phone = $request->subconta_phone_number;
		$customer->responsible_name = $request->subconta_responsible_name;
		$customer->responsible_cpf = $request->subconta_responsible_CPF;
		$customer->account_agency = str_replace(".","",$request->subconta_bank_ag);
		$customer->account_number = str_replace(".","",$request->subconta_bank_cc);

		$return = $customer->save();

		if (!$return){
			$retorno = (object)[];
			$retorno->erro = "Problemas na atualização da base de clientes";
			return $retorno;			
		}

		
		$subconta = [];
		$subconta['data']['price_range'] = $request->subconta_maximum_value;
		$subconta['data']['physical_products'] = ($request->subconta_physical_products == 'Sim');
		$subconta['data']['business_type'] = $request->subconta_business_type;
		$subconta['data']['person_type'] = ($request->subconta_person_type =='fis')? 'Pessoa Física' : 'Pessoa Jurídica';
		$subconta['data']['automatic_transfer'] = ($request->subconta_automatic_transfer == 'Sim');
		$subconta['data']['cnpj'] = $request->subconta_CNPJ;
		$subconta['data']['cpf'] = $request->subconta_CPF;
		$subconta['data']['company_name'] = $request->subconta_company_name;
		$subconta['data']['name'] = $request->subconta_nome;
		$subconta['data']['address'] = $request->subconta_address_street;
		$subconta['data']['cep'] = $request->subconta_address_zip_code;
		$subconta['data']['city'] = $request->subconta_address_city;
		$subconta['data']['state'] = $request->subconta_address_state;
		$subconta['data']['telephone'] = $request->subconta_phone_number;
		$subconta['data']['resp_name'] = $request->subconta_responsible_name;
		$subconta['data']['resp_cpf'] = $request->subconta_responsible_cpf;
		$subconta['data']['bank'] = $request->subconta_bank;
		$subconta['data']['bank_ag'] = $request->subconta_bank_ag;
		$subconta['data']['account_type'] = $request->subconta_bank_type;
		$subconta['data']['bank_cc'] = $request->subconta_bank_cc;

		$id = $request->subconta_account_id;
		$url = "accounts/{$id}/request_verification";
		$result = $this->callIuguApi('POST',$url,$subconta);

		$result_decode = json_decode($result);

		$retorno = (object)[];

		if (isset($result_decode->erro)){
			$retorno->erro = 'Não foi possível processar a solicitação';
		} else {
			$retorno->account_id = $result_decode->account_id;
		}

		return $retorno;

	}

	public function transferencias(){

		$result = $this->callIuguApi('GET','withdraw_requests');		

		$result_body = json_decode($result);
		
		$lancamentos = [];
		
		foreach($result_body->items as $item){			
			$lancamento = [];
			$lancamento['data'] = StdFunctions::format_date($item->created_at);
			$lancamento['solicitante'] = $item->account_name;
			$lancamento['valor'] = $item->amount;
			$bank_address = json_decode($item->bank_address);
			$lancamento['banco'] = $bank_address->bank;
			$lancamento['agencia'] = $bank_address->bank_ag;
			$lancamento['conta'] = $bank_address->bank_cc;
			$lancamento['status'] = StdFunctions::saque_format_status($item->status);
			$lancamentos[] = $lancamento;
		}

		return $lancamentos;
	}
	public function faturas($parms = array()){

		$result = $this->callIuguApi('GET','invoices',$parms);
		
		$result_body = json_decode($result);

		$faturas = array();

		foreach($result_body->items as $items){
			$fatura = array();			
			$fatura['criacao'] = StdFunctions::format_date($items->created_at_iso);
			$fatura['cliente'] = $items->customer_name;
			$fatura['email'] = $items->email;
			$fatura['vencimento'] = StdFunctions::format_date($items->due_date);
			$fatura['pagamento'] = isset($items->paid_at)? StdFunctions::format_date($items->paid_at) : "";			
			$fatura['status'] = $items->status;
			$original_status = $items->status;
			$fatura['valor'] = $items->total;
			$fatura['taxas'] = StdFunctions::currency_from_cents($items->taxes_paid_cents + $items->commission_cents);
			// $fatura['taxas'] = $items->taxes_paid;
			$fatura['id'] = $items->id;			

			$faturas[] = $fatura;
		}
		
		return $faturas;
	}

	public function faturaDetalhe($id){

		$result = $this->callIuguApi('GET',"invoices/{$id}");
		$fatura = json_decode($result);

		if (isset($fatura->erro)) return (object)["e404"=>"Fatura não encontrada"];

		$fatura->due_date = StdFunctions::format_date($fatura->due_date);
		$fatura->created_at = StdFunctions::format_date($fatura->created_at_iso);
		$fatura->status = $fatura->status;
		$fatura->payment_method = $fatura->payment_method;
		$fatura->total = $fatura->total;

		$fatura->payer_address_city = "";
		$fatura->payer_address_complement = "";
		$fatura->payer_address_district = "";
		$fatura->payer_address_number = "";
		$fatura->payer_address_state = "";
		$fatura->payer_address_street = "";
		$fatura->payer_address_zip_code = "";
		$fatura->payer_cpf_cnpj = "";
		$fatura->payer_name = "";
		$fatura->payer_phone = "";
		$fatura->payer_phone_prefix = "";

		foreach($fatura->variables as $variable){
			$column = str_replace(".","_",$variable->variable);
			$fatura->{$column} = $variable->value;
		}

		$fatura->taxes_paid = StdFunctions::currency_from_cents($fatura->taxes_paid_cents + $fatura->commission_cents);
		$fatura->interest = StdFunctions::currency_from_cents($fatura->interest);
		
		return (object)$fatura;

	}
	
	public function faturaCancel($id){

		$result = $this->callIuguApi('PUT',"invoices/{$id}/cancel");
		$fatura = json_decode($result);

		if (isset($fatura->erro)) return (object)["e404"=>"Fatura não encontrada"];		

		$fatura->due_date = StdFunctions::format_date($fatura->due_date);
		$fatura->created_at = StdFunctions::format_date($fatura->created_at_iso);
		$fatura->status = $fatura->status;
		$fatura->payment_method = $fatura->payment_method;
		$fatura->total = $fatura->total;

		$fatura->payer_address_city = "";
		$fatura->payer_address_complement = "";
		$fatura->payer_address_district = "";
		$fatura->payer_address_number = "";
		$fatura->payer_address_state = "";
		$fatura->payer_address_street = "";
		$fatura->payer_address_zip_code = "";
		$fatura->payer_cpf_cnpj = "";
		$fatura->payer_name = "";

		foreach($fatura->variables as $variable){
			$column = str_replace(".","_",$variable->variable);
			$fatura->{$column} = $variable->value;
		}

		return (object)$fatura;

	}	
	public function faturaIncluir($dados_fatura){

		$itens = [];
		foreach($dados_fatura as $key=>$item){
			if (strpos($key,'cus_fatura_desc_')!==false){
				$ind = str_replace("cus_fatura_desc_","",$key);
				$itens_line = [];
				$field_description = "cus_fatura_desc_".$ind;
				$field_quantity = "cus_fatura_qtd_".$ind;
				$field_value = "cus_fatura_valor_" . $ind;					
				$itens_line['description'] = $dados_fatura->{$field_description};
				$itens_line['quantity'] = preg_replace("/[^0-9]/","",$dados_fatura->{$field_quantity});				
				$itens_line['price_cents'] = StdFunctions::valor_em_centavos($dados_fatura->{$field_value});
				$itens[] = $itens_line;
			}
		}

		$payer = [];
		if ($dados_fatura->cus_fatura_person_type=='fis'){
			$payer['cpf_cnpj'] = $dados_fatura->cus_fatura_pagador_cpf;
		} else {
			$payer['cpf_cnpj'] = $dados_fatura->cus_fatura_pagador_cnpj;
		}		
		$payer['name'] = $dados_fatura->cus_fatura_pagador_nome;
		$payer['phone_prefix'] = isset($dados_fatura->cus_fatura_pagador_phone_prefix)? $dados_fatura->cus_fatura_pagador_phone_prefix : "";
		$payer['phone'] = isset($dados_fatura->cus_fatura_pagador_phone)?$dados_fatura->cus_fatura_pagador_phone:"";
		$payer['email'] = $dados_fatura->cus_fatura_pagador_email;
		$payer['address'] = [];
		$payer['address']['zip_code'] = $dados_fatura->cus_fatura_pagador_address_zip_code;
		$payer['address']['street'] = $dados_fatura->cus_fatura_pagador_address_street;
		$payer['address']['number'] = $dados_fatura->cus_fatura_pagador_address_number;
		$payer['address']['city'] = $dados_fatura->cus_fatura_pagador_address_city;
		$payer['address']['state'] = $dados_fatura->cus_fatura_pagador_address_state;
		$payer['address']['complement'] = $dados_fatura->cus_fatura_pagador_address_others;
		$payer['address']['country'] = 'Brasil';				

		// $customer_id = finalCustomer::where('email',$dados_fatura->cus_fatura_pagador_email)->first()->account_id;
		
		$fatura = [
			'email'		=> $dados_fatura->cus_fatura_pagador_email,
			'due_date'	=> date_format(date_create_from_format("d/m/Y",$dados_fatura->cus_fatura_vencimento),"Y-m-d"),
			'ensure_workday_due_date' => true,
			'items'		=> $itens,
			'payer'		=> $payer,
			'customer_id' => isset($dados_fatura->cus_fatura_pagador_id)? $dados_fatura->cus_fatura_pagador_id:""
		];	

		if (isset($dados_fatura->cus_fatura_juros_multa_aplicar)){
			$fatura['fines'] = true;
			$fatura['late_payment_fine'] = (trim($dados_fatura->cus_fatura_juros_multa_valor)!="")?$dados_fatura->cus_fatura_juros_multa_valor:0;
			$fatura['per_day_interest'] = isset($dados_fatura->cus_fatura_juros_prorata_aplicar);
		} else {
			$fatura['fines'] = false;
		}
		
		// return response(json_encode($fatura),422)->header('Content-Type',"application/json");
		// $customer = finalCustomer::where('email',$dados_fatura->cus_fatura_pagador_email)->where('parent_account_id',auth()->user()->customer()->first()->account_id)->first();				
		// if ($customer) $fatura['customer_id'] = $customer->account_id;
		
		$retorno=json_decode($this->callIuguApi('POST','invoices',$fatura));

		if (isset($retorno->erro)){
			return (object)['erro'=> json_decode($retorno->erro)];
		}

		$fatura_id = $retorno->id;		

		return $fatura_id;

	}

public function clientes($parms=null){

		$result = $this->callIuguApi('GET','customers',$parms);
		
		$result_body = json_decode($result);

		$clientes = array();
		
		foreach($result_body->items as $items){
			$cliente = array();	
			$cliente['nome'] = $items->name;
			$cliente['cnpj'] = $items->cpf_cnpj;
			$cliente['email'] = $items->email;
			$cliente['criacao'] = StdFunctions::format_date($items->created_at);
			$cliente['id'] = $items->id;
			$clientes[] = $cliente;
		}

		return $clientes;
	}

	public function clienteDetalhe($id){

		$result = $this->callIuguApi('GET',"customers/{$id}");
		$retorno = json_decode($result);

		if (isset($retorno->erro)) return (object)["e404"=>"Cliente não encontrado"];

    	$cliente = (object)[];
    	$cliente->name = $retorno->name;
    	$cliente->id = $retorno->id;
    	$cliente->email = $retorno->email;
    	$cliente->cpf_cnpj = $retorno->cpf_cnpj;
    	$cliente->address_zip_code = $retorno->zip_code;
    	$cliente->address_street = $retorno->street;
    	$cliente->address_number = $retorno->number;
    	$cliente->address_others = $retorno->complement;
    	$cliente->phone = $retorno->phone;
    	$cliente->phone_prefix = $retorno->phone_prefix;
    	$cliente->address_city = $retorno->city;
    	$cliente->address_state = $retorno->state;
    	$cliente->address_district = $retorno->district;
    	$cliente->customer_since = date_format(date_create($retorno->created_at),'d/m/Y');

    	$cliente->faturas = $this->faturasPorCliente($cliente->id);
    	
    	if (!$cliente->faturas) $cliente->faturas = array();
    	
		return $cliente;

	}

public function clienteDeletar($id){

		$faturas = $this->faturasPorCliente($id);

		$faturas_pendentes = false;		
		foreach($faturas as $fatura){
			if (strpos($fatura['status'],"pending")!==false){
				$faturas_pendentes = true;
				break;
			} 
		}

		if ($faturas_pendentes) return (object)['errors'=>'Cliente não pode ser excluído, pois possui faturas em aberto'];

		$result = $this->callIuguApi('DELETE',"customers/{$id}");		
		$retorno = json_decode($result);

		if (!isset($retorno->id)) dd($retorno);
		
		if (isset($retorno->erro)) return (object)["e404"=>"Cliente não encontrado"];

        //$cliente = FinalCustomer::where('account_id',$id);
        //$cliente->delete();

		
    	
		return $retorno;

	}

	public function clienteNovoStore($cliente){

        $retorno_iugu = json_decode($this->callIuguApi('POST','customers',$cliente));
        
        if (!isset($retorno_iugu->id)) dd($retorno_iugu);

        $cliente_id = $retorno_iugu->id;

        finalCustomer::create([
                'name' => $cliente['name'],
                'email' => $cliente['email'],
                'account_id' => $cliente_id,
                'parent_account_id' => auth()->user()->customer()->first()->account_id
            ]);		    	
		
		return $cliente_id;

	}

	public function clienteAtualizaStore($cliente){
    	
		$retorno = json_decode($this->callIuguApi('PUT',"customers/{$cliente['account_id']}",$cliente));		
		if (isset($retorno->erro)) return (object)["e404"=>"Cliente não encontrado"];

		$cliente_id = $retorno->id;

        $customer = finalCustomer::where('account_id',$cliente_id)->first();
        
        $customer->name = $cliente['name'];
        $customer->email = $cliente['email'];

        $customer->save();				

		return $cliente_id;

	}

	public function faturasPorCliente($id = 2){

		return $this->faturas(['customer_id'=>$id]);

	}

	public function dashboard($account = null){
		
		if (!$account){
			$user_data = auth()->user()->customer()->first();
			$account = $user_data->account_id;
		}

		//inicia dashboard para enviar dados vazios caso o acesso não seja autorizado
		$dashboard = (object)[];
		$dashboard_columns = ['last_withdraw','volume_last_month','volume_this_month','total_subscriptions','balance','balance_available_for_withdraw','protected_balance','payable_balance','receivable_balance','commission_balance','taxes_paid_last_month','taxes_paid_this_month','id','name','created_at','is_verified', 'last_verification_status','resp_name','resp_cpf','payment_email_notification','webapp_on_test_mode','auto_withdraw','raw_auto_withdraw','raw_payment_email_notification','payment_email_notification_receiver','bank_slip_fee','cnpj','this_month_invoice_created_count','this_month_invoice_created_total'];

  		$personal_data_fields = ['person_type','automatic_transfer','address','cep','city','state','telephone','bank','bank_ag','account_type','bank_cc','document_id','document_cpf','document_activity','business_type'];
  		
  		foreach($dashboard_columns as $field){
  			$dashboard->{$field} = "";
  		}  		

  		foreach($personal_data_fields as $field){
  			$dashboard->{$field} = "";
  		}  		

		$return = (object)json_decode($this->callIuguApi('GET',"accounts/{$account}"));

		if (isset($return->erro)){
			session(['error'=>$return->erro]);
			return $dashboard;
		}		

		$last_withdraw = isset($return->last_withdraw->amount)? $return->last_withdraw->amount : "";
		$dashboard->last_withdraw = StdFunctions::float_to_currency($last_withdraw);
  		$dashboard->volume_last_month = StdFunctions::format_null_currency($return->volume_last_month);
  		$dashboard->volume_this_month = StdFunctions::format_null_currency($return->volume_this_month);
  		$dashboard->total_subscriptions = $return->total_subscriptions;

		$dashboard->balance = StdFunctions::format_null_currency($return->balance);
		$dashboard->balance_available_for_withdraw = StdFunctions::format_null_currency($return->balance_available_for_withdraw);
  		$dashboard->protected_balance = StdFunctions::format_null_currency($return->protected_balance);
  		$dashboard->payable_balance = StdFunctions::format_null_currency($return->payable_balance);
  		$dashboard->receivable_balance = StdFunctions::format_null_currency($return->receivable_balance);
  		$dashboard->commission_balance = StdFunctions::format_null_currency($return->commission_balance);
  		$dashboard->taxes_paid_last_month = StdFunctions::format_null_currency($return->taxes_paid_last_month);
  		$dashboard->taxes_paid_this_month = StdFunctions::format_null_currency($return->taxes_paid_this_month);
  		
  		$dashboard->id = $return->id;
  		$dashboard->name = $return->name;
  		$dashboard->created_at = StdFunctions::format_date($return->created_at);
  		$praquepontodeinterrogacaononome = "is_verified?";
  		$dashboard->is_verified = $this->verificada($return->{$praquepontodeinterrogacaononome});
  		$dashboard->last_verification_status = StdFunctions::status_verif_account($return->last_verification_request_status);


  		$personal_data_fields = ['person_type','automatic_transfer','address','cep','city','state','telephone','bank','bank_ag','account_type','bank_cc','document_id','document_cpf','document_activity'];
  		
  		foreach($personal_data_fields as $field){
  			$dashboard->{$field} = "";
  		}
  		$dashboard->resp_name = "";
  		$dashboard->resp_cpf = "";
  		

  		if ($personal_data = $return->informations){
	  		foreach($personal_data as $data){
	  			$dashboard->{$data->key} = $data->value;
	  		}
	  		// $dashboard->responsible_name = $personal_data->name;
  			// $dashboard->responsible_cpf = $personal_data->cpf;
  		}  		
  		
  		if (isset($return->business_type)) $dashboard->business_type = $return->business_type;
  		$dashboard->payment_email_notification = StdFunctions::tag_true_false($return->payment_email_notification);  		
  		$dashboard->webapp_on_test_mode = StdFunctions::tag_true_false($return->webapp_on_test_mode);
  		$dashboard->auto_withdraw = StdFunctions::tag_true_false($return->auto_withdraw);
  		$dashboard->raw_auto_withdraw = $return->auto_withdraw;
  		$dashboard->raw_payment_email_notification = $return->payment_email_notification;
  		$dashboard->payment_email_notification_receiver = $return->payment_email_notification_receiver;
  		// if (isset($return->commissions->bank_slip_cents))  		
  		$dashboard->bank_slip_fee = isset($return->commissions->bank_slip_cents)? StdFunctions::currency_from_cents($return->commissions->bank_slip_cents) : "R$ 0,00";
  		$dashboard->credit_card_fee = isset($return->commissions->credit_card_cents)? StdFunctions::currency_from_cents($return->commissions->credit_card_cents) : "R$ 0,00";

  		$invoice_data = $this->total_faturas_geradas_no_mes();
  		$dashboard->this_month_invoice_created_count = $invoice_data['invoice_count'];
  		$dashboard->this_month_invoice_created_total = StdFunctions::currency_from_cents($invoice_data['invoice_total']);
  		
  		return $dashboard;

	}

	public function total_faturas_geradas_no_mes(){				

		$parms = [
			'created_at_from' => date_format(date_create(),"Y-m-")."01T00:00:00-03:00",
			'created_at_to' => date_format(date_create(),"Y-m-t")."T23:59:59-03:00"
		];

		$result = json_decode($this->callIuguApi('GET','invoices',$parms));
		
		$return = [
			'invoice_count' => $result->totalItems,
			'invoice_total' => 0			
		];		

		foreach($result->items as $item){			
			$return['invoice_total']+= $item->total_cents;
		}		
		
		return $return;

	}

	public function bank_change_requests(){

		$return = $this->callIuguApi("GET","bank_verification");
		return json_decode($return);

	}

	public function bank_change($bank_data){

		$return = $this->callIuguApi("POST","bank_verification",$bank_data);
		return json_decode($return);
		
	}

	public function withdraw($request){

		$account_data = auth()->user()->customer()->first();
		$account_id = $account_data->account_id;
		// dd($account_data);

		$parms = [
			'amount' => StdFunctions::valor_em_float($request->withdraw_value)
		];

		$return = $this->callIuguApi('POST',"accounts/{$account_id}/request_withdraw",$parms);

		return json_decode($return);

	}

	public function relFaturas($parms){

		$result = $this->callIuguApi('GET','accounts/invoices',$parms);
		
		$result_body = json_decode($result);

		$faturas = array();
		
		foreach($result_body->items as $items){
			$fatura = array();			
			$fatura['criacao'] = StdFunctions::format_date($items->created_at);
			$fatura['cliente'] = $items->customer_name;			
			$fatura['vencimento'] = StdFunctions::format_date($items->due_date);
			$fatura['pagamento'] = StdFunctions::format_date($items->paid_at);
			$fatura['valor_pago'] = $items->paid_value;
			$fatura['valor_pendente'] = $items->pending_value;
			// $fatura['taxas'] = $items->taxes_paid;			
			$url = route('can.faturaDetalhe',['id'=> $items->id]);
			$fatura['id'] = "<a href='{$url}'><span class='glyphicon glyphicon-search green_tag'></a>";
			$faturas[] = $fatura;
		}		
        
		return $faturas;

	}

	public function relatorioFinanceiro(){

		$retorno = $this->callIuguApi('GET','accounts/financial');		
		
		// $retorno = '{
		// 	    "transactions": [
		// 	        {
		// 	            "amount": "99.90 BRL",
		// 	            "type": "credit",
		// 	            "description": "Fatura 4D6859EF38C14F42A694193F65FCFA0A Parcela: Única",
		// 	            "entry_date": "2017-06-27",
		// 	            "reference": "4D6859EF38C14F42A694193F65FCFA0A",
		// 	            "reference_type": "Invoice",
		// 	            "account_id": "57FD9F82ED004D959497226A196ED654",
		// 	            "transaction_type": "invoice_return",
		// 	            "balance": "99.90 BRL",
		// 	            "customer_ref": "João"
		// 	        },
		// 	        {
		// 	            "amount": "-4.79 BRL",
		// 	            "type": "debit",
		// 	            "description": "Tarifas - Fatura 4D6859EF38C14F42A694193F65FCFA0A Parcela: Única",
		// 	            "entry_date": "2017-06-27",
		// 	            "reference": "4D6859EF38C14F42A694193F65FCFA0A",
		// 	            "reference_type": "Invoice",
		// 	            "account_id": "57FD9F82ED004D959497226A196ED654",
		// 	            "transaction_type": "invoice_return",
		// 	            "balance": "95.11 BRL",
		// 	            "customer_ref": "José"
		// 	        },
		// 	        {
		// 	            "amount": "99.90 BRL",
		// 	            "type": "credit",
		// 	            "description": "Fatura C4E1B42BBC5146ACAAB27B03ED5D34F8 Parcela: Única",
		// 	            "entry_date": "2017-06-27",
		// 	            "reference": "C4E1B42BBC5146ACAAB27B03ED5D34F8",
		// 	            "reference_type": "Invoice",
		// 	            "account_id": "57FD9F82ED004D959497226A196ED654",
		// 	            "transaction_type": "invoice_return",
		// 	            "balance": "195.01 BRL",
		// 	            "customer_ref": "José"
		// 	        },
		// 	        {
		// 	            "amount": "-4.79 BRL",
		// 	            "type": "debit",
		// 	            "description": "Tarifas - Fatura C4E1B42BBC5146ACAAB27B03ED5D34F8 Parcela: Única",
		// 	            "entry_date": "2017-06-27",
		// 	            "reference": "C4E1B42BBC5146ACAAB27B03ED5D34F8",
		// 	            "reference_type": "Invoice",
		// 	            "account_id": "57FD9F82ED004D959497226A196ED654",
		// 	            "transaction_type": "invoice_return",
		// 	            "balance": "190.22 BRL",
		// 	            "customer_ref": "João"
		// 	        },
		// 	        {
		// 	            "amount": "-3.42 BRL",
		// 	            "type": "debit",
		// 	            "description": "Antecipação de Recebíveis - R$ 190,22 - custo: R$ 3,42",
		// 	            "entry_date": "2017-06-27",
		// 	            "reference": null,
		// 	            "reference_type": null,
		// 	            "account_id": "57FD9F82ED004D959497226A196ED654",
		// 	            "transaction_type": "advance",
		// 	            "balance": "186.80 BRL",
		// 	            "customer_ref": ""
		// 	        }
		// 	    ],
		// 	    "initial_balance": {
		// 	        "amount": "0.00 BRL",
		// 	        "entry_date": "2017-05-28"
		// 	    },
		// 	    "initial_date": "2017-05-28T00:00:00-03:00",
		// 	    "final_date": "2017-06-27T16:10:24-03:00"
		// 	}';

		$retorno_array = json_decode($retorno);

		$lancamento = array();
		$lancamento = [];
		$lancamento['data'] = StdFunctions::format_date($retorno_array->initial_balance->entry_date);		
		$lancamento['cliente'] = "";
		$lancamento['descricao'] = "Saldo anterior";
		$lancamento['valor'] = "";
		$lancamento['saldo'] = StdFunctions::format_brl_currency($retorno_array->initial_balance->amount);
		$lancamentos[] = $lancamento;

		foreach($retorno_array->transactions as $transaction){
			$lancamento = [];
			$lancamento['data'] = StdFunctions::format_date($transaction->entry_date);			
			$lancamento['cliente'] = $transaction->customer_ref;
			$lancamento['descricao'] = $transaction->description;
			$lancamento['valor'] = StdFunctions::format_brl_currency($transaction->amount);
			$lancamento['saldo'] = StdFunctions::format_brl_currency($transaction->balance);
			$lancamentos[] = $lancamento;
		}

		return $lancamentos;

	}

	public function general_settings_update($config){

		$retorno = $this->callIuguApi('POST','accounts/configuration',$config);
		
		return;
	}



 	

	private function payment_method($method = 'not_selected'){

		$methods = [
		'bank_slip'				=> "<span class='fatura-status-pago'>Boleto Bancário</span>",
		'iugu_bank_slip_test'	=> "<span class='fatura-status-pago'>Boleto Bancário</span>",
		'credit_card'			=> "<span class='fatura-status-expirado'>Cartão de crédito</span>",
		'not_selected'			=> "<span class='fatura-status-atraso'>Não selecionado</span>",
		];

		if (!$method) return $methods['not_selected'];

		return $methods[$method];

	}   	

	private function arrayToParams($array, $prefix = null){
        if (!is_array($array)) {
            return $array;
        }
        $params = [];
        foreach ($array as $k => $v) {
            if (is_null($v)) {
                continue;
            }
            if ($prefix && $k && !is_int($k)) {
                $k = $prefix.'['.$k.']';
            } elseif ($prefix) {
                $k = $prefix.'[]';
            }
            if (is_array($v)) {
                $params[] = self::arrayToParams($v, $k);
            } else {
                $params[] = $k.'='.urlencode((string)$v);
            }
        }
        return implode('&', $params);
    }	

    private function verificada($value){
    	if ($value)
    		return "<span style='background-color:green;color:white;padding:3px 5px;border-radius:3px'>VERIFICADA</span>";
		return "<span style='background-color:gray;color:white;padding:3px 5px;border-radius:3px'>NÃO VERIFICADA</span>";    	
    }

}
?>