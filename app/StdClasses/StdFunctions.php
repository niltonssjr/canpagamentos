<?php 
namespace App\StdClasses;	

use Gate;

class StdFunctions{

	public static function AllowAdmin(){
		if (!Gate::allows('isAdmin')) abort(404,'Página inacessível');
		return;
	}

	public static function DenyInactive(){

		if (Gate::allows('isInactive')) abort(404,'Função indisponível');//return redirect()->route('indisponivel');
		return;
	}

	public static function ApiIsActive(){
		if (Gate::allows('isInactive')) return false;
		return true;
	}

	public static function format_brl_currency($value){

		$return = str_replace(['BRL'],[''],$value);
		if ((float)$return < 0){
			$return = "<span style='color:red'>-R$ " . str_replace('-','',$return) . "</span>";
		} else {
			$return = "R$ " . $return;
		}
		$return = str_replace('.',',',$return);
		return $return;

	}

	public static function format_null_currency($value){

		return ($value)?:"R$ 0,00";

	}

	public static function float_to_currency($value){

		if (!$value) return "R$ 0,00";

		$currency_value = (float)$value;

		$currency_value = "R$ ".number_format($currency_value,2,",",".");

		return $currency_value;				
	}

	public static function currency_from_cents($value)	{

		if (!$value) return "R$ 0,00";

		$currency_value = $value / 100;

		$currency_value = "R$ ".number_format($currency_value,2,",",".");

		return $currency_value;		

	}

	public static function valor_em_centavos($valor){

		$valor_orig = str_replace(['.',','],['','.'],$valor);			
		if ($valor_orig){
			$valor = (float)$valor_orig * 100;
			$valor = (integer)$valor;
		}

		return $valor;
	}

	public static function valor_em_float($valor){

		return str_replace(['R$','.',','],['','','.'],$valor);

	}

	public static function format_date($date){
		if (!$date) return "";
		$date_obj = date_create($date);
		return date_format($date_obj,'d/m/Y');
	}

	public static function format_datetime($date){
		$date_obj = date_create($date);
		return date_format($date_obj,'d/m/Y H:i:s');
	}

	public static function status_verif_account($status){
		$format = [
			null => "<span class='gray_tag'>Não verificada</span>",
			'pending' => "<span class='orange_tag'>Pendente</span>",
			'approved' => "<span class='green_tag'>Aprovada</span>",
			'rejected' => "<span class='red_tag'>Aprovada</span>",
			'accepted' => "<span class='green_tag'>Aceita</span>"
		];
		return $format[$status];
	}

	public static function tag_true_false($status){
		if ($status) return "<span class='green_tag'>Sim</span>";
		return "<span class='red_tag'>Não</span>";
	}

	public static function verify_invoice_items($request){

		$inds=[];
		$items=[];

		foreach($request->all() as $key=>$item){
			if (strpos($key,'cus_fatura_desc')!== false){
				$inds[] = str_replace("cus_fatura_desc_","",$key);				
			}
		}

		return $inds;
	}

	public static function invoice_items_validator($request){

		$inds = self::verify_invoice_items($request);

		$field_desc = "cus_fatura_desc_";
		$field_qtd = "cus_fatura_qtd_";
		$field_valor = "cus_fatura_valor_";

		$rules = [];
		$messages = [];

		foreach($inds as $ind){
			
			$rules[$field_desc.$ind] = 'required';
			$rules[$field_qtd.$ind] = 'required';
			$rules[$field_valor.$ind] = 'required';

			$messages[$field_desc.$ind.".required"] = "Preencha a descrição do item {$ind} ou apague toda a linha";
			$messages[$field_qtd.$ind.".required"] = "Preencha a quantidade do item {$ind} ou apague toda a linha";
			$messages[$field_valor.$ind.".required"] = "Preenchao valor unitário do item {$ind} ou apague toda a linha";

		}

		return ['rules'=>$rules,'messages'=>$messages];

	}

	public static function fatura_traduz_status($status){

		$status_tab = [
		'paid'		=> "paga",
		'expired'	=> "expirada",
		'pending'	=> "pendente",
		'canceled'	=> "cancelada",
		];

		$return = (isset($status_tab[$status])) ? $status_tab[$status] : "indisponível";

		return $return;
	}  	

	public static function fatura_format_status($status){

		$status_tab = [
		'paid'		=> "<span class='fatura-status-pago'>Pago</span>",
		'expired'	=> "<span class='fatura-status-expirado'>Expirado</span>",
		'pending'	=> "<span class='fatura-status-pendente'>Pendente</span>",
		'canceled'	=> "<span class='fatura-status-cancelado'>Cancelado</span>",
		];

		$return = (isset($status_tab[$status])) ? $status_tab[$status] : "<span class='fatura-status-desconhecido'>Desconhecido</span>";

		return $return;
	}  	

	public static function saque_format_status($status){

		$status_tab = [
		'paid'		=> "<span class='fatura-status-pago'>Pago</span>",
		'expired'	=> "<span class='fatura-status-expirado'>Expirado</span>",
		'pending'	=> "<span class='fatura-status-pendente'>Pendente</span>",
		'canceled'	=> "<span class='fatura-status-cancelado'>Cancelado</span>",
		];

		if (isset($status_tab[$status])){
			$return = $status_tab[$status];
		} else {
			$return = "<span class='fatura-status-desconhecido'>$status</span>";
		}		

		return $return;
	}  

	public static function fatura_traduz_payment_method($method = 'not_selected'){

		$methods = [
		'bank_slip'				=> "boleto bancário",
		'iugu_bank_slip_test'	=> "boleto bancário de teste",
		'iugu_bank_slip'		=> "boleto bancário",
		'credit_card'			=> "cartão de crédito",
		'not_selected'			=> "não selecionado",
		];

		if (!$method) return $methods['not_selected'];

		return $methods[$method];

	}   		

	public static function fatura_format_payment_method($method = 'not_selected'){

		$methods = [
		'bank_slip'				=> "<span class='fatura-status-pago'>Boleto Bancário</span>",
		'iugu_bank_slip'		=> "<span class='fatura-status-pago'>Boleto Bancário</span>",
		'iugu_bank_slip_test'	=> "<span class='fatura-status-pago'>Boleto Bancário</span>",
		'credit_card'			=> "<span class='fatura-status-expirado'>Cartão de crédito</span>",
		'not_selected'			=> "<span class='fatura-status-atraso'>Não selecionado</span>",
		];

		if (!$method) return $methods['not_selected'];

		return $methods[$method];

	}   			

	public static function dd_api($content){
		ob_start();
		echo "<pre>";
		print_r($content);
		echo "</pre>";
		$ret = ob_get_contents();
		ob_end_clean();
		return response($ret,442);
	}
}

?>