<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Models\Customer;
use DB;
use App\StdClasses\IuguApi;
use App\StdClasses\StdFunctions;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','image', 'level', 'status', 'oper_mode'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function customer(){
        return $this->hasOne(Customer::class);
    }

    public function updatePassword($request){
        
        $this->password = bcrypt($request->pw_password);
        $return = $this->save();
        return $return;

    }

    public function inativar(){

        $account_id = $this->customer()->first()->account_id;
        
        $iugu = new IuguApi;
        $new_account_info = $iugu->subconta_inativar($account_id);        

        $this->status = 'Cancelled';        
        $return = $this->save();

        return $return;

    }

    public static function inativar_apenas_iugu($id){
        //rotina criada exclusivamente para inativar conta criada com erro de cadastramento
        $iugu = new IuguApi;
        $retorno = $iugu->subconta_inativar($id);        
        return $retorno;
    }

    public function insere($request){

        $email_parts = explode("@",$request->subconta_email);
        $temp_password = $email_parts[0];
        $adm_data = auth()->user()->customer()->first();
        $adm_account_id = $adm_data->account_id;
        
        DB::beginTransaction();

        $new_user = $this->create([
                'name' => $request->subconta_nome,
                'email' => $request->subconta_email,
                'password' => bcrypt($temp_password),
                'status' => 'Active',
                'oper_mode' => 'LIVE'
            ]);

        if (!$new_user) return ['error' => 'Falha ao incluir o usuário'];        
        
        $iugu = new IuguApi;
        $new_account_info = $iugu->subconta_nova($request);

        $new_customer = $new_user->customer()->create([
                'user_id' => $this->id,
                'created_by' => auth()->user()->id,
                'account_id' => $new_account_info->account_id,
                'name' => $new_account_info->name,
                'live_api_token' => $new_account_info->live_api_token,
                'test_api_token' => $new_account_info->test_api_token,
                'user_token' => $new_account_info->user_token,
                'oper_mode' => 'LIVE',
                'parent_account_id' => $adm_account_id
            ]);

        if (!$new_customer) return ['error' => 'Falha ao incluir o cliente'];

        $new_user->createToken('paguecan_api')->accessToken;
        $new_user->save();

        DB::commit();

        return;

    }
}
