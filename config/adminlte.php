<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Title
    |--------------------------------------------------------------------------
    |
    | The default title of your admin panel, this goes into the title tag
    | of your page. You can override it per page with the title section.
    | You can optionally also specify a title prefix and/or postfix.
    |
    */

    'title' => 'PAGUECAN',

    'title_prefix' => '',

    'title_postfix' => '',

    /*
    |--------------------------------------------------------------------------
    | Logo
    |--------------------------------------------------------------------------
    |
    | This logo is displayed at the upper left corner of your admin panel.
    | You can use basic HTML here if you want. The logo has also a mini
    | variant, used for the mini side bar. Make it 3 letters or so
    |
    */

    'logo' => '<b>PAGUECAN</b>',

    'logo_mini' => '<b>CAN</b>',

    /*
    |--------------------------------------------------------------------------
    | Skin Color
    |--------------------------------------------------------------------------
    |
    | Choose a skin color for your admin panel. The available skin colors:
    | blue, black, purple, yellow, red, and green. Each skin also has a
    | ligth variant: blue-light, purple-light, purple-light, etc.
    |
    */

    'skin' => 'green',

    /*
    |--------------------------------------------------------------------------
    | Layout
    |--------------------------------------------------------------------------
    |
    | Choose a layout for your admin panel. The available layout options:
    | null, 'boxed', 'fixed', 'top-nav'. null is the default, top-nav
    | removes the sidebar and places your menu in the top navbar
    |
    */

    'layout' => null,

    /*
    |--------------------------------------------------------------------------
    | Collapse Sidebar
    |--------------------------------------------------------------------------
    |
    | Here we choose and option to be able to start with a collapsed side
    | bar. To adjust your sidebar layout simply set this  either true
    | this is compatible with layouts except top-nav layout option
    |
    */

    'collapse_sidebar' => false,

    /*
    |--------------------------------------------------------------------------
    | URLs
    |--------------------------------------------------------------------------
    |
    | Register here your dashboard, logout, login and register URLs. The
    | logout URL automatically sends a POST request in Laravel 5.3 or higher.
    | You can set the request to a GET or POST with logout_method.
    | Set register_url to null if you don't want a register link.
    |
    */

    'dashboard_url' => 'admin/dashboard',

    'logout_url' => 'logout',

    'logout_method' => null,

    'login_url' => 'login',

    // 'register_url' => 'register',
    'register_url' => null,

    /*
    |--------------------------------------------------------------------------
    | Menu Items
    |--------------------------------------------------------------------------
    |
    | Specify your menu items to display in the left sidebar. Each menu item
    | should have a text and and a URL. You can also specify an icon from
    | Font Awesome. A string instead of an array represents a header in sidebar
    | layout. The 'can' is a filter on Laravel's built in Gate functionality.
    |
    */

    'menu' => [
        'OPÇÕES GERAIS',
        [
            'text'          => 'Dashboard',
            'url'           => 'admin/dashboard',
            'icon'          => 'circle',
        ],       
        [
            'text'        => 'Faturas',
            'url'         => 'admin/undercon',
            'icon'        => 'circle', 
            'submenu' => [
                [
                    'text' => 'Emissão de fatura',
                    'url'  => 'admin/cus/novafatura',
                    'icon' => 'circle',
                ],
                [
                    'text' => 'Todas as faturas',
                    'url'  => 'admin/cus/faturas',
                    'icon' => 'circle',
                ],                
            ],
        ],          
         
        [
            'text'        => 'Solicitação de saque',
            'url'         => 'admin/withdraw',
            'icon'        => 'circle',            
        ],     
[
            'text'        => 'Clientes',
            'url'         => 'admin/undercon',
            'icon'        => 'users', 
            'submenu' => [
                [
                    'text' => 'Novo cliente',
                    'url'  => 'admin/cus/novocliente',
                    'icon' => 'circle',
                ],
                [
                    'text' => 'Todos os clientes',
                    'url'  => 'admin/cus/clientes',
                    'icon' => 'circle',
                ],                
            ],
        ], 
        [
            'text'        => 'Extratos',
            'url'         => 'admin/undercon',
            'icon'        => 'circle', 
            'submenu' => [
                [
                    'text' => 'Financeiro',
                    'url'  => 'admin/relatoriofinanceiro',
                    'icon' => 'circle',
                ],
                [
                    'text' => 'Faturas',
                    'url'  => 'admin/relatoriofaturas',
                    'icon' => 'circle',
                ],                
                [
                    'text' => 'Transferências bancárias',
                    'url'  => 'admin/transferencias',
                    'icon' => 'circle',
                ],                  
            ],
        ],
        [
            'text'          => 'Sub-Contas',
            'url'           => 'admin/undercon',
            'icon'          => 'circle',
            'can'           => 'isAdmin',
            'submenu'       => [
                [
                    'text'  => 'Nova sub-conta',
                    'url'   => 'admin/can/subcontanova',
                    'icon'  => 'user'
                ],
                [
                    'text'  => 'Todas as sub-contas',
                    'url'   => 'admin/can/subcontas',
                    'icon'  => 'users'
                ],                
            ]
        ],       
       
        'CONFIGURAÇÕES',
        [
            'text'          => 'Configurações',
            'url'           => 'admin/undercon',
            'icon'          => 'user',
            'submenu' => [
                [
                    'text' => 'Gerais',
                    'url'  => 'admin/config-gerais',
                    'icon' => 'circle',
                ],
                [
                    'text' => 'Bancos',
                    'url'  => 'admin/config-bancos',
                    'icon' => 'circle',
                ],
                [
                    'text' => 'API REST',
                    'url'  => 'admin/userapitoken',
                    'icon' => 'circle',
                ],                                
            ],            
        ],         
        [
            'text' => 'Alteração de senha',
            'url'  => 'admin/password',
            'icon' => 'lock',
        ],        
    ],

    /*
    |--------------------------------------------------------------------------
    | Menu Filters
    |--------------------------------------------------------------------------
    |
    | Choose what filters you want to include for rendering the menu.
    | You can add your own filters to this array after you've created them.
    | You can comment out the GateFilter if you don't want to use Laravel's
    | built in Gate functionality
    |
    */

    'filters' => [
        JeroenNoten\LaravelAdminLte\Menu\Filters\HrefFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\ActiveFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\SubmenuFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\ClassesFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\GateFilter::class,
    ],

    /*
    |--------------------------------------------------------------------------
    | Plugins Initialization
    |--------------------------------------------------------------------------
    |
    | Choose which JavaScript plugins should be included. At this moment,
    | only DataTables is supported as a plugin. Set the value to true
    | to include the JavaScript file from a CDN via a script tag.
    |
    */

    'plugins' => [
        'datatables' => true,
        'select2'    => true,
        'chartjs'    => true,
    ],
];
