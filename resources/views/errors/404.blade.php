@extends('adminlte::page')

@section('title', 'Página não encontrada')

@section('content_header')
<h1>Página não encontrada</h1>
@stop

@section('content')   
<div class="row">
	<div class='col-lg-6 col-md-6 col-sm-6 col-xs-12'> 
		@include ('includes.alerts')
	</div>
</div>
<div class="row">
	<div class='col-lg-6 col-md-6 col-sm-6 col-xs-12'>
		<div class='box-content'>
			<p>A página que você procura não existe ou está indisponível no momento. Se você acredita que isso está ocorrendo indevidamente, entre em contato com a PagueCan.</p>
			@if ($exception->getMessage())
				<h4>{{ $exception->getMessage()}}</h4>
			@endif
		</div>
	</div>		
</div>
@stop

@section('css')
<link rel="stylesheet" href='{{ url("/") }}/css/cantarino.css'>
@stop
