@extends('adminlte::page')

@section('title', 'Detalhes do cliente')

@section('content_header')
<h1>Detalhes do cliente</h1>
@stop

@php

$linhas = [];

foreach($cliente->faturas as &$fatura){
    $fatura['status'] = $fatura['format_status'];

    $url_details = route('can.faturaDetalhe',['id'=> $fatura['id']]);
    $action_details = "<a href='{$url_details}'><span class='glyphicon glyphicon-search green_tag'></span></a>";

    $action_cancel = "";

    if ($fatura['status'] == 'pending'){        
        $id = $fatura['id'];
        $url_cancel = "javascript:delete_confirmation('{$id}')";
        $action_cancel = "<a href={$url_cancel}><span class='glyphicon glyphicon-trash red_tag'></span></a>";                   
    }
    
    $fatura['actions'] = $action_details . " " . $action_cancel;    

}

@endphp

@section('content')    
	<div class='box-content'>
    	<table class='table table-striped'>
            <tr>
                <th class='col-md-3'>Nome: </th><td>{{ $cliente->name }}</td>
            </tr>         
    		<tr>
    			<th>Código: </th><td>{{ $cliente->id }}</td>
    		</tr>
    		<tr>
    			<th>Email: </th><td>{{ $cliente->email }}</td>
    		</tr>
            <tr>
                <th>CPF / CNPJ: </th><td>{{ $cliente->cpf_cnpj }}</td>
            </tr>    				
            <tr>
                <th>Telefone: </th><td>{{ $cliente->phone_prefix . "-" . $cliente->phone }}</td>
            </tr>                        
    	</table>
    </div>
    <div class=''>
    	<h3>Endereço</h3>
    </div>
    <div class='box-content'>
        <table class='table table-striped'>
            <tr>
                <th class='col-md-3'>Logradouro: </th><td>{{ $cliente->address_street }}</td>
            </tr>         
            <tr>
                <th>Número: </th><td>{{ $cliente->address_number }}</td>
            </tr>
            <tr>
                <th>Complemento: </th><td>{{ $cliente->address_others }}</td>
            </tr>
            <tr>
                <th>CEP: </th><td>{{ $cliente->address_zip_code }}</td>
            </tr>                   
            <tr>
                <th>Bairro: </th><td>{{ $cliente->address_district }}</td>
            </tr> 
            <tr>
                <th>Cidade: </th><td>{{ $cliente->address_city }}</td>
            </tr>                        
            <tr>
                <th>Estado: </th><td>{{ $cliente->address_state }}</td>
            </tr>            
        </table>
    </div>  
    <div class=''>
        <h3>Faturas</h3>
    </div>      
    <div class="box-content">
        <table class='table table-striped'>
            <thead>
                <tr>
                    <th>Vencimento</th>
                    <th>Status</th>
                    <th>Total</th>
                    <th>+</th>
                </tr>
            </thead>
            <tbody>
                @forelse ($cliente->faturas as $fatura)
                    <tr>
                        <td>{{ $fatura['vencimento'] }}</td>
                        <td>{!! $fatura['status'] !!}</td>
                        <td>{{ $fatura['valor'] }}</td>
                        <td>{!! $fatura['actions'] !!}</td>
                    </tr>
                @empty
                <tr><td colspan=3>Não há faturas para este cliente</td></tr>
                @endforelse
            </tbody>
        </table>
    </div>
    <br/><a class='btn btn-primary' href="{{ URL::previous() }}">Voltar</a>
@stop

@section('css')
    <link rel="stylesheet" href='{{ url("/") }}/css/cantarino.css'>
@stop