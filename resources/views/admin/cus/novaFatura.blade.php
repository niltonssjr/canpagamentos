@extends('adminlte::page')

@section('title', 'Nova Fatura')

@section('content_header')
<h1>Nova fatura</h1>
@stop

@section('content')
@include('includes.alerts')
<form action="{{ route('cus.novafatura.store') }}" method='post'>
	{!! csrf_field() !!}
	<div class='box-content'>
		<div class='row'>
			<div class='col-md-2'>
				<div class="form-group">
					<label for='cus_fatura_vencimento'>Vencimento:</label>
					<input type="text" name="cus_fatura_vencimento" id="cus_fatura_vencimento" class='form-control as_mask_date' value="{{ old('cus_fatura_vencimento') }}">
				</div>
			</div>
		</div>
	</div>
	<h3>Juros e multa</h3>
	<div class='box-content'>
		<div class='row'>
			<div class='col-md-4'>				
				<div class="form-group">					
					<input type="checkbox" name="cus_fatura_juros_multa_aplicar" id="cus_fatura_juros_multa_aplicar">
					<label for='cus_fatura_juros_multa_aplicar'>Aplicar multa em caso de atraso no pagamento</label><br/>
				</div>
				<div class="form-group">					
					<label for='cus_fatura_usar_juros_multa_padrão'>Percentual (%)</label>	
					<input type="text" name="cus_fatura_juros_multa_valor" id="cus_fatura_juros_multa_valor" class='form-control as_mask_percentage cus_fatura_multa_aplicada'><br/>						
				</div>
				<div class="form-group">					
					<input type="checkbox" name="cus_fatura_juros_prorata_aplicar" id="cus_fatura_juros_prorata_aplicar" class="cus_fatura_multa_aplicada" value="padrao">
					<label for='cus_fatura_juros_prorata_aplicar'>Aplicar juros diários (1% a.m.)</label><br/>						
				</div>				
			</div>
		</div>		
	</div>	
	<h3>Items</h3>
	<div class='box-content'>
		<div class="row">
			<div class='col-md-12'>
				<input type="button" value="Adicionar item" id='btn_adicionar_item' class='btn btn-success'/><br/><br/>
			</div>
		</div>
		<div class='row' id='div_itens'>
			<div class='col-md-12'>
				<table class='table table-striped' id="table_itens">
					<thead>
						<tr>							
							<th class='col-md-7'>Descrição</th>
							<th class='col-md-2'>QTD</th>
							<th class='col-md-2'>valor</th>
							<th class='col-md-1'>+</th>
						</tr>
					</thead>
					<tbody>
						<tr>							
							<td>								
								<input type="text" name="cus_fatura_desc_1" class='form-control item_desc ' placeholder='Descrição do item 1' value="{{ old('cus_fatura_desc_1') }}">
							</td>
							<td>
								<input type="text" name="cus_fatura_qtd_1" class='form-control item_qtd as_mask_small_qtd' placeholder='Quantidade do item 1' value="{{ old('cus_fatura_qtd_1') }}">
							</td>
							<td>
								<div class="input-group">
									<span class="input-group-addon">R$</span>
									<input type="text" name="cus_fatura_valor_1" class='form-control item_value as_mask_money' placeholder='Valor do item 1' value="{{ old('cus_fatura_valor_1') }}">
								</div>	
							</td>
							<td>
								<button class='btn btn-danger item_btn'>
									<i class="glyphicon glyphicon-trash"></i>
								</button>	
							</td>
						</tr>
						@for ($x=2; $x<1000; $x++)
						@php 
						$desc = "cus_fatura_desc_".$x;
						$qtd = "cus_fatura_qtd_".$x;
						$value= "cus_fatura_valor_".$x;
						@endphp
						@if(old($desc) || old($qtd) || old($value))
						<tr>							
							<td>								
								<input type="text" name="cus_fatura_desc_{{$x}}" class='form-control item_desc ' placeholder='Descrição do item {{$x}}' value="{{ old('cus_fatura_desc_'.$x) }}">
							</td>
							<td>
								<input type="text" name="cus_fatura_qtd_{{$x}}" class='form-control item_qtd as_mask_small_qtd' placeholder='Quantidade do item {{$x}}' value="{{ old('cus_fatura_qtd_'.$x) }}">
							</td>
							<td>
								<div class="input-group">
									<span class="input-group-addon">R$</span>
									<input type="text" name="cus_fatura_valor_{{$x}}" class='form-control item_value as_mask_money' placeholder='Valor do item {{$x}}' value="{{ old('cus_fatura_valor_'.$x) }}">
								</div>	
							</td>
							<td>
								<button class='btn btn-danger item_btn'>
									<i class="glyphicon glyphicon-trash"></i>
								</button>	
							</td>
						</tr>							
						@endif
						@endfor																				
					</tbody>				
				</table>	
			</div>					
		</div>
	</div>	
	<h3>Pesquisar cliente</h3>
	<div class='box-content'>
		<div class="row">
			<div class='col-md-6'>		
				<div class="form-group">
					<label for='cus_fatura_pagador_busca_cliente'>Informação para pesquisa:</label>
					<div class="input-group">						
						<input type="text" class="form-control" name='cus_fatura_pagador_busca_cliente' id='cus_fatura_pagador_busca_cliente' value="{{ old('cus_fatura_pagador_busca_cliente') }}">
						<span class="input-group-btn">
							<button class="btn btn-primary" id='btn_search_cliente' type="button"><i class="glyphicon glyphicon-search"></i></button>
						</span>
					</div>							
				</div>
			</div>	
		</div>
		<div class="row" id="customers_list">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">				
				<table class="table table-striped table-condensed table-bordered">
					<thead>
						<th>Nome</th>
						<th>CPF</th>
						<th>Email</th>
						<th></th>
					</thead>
					<tbody id="customers_list_tbody">
						
					</tbody>
				</table>
			</div>
		</div>	
	</div>
	<h3>Informações sobre o pagador</h3>
	<div class='box-content'>	
		<div class="row">
			<div class='col-md-6'>		
				<div class="form-group">
					<label for='cus_fatura_pagador_email'>Email:</label>
					<div class="form-group">						
						<input type="email" class="form-control" name='cus_fatura_pagador_email' id='cus_fatura_pagador_email' value="{{ old('cus_fatura_pagador_email') }}">
					</div>							
				</div>
			</div>	
		</div>
		<div class="row">
			<div class='col-md-12'>
				<div class="form-group">
					<label for='cus_fatura_pagador_nome'>Nome</label>
					<input type="text" name="cus_fatura_pagador_nome" id="cus_fatura_pagador_nome" class='form-control' placeholder='Nome do pagador' value="{{ old('cus_fatura_pagador_nome') }}">
					<input type="hidden" name="cus_fatura_pagador_id" id="cus_fatura_pagador_id" value="{{ old('cus_fatura_pagador_id')}}">
				</div>
			</div>
		</div>
		<div class="row">
			<div class='col-md-3'>
				<div class="form-group">
					<label for='cus_fatura_person_type'>Tipo de pessoa:</label>
					<select class='form-control' name='cus_fatura_person_type' id='cus_fatura_person_type'>
						<option value='fis'>Pessoa física</option>
						<option value='jur' @if(old('cus_fatura_person_type')=='jur') SELECTED @endif>Pessoa jurídica</option>							
					</select>					
				</div>					
			</div>			
			<div class='col-md-3' id="cus_fatura_cpf_div">
				<div class="form-group">
					<label for='cus_fatura_pagador_cpf'>CPF:</label>
					<input type="text" name="cus_fatura_pagador_cpf" id="cus_fatura_pagador_cpf" class='form-control as_mask_cpf' value="{{ old('cus_fatura_pagador_cpf') }}">
				</div>
			</div>
			<div class='col-md-3' id="cus_fatura_cnpj_div">
				<div class="form-group">
					<label for='cus_fatura_pagador_cnpj'>CNPJ:</label>
					<input type="text" name="cus_fatura_pagador_cnpj" id="cus_fatura_pagador_cnpj" class='form-control as_mask_cnpj' value="{{ old('cus_fatura_pagador_cnpj') }}">
				</div>
			</div>			
			<div class='col-md-1'>
				<div class="form-group">
					<label for='cus_fatura_pagador_phone'>DDD:</label>
					<input type="text" name="cus_fatura_pagador_phone_prefix" id="cus_fatura_pagador_phone_prefix" class='form-control as_mask_phone_prefix' placeholder='(00)' value="{{ old('cus_fatura_pagador_phone_prefix') }}">
				</div>
			</div>
			<div class='col-md-5'>
				<div class="form-group">
					<label for='cus_fatura_pagador_phone'>Telefone:</label>
					<input type="text" name="cus_fatura_pagador_phone" id="cus_fatura_pagador_phone" class='form-control as_mask_phone_number' placeholder='0-0000-0000' value="{{ old('cus_fatura_pagador_phone') }}">
				</div>
			</div>
		</div>	
		<div class='row'>
			<div class='col-md-2'>		
				<div class="form-group">
					<label for='cus_fatura_pagador_address_zip_code'>CEP:</label>
					<div class="input-group">						
						<input type="text" class="form-control as_mask_zip_code" name='cus_fatura_pagador_address_zip_code' id='cus_fatura_pagador_address_zip_code' placeholder="00000-000" value="{{ old('cus_fatura_pagador_address_zip_code') }}">
						<span class="input-group-btn">
							<button class="btn btn-primary" id='btn_search_zip' type="button"><i class="glyphicon glyphicon-search"></i></button>
						</span>
					</div>							
				</div>
			</div>
		</div>
		<div class='row'>
			<div class='col-md-10'>
				<div class="form-group">
					<label for='cus_fatura_pagador_address_street'>Logradouro:</label>
					<input type="text" class='form-control' name="cus_fatura_pagador_address_street" id="cus_fatura_pagador_address_street" placeholder="Endereço" value="{{ old('cus_fatura_pagador_address_street') }} ">
				</div>					
			</div>
			<div class='col-md-2'>
				<div class="form-group">
					<label for='cus_fatura_pagador_address_number'>Nº:</label>
					<input type="text" class='form-control' name="cus_fatura_pagador_address_number" id="cus_fatura_pagador_address_number" placeholder="" value=" {{ old('cus_fatura_pagador_address_number') }} ">
				</div>					
			</div>				
		</div>
		<div class="row">
			<div class='col-md-6'>
				<div class="form-group">
					<label for='cus_fatura_pagador_address_others'>Complemento:</label>
					<input type="text" class='form-control' name="cus_fatura_pagador_address_others" id="cus_fatura_pagador_address_others" placeholder="Complemento" value=" {{ old('cus_fatura_pagador_address_others') }} ">
				</div>				
			</div>
			<div class='col-md-6'>
				<div class="form-group">
					<label for='cus_fatura_pagador_address_district'>Bairro:</label>
					<input type="text" class='form-control' name="cus_fatura_pagador_address_district" id="cus_fatura_pagador_address_district" placeholder="Bairro" value=" {{ old('cus_fatura_pagador_address_district') }} ">
				</div>				
			</div>			
		</div>


		<div class='row'>
			<div class='col-md-10'>
				<div class="form-group">
					<label for='cus_fatura_pagador_address_city'>Cidade:</label>
					<input type="text" class='form-control' name="cus_fatura_pagador_address_city" id="cus_fatura_pagador_address_city" placeholder="Cidade" value=" {{ old('cus_fatura_pagador_address_city') }} ">
				</div>					
			</div>
			<div class='col-md-2'>
				<div class="form-group">
					<label for='cus_fatura_pagador_address_state'>UF:</label>
					<input type="text" class='form-control' name="cus_fatura_pagador_address_state" id="cus_fatura_pagador_address_state" placeholder="" value="{{ old('cus_fatura_pagador_address_state') }}">
				</div>					
			</div>				
		</div>				
	</div>
	<div class='row'>
		<div class='col-md-12'>
			<br>
			<input type="submit" value="Criar" class='btn btn-primary'>	
		</div>		
	</div>
</form>
@stop

@section('css')
<link href="{{ url('/')}}/css/bootstrap-datepicker.min.css" rel="stylesheet">
<link rel="stylesheet" href='{{ url("/") }}/css/cantarino.css'>
@stop

@section('js')
<script src='{{ url("/") }}/js/jquery.mask.min.js'></script>
<script src='{{ url("/") }}/js/bootstrap-datepicker.min.js'></script>
<script src='{{ url("/") }}/js/bootstrap-datepicker.pt-BR.min.js'></script>
<script src='{{ url("/") }}/js/auge_masks.js'></script>
<script src='{{ url("/") }}/js/sweetalert2.all.js'></script>
<script type="text/javascript">

	$(document).ready(function(){

		//datepicker for due_date
		$("#cus_fatura_vencimento").datepicker({
			format: "dd/mm/yyyy",
			todayBtn: true,
			autoclose: true,
			language: "pt-BR",
			startDate: "1d"
		});

		switch_person_type($("#cus_fatura_person_type option").filter(":selected").val());			
		$("#cus_fatura_person_type").change(function(){
			switch_person_type($("#cus_fatura_person_type option").filter(":selected").val());			
		});		

		//load final customer data based on the email
		$("#btn_search_cliente").click(function(){
			listar_cliente();
		});

		$("#btn_search_zip").click(function(){
			search_zip_code();
		});

		$("#btn_adicionar_item").click(function() {
			add_lines_on_items_table();          
			return false;
		});		

		$(".item_btn").click(function(){        	
			remove_line_on_item_table(this);
			return false;
		});

		$('body').on('click','.btn_sel_cliente',function(){		
			selecionar_cliente(this.getAttribute('id'));
			return false;
		})

		switch_config_geral_multa($("#cus_fatura_juros_multa_aplicar").is(':checked'));		

		$("#cus_fatura_juros_multa_aplicar").change(function(){
			switch_config_geral_multa($("#cus_fatura_juros_multa_aplicar").is(':checked'));			
		});

	});

	function switch_config_geral_multa(checked){
		if (!checked){
			$(".cus_fatura_multa_aplicada").attr('disabled','disabled');
		} else {
			$(".cus_fatura_multa_aplicada").removeAttr('disabled');
		}
	}

	function switch_person_type(type){

		if (type=='fis'){
			$("#cus_fatura_cnpj_div").hide();			
			$("#cus_fatura_cpf_div").show();
		} else {
			$("#cus_fatura_cnpj_div").show();			
			$("#cus_fatura_cpf_div").hide();			
		}

	}
	// function pesquisar_cliente(){

	// 	cliente = $("#cus_fatura_pagador_email").val();
	// 	url = '/admin/cus/clienteEmail/'+cliente;
	// 	url = "{{ url('/admin/cus/clienteEmail') }}" + "/" + cliente;

	// 	$.get(url,function(cliente_data){	
	// 		console.log(cliente_data);
	// 		cliente = JSON.parse(cliente_data);			
	// 		if (!cliente.name) return false;	
	// 		console.log(cliente);

	// 		$("#cus_fatura_pagador_nome").val(cliente.name);

	// 		$("#cus_fatura_pagador_phone").val(cliente.phone);
	// 		$("#cus_fatura_pagador_phone_prefix").val(cliente.phone_prefix);
	// 		$("#cus_fatura_pagador_address_zip_code").val(cliente.address_zip_code);
	// 		$("#cus_fatura_pagador_address_street").val(cliente.address_street);
	// 		$("#cus_fatura_pagador_address_number").val(cliente.address_number);
	// 		$("#cus_fatura_pagador_address_others").val(cliente.address_others);
	// 		$("#cus_fatura_pagador_address_district").val(cliente.address_district);
	// 		$("#cus_fatura_pagador_address_city").val(cliente.address_city);
	// 		$("#cus_fatura_pagador_address_state").val(cliente.address_state);

	// 		cpf_cnpj = cliente.cpf_cnpj.replace(/[^0-9]/g,"");

	// 		if (cpf_cnpj.length==11){
	// 			type_person = "fis";
	// 			$("#cus_fatura_pagador_cpf").val(cliente.cpf_cnpj);		
	// 			switch_person_type("fis");
	// 		} else {
	// 			type_person = "jur";
	// 			$("#cus_fatura_pagador_cnpj").val(cliente.cpf_cnpj);						
	// 			switch_person_type("jur");
	// 		}

	// 		$("#cus_fatura_person_type option[value='"+type_person+"']").prop('selected',true);	

	// 		return false;
	// 	});
	// }
	function listar_cliente(){

		argumento = $("#cus_fatura_pagador_busca_cliente").val();

		// url = '/admin/cus/pesquisaCliente/'+argumento;
		url = "{{ url('/admin/cus/pesquisaCliente') }}" + "/" + argumento;
		console.clear();
		console.log(url);
		$.get(url,function(cliente_data){	

			var clientes = JSON.parse(cliente_data);
			console.clear();
			console.log(clientes);

			var customers_list_tbody = document.getElementById("customers_list_tbody");

			$("#customers_list").slideUp();

			customers_list_tbody.innerHTML="";
			if (clientes.length==0) return false;

			for (x in clientes){

				var linha = document.createElement("tr");

				var td_nome = document.createElement("td");
				td_nome.innerHTML = clientes[x].nome;
				linha.appendChild(td_nome);

				var td_cnpj = document.createElement("td");
				td_cnpj.innerHTML = clientes[x].cnpj;
				linha.appendChild(td_cnpj);

				var td_email = document.createElement("td");
				td_email.innerHTML = clientes[x].email;
				linha.appendChild(td_email);

				var td_button = document.createElement("td");
				var sel_button = document.createElement("button");
				sel_button.classList.add("btn");
				sel_button.classList.add("btn-primary");
				sel_button.classList.add("btn_sel_cliente");
				sel_button.setAttribute('type','button');
				sel_button.setAttribute('id',clientes[x].id);
				sel_button.innerHTML = "Selecionar";
				td_button.appendChild(sel_button);
				linha.appendChild(td_button);				

				customers_list_tbody.appendChild(linha);

			}
			
			$("#customers_list").slideDown();			

			return false;
		}).fail(function(data){
			console.log(data);
		});
	}

	function selecionar_cliente(id){		
		
		url = "{{ url('/admin/cus/clienteId') }}" + "/" + id;
		
		$.get(url,function(cliente_data){	
			console.log(cliente_data);
			cliente = JSON.parse(cliente_data);			
			if (!cliente.name) return false;	
			console.log(cliente);

			$("#cus_fatura_pagador_nome").val(cliente.name);
			
			$("#cus_fatura_pagador_phone").val(cliente.phone);
			$("#cus_fatura_pagador_phone_prefix").val(cliente.phone_prefix);
			$("#cus_fatura_pagador_address_zip_code").val(cliente.address_zip_code);
			$("#cus_fatura_pagador_address_street").val(cliente.address_street);
			$("#cus_fatura_pagador_address_number").val(cliente.address_number);
			$("#cus_fatura_pagador_address_others").val(cliente.address_others);
			$("#cus_fatura_pagador_address_district").val(cliente.address_district);
			$("#cus_fatura_pagador_address_city").val(cliente.address_city);
			$("#cus_fatura_pagador_address_state").val(cliente.address_state);
			$("#cus_fatura_pagador_email").val(cliente.email);
			$("#cus_fatura_pagador_id").val(cliente.id);

			cpf_cnpj = cliente.cpf_cnpj.replace(/[^0-9]/g,"");
			
			if (cpf_cnpj.length==11){
				type_person = "fis";
				$("#cus_fatura_pagador_cpf").val(cliente.cpf_cnpj);		
				switch_person_type("fis");
			} else {
				type_person = "jur";
				$("#cus_fatura_pagador_cnpj").val(cliente.cpf_cnpj);						
				switch_person_type("jur");
			}

			$("#cus_fatura_person_type option[value='"+type_person+"']").prop('selected',true);	
			$("#customers_list").slideUp();
			return false;
		});
	}

	function search_zip_code(){

		zip_code = $("#cus_fatura_pagador_address_zip_code").cleanVal().trim();
		console.log(zip_code);

		if (zip_code==""){
			swal('Cep obrigatório',"Informe o CEP para pesquisa",'warning');
			return false;
		}

		if (zip_code.length!=8){
			swal('Cep obrigatório',"O CEP informado não é válido",'warning');
			return false;	
		}

		url = 'http://viacep.com.br/ws/' + zip_code + '/json/';

		$.get(url,function(retorno){
			console.log(retorno);
			
			if (retorno.erro){
				swal('Cep não encontrado','O CEP informado não existe','warning');
				return false;
			}
			$("#cus_fatura_pagador_address_street").val(retorno.logradouro);	
			$("#cus_fatura_pagador_address_district").val(retorno.bairro);	
			$("#cus_fatura_pagador_address_city").val(retorno.localidade);
			$("#cus_fatura_pagador_address_state").val(retorno.uf);			
		});

	}

	function add_lines_on_items_table(){

		last_name = $("#table_itens tbody>tr:last .item_desc").attr('name').split("_");
		last_name_number = last_name[3];
		total_items = Number(last_name_number) + 1;

		linha = document.createElement("tr");
		$("#table_itens tbody").append(linha);
		td_desc = document.createElement('td');
		$('#table_itens tbody>tr:last').append(td_desc);
		$('#table_itens tbody>tr:last>td:last').append("<input type='text' name='cus_fatura_desc_"+total_items+"' class='form-control item_desc' placeholder='Descrição do item "+total_items+"'/>");
		td_qtd = document.createElement('td');
		$("#table_itens tbody>tr:last").append(td_qtd);
		$('#table_itens tbody>tr:last td:last').append("<input type='text' name='cus_fatura_qtd_"+total_items+"' class='form-control item_qtd' data-mask='000.000' placeholder='Quantidade do item "+total_items+"'/>");		
		td_qtd = document.createElement('td');
		$("#table_itens tbody>tr:last").append(td_qtd);
		$('#table_itens tbody>tr:last td:last').append("<div class='input-group'><span class='input-group-addon'>R$</span></div>");
		$('#table_itens tbody>tr:last td:last div.input-group').append("<input type='text' name='cus_fatura_valor_"+total_items+"' class='form-control item_valor' data-mask='000.000,00' data-mask-reverse='true' placeholder='Valor do item "+total_items+"'/>");		
		td_button = document.createElement("td");
		$("#table_itens tbody>tr:last").append(td_button);
		$('#table_itens tbody>tr:last td:last').append("<button type='button' class='btn btn-danger' onclick='remove_line_on_item_table(this);'><i class='glyphicon glyphicon-trash'></i></button>");
		$("#table_itens tbody>tr:last td:last button").addClass('item_btn');

		return false;
		
	}

	function remove_line_on_item_table(obj){
		
		total_items = $("#table_itens tbody tr").length;
		if (total_items==1){
			$(".item_desc").val("");
			$(".item_qtd").val("");
			$(".item_value").val("");
			return false;	
		} 
		line = $(obj).closest('tr');		
		line.remove();
		return false;
	}
</script>
@stop