@extends('adminlte::page')

@section('title', 'Novo cliente')

@section('content_header')
<h1>Atualização de cliente</h1>
@stop

@section('content')

<form method='post' action="{{ route('cus.clienteAtualiza.store')}}">
	@include('includes.formCliente')
</form>

@stop

@section('css')
<link href="{{ url('/')}}/css/bootstrap-datepicker.min.css" rel="stylesheet">
<link rel="stylesheet" href='{{ url("/") }}/css/cantarino.css'>
@stop

@section('js')
<script src='{{ url("/") }}/js/jquery.mask.min.js'></script>
<script src='{{ url("/") }}/js/bootstrap-datepicker.min.js'></script>
<script src='{{ url("/") }}/js/bootstrap-datepicker.pt-BR.min.js'></script>
<script src='{{ url("/") }}/js/auge_masks.js'></script>
<script src='{{ url("/") }}/js/sweetalert2.all.js'></script>
<script type="text/javascript">

	$(document).ready(function(){

		//load final customer data based on the email
		$("#cus_fatura_pagador_email").change(function(){
			pesquisar_cliente();
		});

		$("#btn_search_zip").click(function(){
			search_zip_code();
		});

		switch_person_type($("#cus_customer_person_type option").filter(":selected").val());			
		$("#cus_customer_person_type").change(function(){
			switch_person_type($("#cus_customer_person_type option").filter(":selected").val());			
		});		
	});

	function switch_person_type(type){

		if (type=='fis'){
			$("#cus_customer_cnpj_div").hide();			
			$("#cus_customer_cpf_div").show();
		} else {
			$("#cus_customer_cnpj_div").show();			
			$("#cus_customer_cpf_div").hide();			
		}

	}

	function search_zip_code(){

		zip_code = $("#cus_customer_address_zip_code").cleanVal().trim();
		console.log(zip_code);

		if (zip_code==""){
			swal('Cep obrigatório',"Informe o CEP para pesquisa",'warning');
			return false;
		}

		if (zip_code.length!=8){
			swal('Cep obrigatório',"O CEP informado não é válido",'warning');
			return false;	
		}

		url = 'http://viacep.com.br/ws/' + zip_code + '/json/';

		$.get(url,function(retorno){
			console.log(retorno);
			
			if (retorno.erro){
				swal('Cep não encontrado','O CEP informado não existe','warning');
				return false;
			}
			$("#cus_customer_address_street").val(retorno.logradouro);	
			$("#cus_customer_address_district").val(retorno.bairro);	
			$("#cus_customer_address_city").val(retorno.localidade);
			$("#cus_customer_address_state").val(retorno.uf);			
		});

	}

</script>
@stop