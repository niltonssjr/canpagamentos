@extends('adminlte::page')

@section('title', 'Clientes')

@section('content_header')
	<h1>Clientes</h1>
@stop

@php

$linhas = [];

foreach($clientes as $cliente){

	$linha = [];
	$linha['nome'] = $cliente['nome'];
	$linha['cnpj'] = $cliente['cnpj'];
	$linha['email'] = $cliente['email'];
	$linha['criacao'] = $cliente['criacao'];

	$url_details = route('cus.clienteDetalhe',['id'=> $cliente['id']]);
	$action_details = "<a href='{$url_details}'><span class='glyphicon glyphicon-search green_tag'></a>";

	$url_update = route('cus.clienteAtualiza',['id'=> $cliente['id']]);
	$action_update = "<a href='{$url_update}'><span class='glyphicon glyphicon-pencil orange_tag'></a>";

	$id = $cliente['id'];
	$url_delete = "javascript:delete_confirmation('{$id}')";
	$action_delete = "<a href={$url_delete}><span class='glyphicon glyphicon-trash red_tag'></a>";

	$linha['id'] = $action_details . " " . $action_update . " " . $action_delete;
	
	$linhas[] = $linha;

}

@endphp

@section('content')    
	<div class='box-content'>
		@include('includes.alerts')		
    	@include('includes.tabela')
    </div>
@stop

@section('css')
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs/dt-1.10.18/r-2.2.2/datatables.min.css"/>
    <link rel="stylesheet" href='{{ url("/") }}/css/cantarino.css'>
@stop

@section('js')
	<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/v/bs/dt-1.10.18/r-2.2.2/datatables.min.js"></script>
	<script type="text/javascript" src="{{ url('/')}}/js/sweetalert2.all.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
    		$('#standard_table').DataTable({
    			"responsive" :true,
		        "language": {
		            "lengthMenu": "Mostrando  _MENU_  clientes por página",
		            "emptyTable": "Nenhum cliente encontrado",
		            "search":         "Pesquisar: ",
		            "zeroRecords": "Nenhum cliente encontrado",
		            "info": "Mostrando página  _PAGE_  de  _PAGES_",
		            "infoEmpty": "Nenhum cliente encontrado",
		            "infoFiltered": "(filtrado de _MAX_ clientes)",
		                "paginate": {
					        "first":      "Primeira",
					        "last":       "Última",
					        "next":       "Próxima",
					        "previous":   "Anterior"
					    },
				    "aria": {
				        "sortAscending":  ": ative para a ordenação crescente",
				        "sortDescending": ": ative para a ordenação decrescente"
				    }					    
		        }    			
    		});
		} );
		function delete_confirmation(id){
			swal({
				title: 'Confirma a remoção do cliente?',
				text: "Esta ação não pode ser revertida!",
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Sim, remova!'
			}).then((result) => {
				if (result.value) {
					target_route = "{{ route('cus.clienteDelete',['id'=>'##@@##'])}}";	
					target_url = target_route.replace('##@@##',id);
					window.location.replace(target_url);				
				}
			})
		}		
	</script>
@stop

 

