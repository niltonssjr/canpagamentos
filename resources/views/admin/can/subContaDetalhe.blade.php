@extends('adminlte::page')

@section('title', 'Informações sobre a sub-conta')

@section('content_header')
<h1>Informações sobre a sub-conta</h1>
@stop

@section('content')    
    @include('includes.alerts')    

	<div class='box-content'>
    	<table class='table table-striped'>
            <tr><th class='col-lg-3 col-md-3'>ID</th><td class='col-lg-9 col-md-9'>{{ $dashboard->id}}</td></tr>
            <tr><th class='col-lg-3 col-md-3'>Nome</th><td class='col-lg-9 col-md-9'>{{ $dashboard->name}}</td></tr>
            <tr><th class='col-lg-3 col-md-3'>Criada em</th><td class='col-lg-9 col-md-9'>{{ $dashboard->created_at}}</td></tr>
            <tr><th class='col-lg-3 col-md-3'>Conta verificada</th><td class='col-lg-9 col-md-9'>{!! $dashboard->is_verified!!}</td></tr>
            <tr><th class='col-lg-3 col-md-3'>Última verificação de conta</th><td class='col-lg-9 col-md-9'>{!! $dashboard->last_verification_status!!}</td></tr>
        </table>
    </div>
    <div class=''>
    	<h3>Dados verificados</h3>
    </div>    
    <div class='box-content'>
        <table class='table table-striped'>
            <tr><th class='col-lg-3 col-md-3'>Pessoa física ou Jurídica</th><td class='col-lg-9 col-md-9'>{{ $dashboard->person_type}}</td></tr>
            <tr><th class='col-lg-3 col-md-3'>Nome do responsável</th><td class='col-lg-9 col-md-9'>{{ $dashboard->resp_name}}</td></tr>
            <tr><th class='col-lg-3 col-md-3'>CPF</th><td class='col-lg-9 col-md-9'>{{ $dashboard->resp_cpf}}</td></tr>
            <tr><th class='col-lg-3 col-md-3'>Endereço</th><td class='col-lg-9 col-md-9'>{{ $dashboard->address}}</td></tr>
            <tr><th class='col-lg-3 col-md-3'>CEP</th><td class='col-lg-9 col-md-9'>{{ $dashboard->cep}}</td></tr>
            <tr><th class='col-lg-3 col-md-3'>Cidade</th><td class='col-lg-9 col-md-9'>{{ $dashboard->city}}</td></tr>
            <tr><th class='col-lg-3 col-md-3'>Estado</th><td class='col-lg-9 col-md-9'>{{ $dashboard->state}}</td></tr>
            <tr><th class='col-lg-3 col-md-3'>Telefone</th><td class='col-lg-9 col-md-9'>{{ $dashboard->telephone}}</td></tr>
        </table>
    </div>
    <div class=''>
        <h3>Dados bancários</h3>
    </div>    
    <div class='box-content'>
        <table class='table table-striped'>
            <tr><th class='col-lg-3 col-md-3'>Banco</th><td class='col-lg-9 col-md-9'>{{ $dashboard->bank}}</td></tr>
            <tr><th class='col-lg-3 col-md-3'>Agência</th><td class='col-lg-9 col-md-9'>{{ $dashboard->bank_ag}}</td></tr>
            <tr><th class='col-lg-3 col-md-3'>Conta</th><td class='col-lg-9 col-md-9'>{{ $dashboard->bank_cc}}</td></tr>            
            <tr><th class='col-lg-3 col-md-3'>CNPJ</th><td class='col-lg-9 col-md-9'>{{ $dashboard->cnpj}}</td></tr>
            <tr><th class='col-lg-3 col-md-3'>Atividade</th><td class='col-lg-9 col-md-9'>{{ $dashboard->business_type}}</td></tr>
        </table>
    </div> 
    <div class=''>
        <h3>Configurações</h3>
    </div>    
    <div class='box-content'>
        <table class='table table-striped'>
            <tr><th class='col-lg-3 col-md-3'>E-mail cadastrado</th><td class='col-lg-9 col-md-9'>{!! $dashboard->payment_email_notification_receiver!!}</td></tr>
            <tr><th class='col-lg-3 col-md-3'>Emails habilitados</th><td class='col-lg-9 col-md-9'>{!! $dashboard->payment_email_notification!!}</td></tr>
            <tr><th class='col-lg-3 col-md-3'>Aplicação em teste</th><td class='col-lg-9 col-md-9'>{!! $dashboard->webapp_on_test_mode!!}</td></tr>
            <tr><th class='col-lg-3 col-md-3'>Saque automático</th><td class='col-lg-9 col-md-9'>{!! $dashboard->auto_withdraw!!}</td></tr>
            <tr><th class='col-lg-3 col-md-3'>Taxa do boleto</th><td class='col-lg-9 col-md-9'>{!! $dashboard->bank_slip_fee!!}</td></tr>

        </table>
    </div>     
    <div class=''>
        <h3>Informações financeiras</h3>
    </div>    
    <div class='box-content'>
        <table class='table table-striped'>
            <tr><th class='col-lg-3 col-md-3'>Último saque</th><td class='col-lg-9 col-md-9'>{{ $dashboard->last_withdraw}}</td></tr>
            <tr><th class='col-lg-3 col-md-3'>Volume no último mês</th><td class='col-lg-9 col-md-9'>{{ $dashboard->volume_last_month}}</td></tr>
            <tr><th class='col-lg-3 col-md-3'>Volume neste mês</th><td class='col-lg-9 col-md-9'>{{ $dashboard->volume_this_month}}</td></tr>
            <tr><th class='col-lg-3 col-md-3'>Total de assinaturas</th><td class='col-lg-9 col-md-9'>{{ $dashboard->total_subscriptions}}</td></tr>
            <tr><th class='col-lg-3 col-md-3'>Saldo</th><td class='col-lg-9 col-md-9'>{{ $dashboard->balance}}</td></tr>
            <tr><th class='col-lg-3 col-md-3'>Saldo disponível para saque</th><td class='col-lg-9 col-md-9'>{{ $dashboard->balance_available_for_withdraw}}</td></tr>
            <tr><th class='col-lg-3 col-md-3'>Saldo bloqueado</th><td class='col-lg-9 col-md-9'>{{ $dashboard->protected_balance}}</td></tr>
            <tr><th class='col-lg-3 col-md-3'>Saldo pagável</th><td class='col-lg-9 col-md-9'>{{ $dashboard->payable_balance}}</td></tr>
            <tr><th class='col-lg-3 col-md-3'>Saldo recebível</th><td class='col-lg-9 col-md-9'>{{ $dashboard->receivable_balance}}</td></tr>
            <tr><th class='col-lg-3 col-md-3'>Comissões</th><td class='col-lg-9 col-md-9'>{{ $dashboard->commission_balance}}</td></tr>
            <tr><th class='col-lg-3 col-md-3'>Taxas pagas no último mês</th><td class='col-lg-9 col-md-9'>{{ $dashboard->taxes_paid_last_month}}</td></tr>
            <tr><th class='col-lg-3 col-md-3'>Taxas pagas neste mês</th><td class='col-lg-9 col-md-9'>{{ $dashboard->taxes_paid_this_month}}</td></tr>

        </table>
    </div> 
    <br/><a class='btn btn-primary' href="{{ URL::previous() }}">Voltar</a>
@stop

@section('css')
    <link rel="stylesheet" href='{{ url("/") }}/css/cantarino.css'>
@stop