@extends('adminlte::page')

@section('title', 'Solicitação de saque')

@section('content_header')
<h1>Solicitação de saque</h1>
@stop

@section('content')    
	
	<div class='box-content col-md-6'>
    @include('includes.alerts')
    	<h3>Saldo disponível para saque: <strong>{{ $balance }}</strong></h3>
    	<form method='post' action="{{ route('withdraw.store') }}">
    		{!! csrf_field() !!}
    		<div class="form-group">
    			<label for='withdraw_value'>Valor a ser retirado:</label>
    			<input type="text" name="withdraw_value" class='form-control' value="{{ old('withdraw_value') }}">
    		</div>
    		<input type="submit" value="Solicitar" class='btn btn-success'>
    	</form>
    </div>
@stop

@section('css')
    <link rel="stylesheet" href='{{ url("/") }}/css/cantarino.css'>
@stop