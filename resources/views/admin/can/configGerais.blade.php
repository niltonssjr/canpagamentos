@extends('adminlte::page')

@section('title', 'Configurações gerais')

@section('content_header')
<h1>Configurações gerais</h1>
@stop

@section('content')    

<div class="row">
	<div class='col-lg-6 col-md-6 col-sm-6 col-xs-12'>
		<div class='box-content'>
			<form method='post' action="{{ route('can.config-geral.store') }}">
				{!! csrf_field(); !!}
				<div class="checkbox">
					<label>
						<input type="checkbox" name='config_gerais_saque' @if($settings->auto_withdraw) CHECKED @endif> Saque automático
					</label>
				</div>
				<div class="checkbox">
					<label>
						<input type="checkbox" name='config_gerais_receber_emails' id='config_gerais_receber_emails' @if($settings->receive_notification_emails) CHECKED @endif> Receber e-mails de notificação
					</label>
				</div>	
				<div class='form-group'>
					<label for="config_gerais_email">Email para notificação:</label>
					<input type="email" class='form-control' name="config_gerais_email" id="config_gerais_email" value="{{ old ('config_gerais_email', $settings->notification_email)}}"/>
				</div>			
				<input type="submit" class='btn btn-primary' value="Atualizar">
			</form>	
		</div>
	</div>		
</div>
@stop

@section('css')
<link rel="stylesheet" href='{{ url("/") }}/css/cantarino.css'>
@stop

@section('js')
<script>
$(document).ready(function(){
	$("#config_gerais_receber_emails").change(function(){
		if (this.checked){
			$("#config_gerais_email").prop('disabled',false);
		} else {
			$("#config_gerais_email").prop('disabled',true);
		}
	});
});
</script>
@stop



