@extends('adminlte::page')

@section('title', 'Detalhamento da fatura')

@section('content_header')
<h1>Detalhamento da fatura</h1>
@stop

@section('content')    
	<div class='box-content'>
    	<table class='table table-striped'>
    		<tr>
    			<th>URL</th><td>{{ $fatura->secure_url }}</td>
    		</tr>
    		<tr>
    			<th>CÓDIGO</th><td>{{ $fatura->id }}</td>
    		</tr>
    		<tr>
    			<th>EMAIL</th><td>{{ $fatura->email }}</td>
    		</tr>
    		<tr>
    			<th>DATA DE CRIAÇÃO</th><td>{{ $fatura->created_at }}</td>
    		</tr>
    		<tr>
    			<th>VENCIMENTO</th><td>{{ $fatura->due_date }}</td>
    		</tr>
            <tr>
                <th>VALOR DA FATURA</th><td>{{ $fatura->total }}</td>
            </tr>            
    		<tr>
    			<th>TAXAS PAGAS</th><td>{{ $fatura->taxes_paid }}</td>
    		</tr>    		    		    		
    		<tr>
    			<th>STATUS</th><td>{!! $fatura->status !!}</td>
    		</tr>
    		<tr>
    			<th>MULTA</th><td>{{ $fatura->total_overpaid }}</td>
    		</tr>
    		<tr>
    			<th>MÉTODO DE PAGAMENTO</th><td>{!! $fatura->payment_method !!}</td>
    		</tr>      		
    		<tr>
    			<th>VALOR PAGO</th><td>{{ $fatura->total_paid }}</td>
    		</tr>
    		<tr>
    			<th>CLIENTE</th><td>{{ $fatura->customer_name }}</td>
    		</tr>      		
    	</table>
    </div>
    <div class=''>
    	<h2>Items</h2>
    </div>    
    <div class="box-content">
    	<table class='table table-striped'>
    		<thead>
    			<th>Descrição</th>
    			<th>Preço</th>
    			<th>Qtd</th>
    		</thead>
    		<tbody>
    			@foreach($fatura->items as $item)
    				<tr>
    					<td>{{ $item->description }}</td>
    					<td>{{ $item->price }}</td>
    					<td>{{ $item->quantity }}</td>
    				</tr>
    			@endforeach
    		</tbody>
    	</table>
    </div>
    <div class=''>
    	<h2>Informações sobre o pagador</h2>
    </div>  
    <div class="box-content">
    	<table class='table table-striped'>
    		<tr><th>NOME</th><td>{{ $fatura->payer_name }}</td></tr>
    		<tr><th>CPF OU CNPJ</th><td>{{ $fatura->payer_cpf_cnpj }}</td></tr>
    		<tr><th>ENDEREÇO</th><td>{{ $fatura->payer_address_street }}</td></tr>
    		<tr><th>NÚMERO</th><td>{{ $fatura->payer_address_number }}</td></tr>
    		<tr><th>COMPLEMENTO</th><td>{{ $fatura->payer_address_complement }}</td></tr>
    		<tr><th>BAIRRO</th><td>{{ $fatura->payer_address_district }}</td></tr>
    		<tr><th>CIDADE</th><td>{{ $fatura->payer_address_city }}</td></tr>
    		<tr><th>ESTADO</th><td>{{ $fatura->payer_address_state }}</td></tr>
            <tr><th>CEP</th><td>{{ $fatura->payer_address_zip_code }}</td></tr>
    		<tr><th>TELEFONE:</th><td>{{ $fatura->payer_phone_prefix . " - " . $fatura->payer_phone }}</td></tr>
    	</table>
    </div> 
    <div class=''>
    	<h2>Histórico</h2>
    </div> 
    <div class="box-content">
    	<table class='table table-striped'>
    		<thead>
    			<th>Data</th>
    			<th>Descrição</th>
    			<th>Notas</th>
    		</thead>
    		<tbody>
    			@foreach($fatura->logs as $log)
    				<tr>
    					<td>{{ $log->created_at }}</td>
    					<td>{{ $log->description }}</td>
    					<td>{{ $log->notes }}</td>
    				</tr>
    			@endforeach
    		</tbody>
    	</table>
    </div>
    <br/><a class='btn btn-primary' href="{{ URL::previous() }}">Voltar</a>
@stop

@section('css')
<link rel="stylesheet" href='{{ url("/") }}/css/cantarino.css'>
@stop