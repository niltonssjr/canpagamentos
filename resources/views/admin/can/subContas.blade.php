@extends('adminlte::page')

@section('title', 'Sub-Contas')

@section('content_header')
<h1>Sub-Contas</h1>
@stop

@php
$linhas = [];
foreach($subcontas as $subconta){
$linha = [];
$linha[] = $subconta['id'];
$linha[] = $subconta['name'];
$linha[] = $subconta['verified'];

$action_verificar = "";
if (!$subconta['isverified']){
$url_verificar = route('can.subconta.validar',['id'=>$subconta['id']]);	
$action_verificar = "<a href='{$url_verificar}'><span data-toggle='tooltip' data-placement='bottom' title='Solicitar verificação da sub-conta' class='glyphicon glyphicon-ok blue_tag'></span></a>";
}

$url_detalhes = route('can.subconta.detalhe',['id'=>$subconta['id']]);	
$action_detalhes = "<a href='{$url_detalhes}'><span data-toggle='tooltip' data-placement='bottom' title='Detalhes da sub-conta' class='glyphicon glyphicon-search green_tag'></span></a>";

$url_token = route('can.adminapitoken.show',['id'=>$subconta['id']]);	
$action_token = "<a href='{$url_token}'><span data-toggle='tooltip' data-placement='bottom' title='Token para acesso via API' class='glyphicon glyphicon-qrcode purple_tag'></span></a>";

$action_configurar = "";
if ($subconta['name'] !="INATIVA"){
	$url_configurar = route('can.subconta.configurar',['id'=>$subconta['id']]);	
	$action_configurar = "<a href='{$url_configurar}'><span data-toggle='tooltip' data-placement='bottom' title='Reconfigurar sub-conta' class='glyphicon glyphicon-pencil orange_tag'></a>";
}

$action_inativar = "";
if ($subconta['name'] !="INATIVA"){
	$url_inativar = route('can.subconta.inativar',['id'=>$subconta['id']]);
	$id = $subconta['id'];
	$confirm_routine = "javascript:inactivation_confirmation('{$id}')";
	$action_inativar = "<a href={$confirm_routine}><span data-toggle='tooltip' data-placement='bottom' title='Desativar sub-conta' class='glyphicon glyphicon-ban-circle red_tag'></span></a>";
}	

$linha[] = $action_verificar . $action_detalhes . $action_token .$action_configurar  .  $action_inativar;
$linhas[] = $linha;
}

@endphp

@section('content')    
<div class='box-content'>
	@include('includes.alerts')
	@include('includes.tabela')
</div>
@stop

@section('css')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs/dt-1.10.18/r-2.2.2/datatables.min.css"/>
<link rel="stylesheet" href='{{ url("/") }}/css/cantarino.css'>
@stop

@section('js')
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/v/bs/dt-1.10.18/r-2.2.2/datatables.min.js"></script>
<script type="text/javascript" src="{{ url('/')}}/js/sweetalert2.all.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$('[data-toggle="tooltip"]').tooltip();
		$('#standard_table').DataTable({
			"responsive" :true,
			"language": {
				"lengthMenu": "Mostrando  _MENU_  sub-contas por página",
				"emptyTable": "Nenhuma sub-conta encontrada",
				"search":         "Pesquisar: ",
				"zeroRecords": "Nenhuma sub-conta encontrada",
				"info": "Mostrando página  _PAGE_  de  _PAGES_",
				"infoEmpty": "Nenhuma sub-conta encontrada",
				"infoFiltered": "(filtrado de _MAX_ sub-contas)",
				"paginate": {
					"first":      "Primeira",
					"last":       "Última",
					"next":       "Próxima",
					"previous":   "Anterior"
				},
				"aria": {
					"sortAscending":  ": ative para a ordenação crescente",
					"sortDescending": ": ative para a ordenação decrescente"
				}					    
			}    			
		});
	} );
	function inactivation_confirmation(id){
		swal({
			title: 'Confirma a inativação de conta?',
			text: "Esta ação não pode ser revertida!",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Sim, desative!'
		}).then((result) => {
			if (result.value) {
				target_route = "{{ route('can.subconta.inativar',['id'=>'##@@##'])}}";	
				target_url = target_route.replace('##@@##',id);
				window.location.replace(target_url);				
			}
		})
	}
</script>
@stop



