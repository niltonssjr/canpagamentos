@extends('adminlte::page')

@section('title', 'Relatório financeiro')

@section('content_header')
	<h1>Relatório financeiro</h1>
@stop

@section('content')    
	
	<div class='box-content'>
		
		@include('includes.tabela')
		
	</div>
@stop

@section('css')
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs/dt-1.10.18/r-2.2.2/datatables.min.css"/>
    <link rel="stylesheet" href='{{ url("/") }}/css/cantarino.css'>

@section('js')
	<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/v/bs/dt-1.10.18/r-2.2.2/datatables.min.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
    		$('#standard_table').DataTable({
    			"responsive" :true,
    			"ordering" : false,
		        "language": {
		            "lengthMenu": "Mostrando  _MENU_  faturas por página",
		            "emptyTable": "Nenhuma fatura encontrada",
		            "search":         "Pesquisar: ",
		            "zeroRecords": "Nenhuma fatura encontrada",
		            "info": "Mostrando página  _PAGE_  de  _PAGES_",
		            "infoEmpty": "Nenhuma fatura encontrada",
		            "infoFiltered": "(filtrado de _MAX_ faturas)",
		                "paginate": {
					        "first":      "Primeira",
					        "last":       "Última",
					        "next":       "Próxima",
					        "previous":   "Anterior"
					    },
				    "aria": {
				        "sortAscending":  ": ative para a ordenação crescente",
				        "sortDescending": ": ative para a ordenação decrescente"
				    }					    
		        }    			
    		});
		} );
	</script>
@stop

 

