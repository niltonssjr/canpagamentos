@extends('adminlte::page')

@section('title', 'Nova Sub-Conta')

@section('content_header')
<h1>Nova Sub-Conta</h1>
@stop

@section('content')

<form method='post' action=" {{ route('can.subcontanova.store') }}">
	<div class="">
		<div class="row">
			<div class='col-md-8'>
				<div class="box-content">
					

					@include('includes.alerts')
					{!! csrf_field() !!}
					<div class="form-group">
						<label for='subconta_nome'>Nome:</label>
						<input type="text" class='form-control' name="subconta_nome" placeholder='Nome da sub-conta' value="{{ old('subconta_nome')}}"/>
					</div>
					<div class="form-group">
						<label for='subconta_email'>Email:</label>
						<input type="email" class='form-control' name="subconta_email" placeholder='E-mail' value="{{ old('subconta_email')}}"/>
					</div>				
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label for='subconta_comissao_cc_dinheiro'>Comissão para cartão de crédito:</label>
								<div class='input-group'>						
									<span class='input-group-addon'>R$</span>
									<input type="text" class='form-control as_mask_money ' name="subconta_comissao_cc_dinheiro" value="{{ old('subconta_comissao_cc_dinheiro')}}"/>
								</div>
							</div>
						</div>

						<div class="col-md-6">
							<div class="form-group">
								<label for='subconta_comissao_boletos_dinheiro'>Comissão para boletos:</label>
								<div class='input-group'>						
									<span class='input-group-addon'>R$</span>
									<input type="text" class='form-control as_mask_money ' name="subconta_comissao_boletos_dinheiro" value="{{ old('subconta_comissao_boletos_dinheiro')}}"/>
								</div>
							</div>
						</div>
					</div>													
					<input type="submit" class="btn btn-primary" name="" value="Cadastrar">
				</div>
			</div>			
		</div>	
	</div>
</form>

@stop

@section('css')
<link href="{{ url('/')}}/css/bootstrap-datepicker.min.css" rel="stylesheet">
<link rel="stylesheet" href='{{ url("/") }}/css/cantarino.css'>
@stop

@section('js')
<script src='{{ url("/") }}/js/jquery.mask.min.js'></script>
<script src='{{ url("/") }}/js/bootstrap-datepicker.min.js'></script>
<script src='{{ url("/") }}/js/bootstrap-datepicker.pt-BR.min.js'></script>
<script src='{{ url("/") }}/js/auge_masks.js'></script>
<script src='{{ url("/") }}/js/sweetalert2.all.js'></script>
<script type="text/javascript">

	$(document).ready(function(){

		//load final customer data based on the email
		$("#cus_fatura_pagador_email").change(function(){
			pesquisar_cliente();
		});

		$("#btn_search_zip").click(function(){
			search_zip_code();
		});

	});

	function search_zip_code(){

		zip_code = $("#cus_customer_address_zip_code").cleanVal().trim();
		console.log(zip_code);

		if (zip_code==""){
			swal('Cep obrigatório',"Informe o CEP para pesquisa",'warning');
			return false;
		}

		if (zip_code.length!=8){
			swal('Cep obrigatório',"O CEP informado não é válido",'warning');
			return false;	
		}

		url = 'http://viacep.com.br/ws/' + zip_code + '/json/';

		$.get(url,function(retorno){
			console.log(retorno);
			
			if (retorno.erro){
				swal('Cep não encontrado','O CEP informado não existe','warning');
				return false;
			}
			$("#cus_customer_address_street").val(retorno.logradouro);	
			$("#cus_customer_address_district").val(retorno.bairro);	
			$("#cus_customer_address_city").val(retorno.localidade);
			$("#cus_customer_address_state").val(retorno.uf);			
		});

	}

</script>
@stop