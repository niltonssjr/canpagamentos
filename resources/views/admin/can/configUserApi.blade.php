@extends('adminlte::page')

@section('title', 'Token de acesso a API REST')

@section('content_header')
<h1>{{$account_name}}</h1>
@stop

@section('content')    

<div class="row">
	<div class='col-lg-6 col-md-6 col-sm-6 col-xs-12'>
		<div class='box-content'>			
			<form method='post' action="{{ route('can.userapitoken.generate') }}">
				{!! csrf_field(); !!}
				<input type="hidden" name="config_api_account_id" value="{{$account_id}}">
				<div class='form-group'>
					<label for="config_api_token">Token atual para conexão:</label>
					<textarea class='form-control' id='config_api_token' rows=15 readonly>{{$token}}</textarea>					
				</div>			
				<input type="submit" class='btn btn-primary' value="Gerar novo">
				<input type="button" class='btn btn-default' onclick="copyToClipboard()" value="Copiar para a área de transferência">
			</form>	
		</div>
	</div>		
</div>
@stop

@section('css')
<link rel="stylesheet" href='{{ url("/") }}/css/cantarino.css'>
@stop

@section('js')
<script>
function copyToClipboard(){
	textarea = document.getElementById('config_api_token');
	textarea.select();
	document.execCommand('copy');
	return;
}
</script>
@stop



