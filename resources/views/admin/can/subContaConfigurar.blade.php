@extends('adminlte::page')

@section('title', 'Configuração da Sub Conta')

@section('content_header')
<h1>Configuração da Sub-Conta</h1>
@stop

@section('content')

<form method='post' action=" {{ route('can.subconta.configurar.store') }}">
	<div class="">
		<div class="row">
			<div class='col-md-8'>
				<div class="box-content">
					@include('includes.alerts')
					{!! csrf_field() !!}
					<input type="hidden" name="settings_account_id" value="{{old('settings_acont_id',$settings->account_id)}}">
					<div class="row">
						<div class='col-md-12'>
							<div class="checkbox">
								<label>
									<input type="checkbox" name='settings_auto_withdraw' @if(old('settings_auto_withdraw',$settings->auto_withdraw)) CHECKED @endif> Habilitar saque automático
								</label>
							</div>
						</div>
					</div>
					<div class="row">
						<div class='col-md-12'>
							<div class="checkbox">
								<label>
									<input type="checkbox" name='settings_receive_notification_emails' id='settings_receive_notification_emails' @if(old('settings_receive_notification_emails',$settings->receive_notification_emails)) CHECKED @endif> Receber e-mails de notificação
								</label>
							</div>	
						</div>
					</div>
					<div class='form-group'>
						<label for="settings_notification_email">Email para notificação:</label>
						<input type="email" class='form-control' name="settings_notification_email" id="settings_notification_email" value="{{ old ('settings_notification_email', $settings->notification_email)}}"/>
					</div>			
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label for='settings_commission_cc_value'>Comissão para cartão de crédito:</label>
								<div class='input-group'>						
									<span class='input-group-addon'>R$</span>
									<input type="text" class='form-control as_mask_money ' name="settings_commission_cc_value" value="{{ old('settings_commission_cc_value',$settings->commission_cc_value)}}"/>
								</div>
							</div>
						</div>

						<div class="col-md-6">
							<div class="form-group">
								<label for='settings_commission_bank_slip_value'>Comissão para boletos:</label>
								<div class='input-group'>						
									<span class='input-group-addon'>R$</span>
									<input type="text" class='form-control as_mask_money ' name="settings_commission_bank_slip_value" value="{{ old('settings_commission_bank_slip_value',$settings->commission_bank_slip_value)}}"/>
								</div>
							</div>
						</div>
					</div>												
					<input type="submit" class="btn btn-primary" name="" value="Cadastrar">
				</div>
			</div>			
		</div>	
	</div>
</form>

@stop

@section('css')
<link href="{{ url('/')}}/css/bootstrap-datepicker.min.css" rel="stylesheet">
<link rel="stylesheet" href='{{ url("/") }}/css/cantarino.css'>
@stop

@section('js')
<script src='{{ url("/") }}/js/jquery.mask.min.js'></script>
<script src='{{ url("/") }}/js/bootstrap-datepicker.min.js'></script>
<script src='{{ url("/") }}/js/bootstrap-datepicker.pt-BR.min.js'></script>
<script src='{{ url("/") }}/js/auge_masks.js'></script>
<script src='{{ url("/") }}/js/sweetalert2.all.js'></script>
<script type="text/javascript">

	$(document).ready(function(){

		$("#settings_receive_notification_emails").change(function(){
			$("#settings_notification_email").prop('disabled',this.checked == false);
		});


	});



</script>
@stop