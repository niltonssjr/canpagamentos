@extends('adminlte::page')

@section('title', 'Faturas')

@php

$linhas = [];

foreach($faturas as $fatura){
	$linha = [];
	$linha['criacao'] = $fatura['criacao'];
	$linha['cliente'] = $fatura['cliente'];
	$linha['email'] = $fatura['email'];
	$linha['vencimento'] = $fatura['vencimento'];
	$linha['pagamento'] = $fatura['pagamento'];
	$linha['format_status'] = $fatura['format_status'];
	$linha['valor'] = $fatura['valor'];
	$linha['taxas'] = $fatura['taxas'];

	$url_details = route('can.faturaDetalhe',['id'=> $fatura['id']]);
	$action_details = "<a href='{$url_details}'><span class='glyphicon glyphicon-search green_tag'></span></a>";

	$action_cancel = "";

	if ($fatura['status'] == 'pending'){		
		$id = $fatura['id'];
		$url_cancel = "javascript:delete_confirmation('{$id}')";
		$action_cancel = "<a href={$url_cancel}><span class='glyphicon glyphicon-trash red_tag'></span></a>";					
	}
	
	$linha['actions'] = $action_details . " " . $action_cancel;
	$linhas[] = $linha;

}

@endphp

@section('content')    
	<div class='box-content'>
    	@include('includes.tabela')
    </div>
@stop

@section('css')
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs/dt-1.10.18/r-2.2.2/datatables.min.css"/>
<link rel="stylesheet" href='{{ url("/") }}/css/cantarino.css'>
@stop

@section('js')
	<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/v/bs/dt-1.10.18/r-2.2.2/datatables.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.8.4/moment.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/plug-ins/1.10.19/sorting/datetime-moment.js"></script>
	<script type="text/javascript" src="{{ url('/')}}/js/sweetalert2.all.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			$.fn.dataTable.moment( "DD/MM/YYYY");
			jQuery.extend( jQuery.fn.dataTableExt.oSort, {
			    "currency-pre": function ( a ) {
			        // a = (a==="-") ? 0 : a.replace( /[^\d\-\.]/g, "" );
			        a = a.replace("R$","").replace(/\./g,"").replace(",",".");
			        return parseFloat( a );
			    },
			 
			    "currency-asc": function ( a, b ) {
			        return a - b;
			    },
			 
			    "currency-desc": function ( a, b ) {
			        return b - a;
			    }
			} );			
    		$('#standard_table').DataTable({
    			"responsive" :true,
    			"columnDefs":[{"type":"currency","targets":6}],
		        "language": {
		            "lengthMenu": "Mostrando  _MENU_  faturas por página",
		            "emptyTable": "Nenhuma fatura encontrada",
		            "search":         "Pesquisar: ",
		            "zeroRecords": "Nenhuma fatura encontrada",
		            "info": "Mostrando página  _PAGE_  de  _PAGES_",
		            "infoEmpty": "Nenhuma fatura encontrada",
		            "infoFiltered": "(filtrado de _MAX_ faturas)",
		                "paginate": {
					        "first":      "Primeira",
					        "last":       "Última",
					        "next":       "Próxima",
					        "previous":   "Anterior"
					    },
				    "aria": {
				        "sortAscending":  ": ative para a ordenação crescente",
				        "sortDescending": ": ative para a ordenação decrescente"
				    }					    
		        }    			
    		});
		} );
		function delete_confirmation(id){
			swal({
				title: 'Confirma o cancelamento da fatura?',
				text: "Esta ação não pode ser revertida!",
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Sim, cancele!'
			}).then((result) => {
				if (result.value) {					
					target_route = "{{ route('can.faturaCancel',['id'=>'##@@##'])}}";	
					target_url = target_route.replace('##@@##',id);
					window.location.replace(target_url);				
				}
			})
		}			
	</script>
@stop

 

