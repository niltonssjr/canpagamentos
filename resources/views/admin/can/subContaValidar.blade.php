@extends('adminlte::page')

@section('title', 'Validação de sub-conta')

@section('content_header')
<h1>Validação de sub-conta</h1>
@stop

@section('content')

<form method='post' action=" {{ route('can.subconta.validar.store') }}">
	<div class='box-content'>
		<div class="row">
			<div class='col-md-12'>
				@include('includes.alerts')
				{!! csrf_field() !!}				
				<div class="form-group">
					<label for='subconta_account_id'>ID:</label>
					<input type="text" class='form-control' name="subconta_account_id" readonly value="{{ old('id',$id) }}"/>
				</div>									
			</div>
		</div>
		<div class="row">
			<div class='col-md-12'>
				<div class="form-group">
					<label for='subconta_business_type'>Descrição do negócio:</label>
					<input type="text" class='form-control' name="subconta_business_type" value="{{ old('subconta_business_type') }}"/>
				</div>													
			</div>
		</div>
		<div class="row">
			<div class='col-md-6 col-lg-6'>
				<div class="form-group">
					<label for='subconta_maximum_value'>Valor máximo de venda:</label>
					<select class='form-control' name='subconta_maximum_value'>
						<option value='Até R$ 100,00' @if(old("subconta_maximum_value")=="Até R$ 100,00") SELECTED @endif >Até R$100,00</option>
						<option value='Entre R$100,00 e R$500,00' @if(old("subconta_maximum_value")=="Entre R$100,00 e R$500,00") SELECTED @endif>Entre R$100,00 e R$500,00</option>
						<option value='Mais que R$ 500,00' @if(old("subconta_maximum_value")=="Mais que R$ 500,00") SELECTED @endif>Mais que R$ 500,00</option>
					</select>					
				</div>					
			</div>				
			<div class='col-md-6 col-lg-6'>
				<div class="form-group">
					<label for='subconta_physical_products'>Vende produtos físicos?</label>
					<select class='form-control' name='subconta_physical_products'>
						<option value='Sim'>Sim</option>
						<option value='Nao' @if(old('subconta_physical_products')=='Nao') SELECTED @endif>Não</option>
					</select>					
				</div>					
			</div>							
		</div>
		<div class="row">
			<div class='col-md-6 col-lg-6'>
				<div class="form-group">
					<label for='subconta_person_type'>Tipo de pessoa:</label>
					<select class='form-control' name='subconta_person_type' id='subconta_person_type'>
						<option value='fis'>Pessoa física</option>
						<option value='jur' @if(old('subconta_person_type')=='jur') SELECTED @endif>Pessoa jurídica</option>							
					</select>					
				</div>					
			</div>				
			<div class='col-md-6 col-lg-6'>
				<div class="form-group">
					<label for='subconta_automatic_transfer'>Saque automático?</label>
					<select class='form-control' name='subconta_automatic_transfer'>
						<option value='Sim'>Sim (recomendado)</option>
						<option value='Nao' @if(old('subconta_automatic_transfer')=='Nao') SELECTED @endif>Não</option>
					</select>					
				</div>					
			</div>				
		</div>
		<div class="row">
			<div class='div_pessoa_fisica'>
				<div class='col-md-6 col-lg-6'>
					<div class="form-group">
						<label for='subconta_CPF'>CPF:</label>
						<input type="text" class='form-control as_mask_cpf' name="subconta_CPF" value="{{ old('subconta_CPF') }}"/>
					</div>					
				</div>				
				<div class='col-md-6 col-lg-6'>
					<div class="form-group">
						<label for='subconta_name'>Nome:</label>
						<input type="text" class='form-control' name="subconta_nome" value="{{ old('subconta_nome') }}"/>
					</div>					
				</div>									
			</div>			
		</div>
		<div class="row">
			<div class='div_pessoa_juridica'>
				<div class='col-md-6 col-lg-6'>
					<div class="form-group">
						<label for='subconta_CNPJ'>CNPJ:</label>
						<input type="text" class='form-control as_mask_cnpj' name="subconta_CNPJ" value="{{ old('subconta_CNPJ') }}"/>
					</div>					
				</div>				
				<div class='col-md-6 col-lg-6'>
					<div class="form-group">
						<label for='subconta_company_name'>Razão social:</label>
						<input type="text" class='form-control' name="subconta_company_name" value="{{ old('subconta_company_name') }}"/>
					</div>					
				</div>									
			</div>			
		</div>
		<div class="row">
			<div class='div_pessoa_juridica'>
				<div class='col-md-6 col-lg-6'>
					<div class="form-group">
						<label for='subconta_responsible_CPF'>CPF do responsável pela empresa:</label>
						<input type="text" class='form-control as_mask_cpf' name="subconta_responsible_CPF" value="{{ old('subconta_responsible_CPF') }}"/>
					</div>					
				</div>				
				<div class='col-md-6 col-lg-6'>
					<div class="form-group">
						<label for='subconta_responsible_name'>Nome do responsável pela empresa:</label>
						<input type="text" class='form-control' name="subconta_responsible_name" value="{{ old('subconta_responsible_name') }}"/>
					</div>					
				</div>									
			</div>
		</div>
	</div>

	<h3>Dados de contato</h3>
	<div class='box-content'>
		<div class='row'>
			<div class='col-md-2'>		
				<div class="form-group">
					<label for='subconta_address_zip_code'>CEP:</label>
					<div class="input-group">						
						<input type="text" class="form-control as_mask_zip_code" name='subconta_address_zip_code' id='subconta_address_zip_code' placeholder="00000-000" value="{{ old('subconta_address_zip_code') }}">
						<span class="input-group-btn">
							<button class="btn btn-primary" id='btn_search_zip' type="button"><i class="glyphicon glyphicon-search"></i></button>
						</span>
					</div>							
				</div>
			</div>
		</div>	
		<div class="row">
			<div class='col-md-12 col-lg-12'>
				<div class="form-group">
					<label for='subconta_address_street'>Endereço:</label>
					<input type="text" class='form-control' name="subconta_address_street" id="subconta_address_street" value="{{ old('subconta_address_street') }}"/>
				</div>					
			</div>				
		</div>	
		<div class='row'>
			<div class='col-md-6'>
				<div class="form-group">
					<label for='subconta_address_city'>Cidade:</label>
					<input type="text" class='form-control' name="subconta_address_city" id="subconta_address_city" placeholder="Cidade" value=" {{ old('subconta_address_city') }} ">
				</div>					
			</div>
			<div class='col-md-1'>
				<div class="form-group">
					<label for='subconta_address_state'>UF:</label>
					<input type="text" class='form-control' name="subconta_address_state" id="subconta_address_state" placeholder="" value="{{ old('subconta_address_state') }}">
				</div>					
			</div>	
			<div class='col-md-5'>
				<div class="form-group">
					<label for='subconta_phone_number'>Telefone:</label>
					<input type="text" class='form-control as_mask_full_phone' name="subconta_phone_number" id="subconta_phone_number" aceholder="" value="{{ old('subconta_phone_number') }}">
				</div>					
			</div>							
		</div>	

		<h3>Dados bancários</h3>
		<div class="box-content">
			@include('includes.contaBancaria')
		</div>
		<div class="row">
			<div class='col-md-12'>
				<br/>
				<input type="submit" class="btn btn-primary" name="" value="Cadastrar">	
			</div>			
		</div>	
	</form>

	@stop

	@section('css')
	<link href="{{ url('/')}}/css/bootstrap-datepicker.min.css" rel="stylesheet">
	<link rel="stylesheet" href='{{ url("/") }}/css/cantarino.css'>
	@stop

	@section('js')
	<script src='{{ url("/") }}/js/jquery.mask.min.js'></script>
	<script src='{{ url("/") }}/js/bootstrap-datepicker.min.js'></script>
	<script src='{{ url("/") }}/js/bootstrap-datepicker.pt-BR.min.js'></script>
	<script src='{{ url("/") }}/js/auge_masks.js'></script>
	<script src='{{ url("/") }}/js/sweetalert2.all.js'></script>
	<script type="text/javascript">

		$(document).ready(function(){

			$("#btn_search_zip").click(function(){
				search_zip_code();
			});
			switch_person_type($("#subconta_person_type option").filter(":selected").val());			
			$("#subconta_person_type").change(function(){
				switch_person_type($("#subconta_person_type option").filter(":selected").val());			
			});

		});

		function switch_person_type(type){

			if (type=='fis'){
				$(".div_pessoa_juridica").hide();			
				$(".div_pessoa_fisica").show();
			} else {
				$(".div_pessoa_juridica").show();			
				$(".div_pessoa_fisica").hide();			
			}

		}
		function search_zip_code(){

			zip_code = $("#subconta_address_zip_code").cleanVal().trim();
			console.log(zip_code);

			if (zip_code==""){
				swal('Cep obrigatório',"Informe o CEP para pesquisa",'warning');
				return false;
			}

			if (zip_code.length!=8){
				swal('Cep obrigatório',"O CEP informado não é válido",'warning');
				return false;	
			}

			url = 'http://viacep.com.br/ws/' + zip_code + '/json/';

			$.get(url,function(retorno){
				console.log(retorno);

				if (retorno.erro){
					swal('Cep não encontrado','O CEP informado não existe','warning');
					return false;
				}
				$("#subconta_address_street").val(retorno.logradouro);	
				$("#subconta_address_city").val(retorno.localidade);
				$("#subconta_address_state").val(retorno.uf);			
			});

		}

	</script>
	@stop