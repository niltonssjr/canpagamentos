@extends('adminlte::page')

@section('title', 'Alteração de senha')

@section('content_header')
<h1>Alteração de senha</h1>
@stop

@section('content')   
<div class="row">
	<div class='col-lg-6 col-md-6 col-sm-6 col-xs-12'> 
		@include ('includes.alerts')
	</div>
</div>
<div class="row">
	<div class='col-lg-6 col-md-6 col-sm-6 col-xs-12'>
		<div class='box-content'>
			<h3>{{ $current_user }}</h3>
			<form method='post' action="{{ route('can.password.store') }}">
				{!! csrf_field(); !!}
				<div class="form-group">
					<label for='pw_password'>Nova senha:</label>
					<input type="password" name="pw_password" class='form-control' value="{{ old('pw_password')}}">
				</div>
				<div class="form-group">
					<label for='pw_password_confirmation'>Confirme a nova senha:</label>
					<input type="password" name="pw_password_confirmation" class='form-control' value="{{ old('pw_password_confirmation')}}">
				</div>				
				<input type="submit" class='btn btn-primary' value="Atualizar">
			</form>	
		</div>
	</div>		
</div>
@stop

@section('css')
<link rel="stylesheet" href='{{ url("/") }}/css/cantarino.css'>
@stop

@section('js')
<script>
	$(document).ready(function(){
		$("#config_gerais_receber_emails").change(function(){
			if (this.checked){
				$("#config_gerais_email").prop('disabled',false);
			} else {
				$("#config_gerais_email").prop('disabled',true);
			}
		});
	});
</script>
@stop



