@extends('adminlte::page')

@section('title', 'Configurações bancárias')

@section('content_header')

@stop

@section('content')    
<h3>Conta bancária atual</h3>
<div class="row">
	<div class='col-lg-9 col-md-9 col-sm-9 col-xs-12'>
		<div class='box-content'>
			<table class='table table-striped'>
				<thead>
					<th>Banco</th>
					<th>Agência</th>
					<th>Conta</th>
				</thead>
				<tbody>
					<td>{{ $config->banco}}</td>
					<td>{{$config->agencia}}</td>
					<td>{{$config->conta}}</td>
				</tbody>			
			</table>
		</div>
	</div>
</div>
<h3>Histórico de alterações</h3>
<div class="row">
	<div class='col-lg-9 col-md-9 col-sm-9 col-xs-12'>
		<div class='box-content'>
			@include ('includes.tabela')
		</div>
	</div>
</div>
<br/><a href="{{ route('dashboard') }}" class="btn btn-primary">Voltar</a>
<br/><br/>
<form method='post' action="{{ route('can.config-bancos.store') }}">
	<h3>Informe os dados da nova conta</h3>
	<div class="row">
		<div class='col-lg-9 col-md-9 col-sm-9 col-xs-12'>
			<div class='box-content'>
			@include ('includes.alerts')
			{!! csrf_field() !!}
				@include ('includes.contaBancaria')
			
			</div>
		</div>
	</div>
	<br/>
	<input type="submit" class='btn btn-primary' value="Atualizar">
</form>
@stop

@section('css')
<link rel="stylesheet" href='{{ url("/") }}/css/cantarino.css'>
@stop

@section('js')
<script src='{{ url("/") }}/js/jquery.mask.min.js'></script>
<script src='{{ url("/") }}/js/auge_masks.js'></script>
<script>
	$(document).ready(function(){
		$("#config_gerais_receber_emails").change(function(){
			if (this.checked){
				$("#config_gerais_email").prop('disabled',false);
			} else {
				$("#config_gerais_email").prop('disabled',true);
			}
		});
	});
</script>
@stop



