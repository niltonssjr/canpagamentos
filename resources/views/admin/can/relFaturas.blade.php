@extends('adminlte::page')

@section('title', 'Relatório de faturas')

@section('content_header')
	<h1>Relatório de faturas</h1>
@stop

@section('content')    
	<div class='box-content'>
		<form class='form-inline' method='post' action="{{ route('can.faturas.report.filter') }}">
			{!! csrf_field() !!}
			<div class="form-group">
				<label for='relfatura_mes'>Mês: </label>
				<select class='form-control' name='relfatura_mes'>					
					@foreach($meses as $mes)
						<option value="{{ $mes['num'] }}" {{ $mes['selected'] }}>{{$mes['nome']}}</option>
					@endforeach
				</select>
			</div>
			<div class="form-group">
				<label for='relfatura_ano'>Ano: </label>
				<select class='form-control' name='relfatura_ano'>
					@foreach($anos as $ano)
						<option value="{{ $ano['value']}}" {{ $ano['selected']}}>{{ $ano['text']}}</option>
					@endforeach
				</select>
			</div>			
			<div class="form-group">
				<label for='relfatura_status'>Status: </label>
				<select class='form-control' name='relfatura_status'>	
					@foreach($statuses as $status)					
						<option value="{{ $status['value'] }}" {{$status['selected'] }}>{{ $status['text']}}</option>
					@endforeach
				</select>
			</div>	
			<input type="submit" value="Pesquisar" class='btn btn-success'>			
		</form>
	</div>
	<br/>
	<div class='box-content'>
		
		@include('includes.tabela')
		
	</div>
@stop

@section('css')
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs/dt-1.10.18/r-2.2.2/datatables.min.css"/>
    <link rel="stylesheet" href='{{ url("/") }}/css/cantarino.css'>
@stop

@section('js')
	<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/v/bs/dt-1.10.18/r-2.2.2/datatables.min.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
    		$('#standard_table').DataTable({
    			"responsive" :true,
		        "language": {
		            "lengthMenu": "Mostrando  _MENU_  faturas por página",
		            "emptyTable": "Nenhuma fatura encontrada",
		            "search":         "Pesquisar: ",
		            "zeroRecords": "Nenhuma fatura encontrada",
		            "info": "Mostrando página  _PAGE_  de  _PAGES_",
		            "infoEmpty": "Nenhuma fatura encontrada",
		            "infoFiltered": "(filtrado de _MAX_ faturas)",
		                "paginate": {
					        "first":      "Primeira",
					        "last":       "Última",
					        "next":       "Próxima",
					        "previous":   "Anterior"
					    },
				    "aria": {
				        "sortAscending":  ": ative para a ordenação crescente",
				        "sortDescending": ": ative para a ordenação decrescente"
				    }					    
		        }    			
    		});
		} );
	</script>
@stop

 

