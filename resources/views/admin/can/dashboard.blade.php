@extends('adminlte::page')

@section('title', 'Faturas')

@section('content')    
<div class=''>
	<div class="">
		<div class="row">

			<!-- ./col -->
			<div class="col-lg-3 col-xs-6">
				<!-- small box -->
				<div class="small-box bg-green">
					<div class="inner">
						<!-- <h3><sup style="font-size: 20px">R$</sup>1.503,59</h3> -->
						<h3>{{ $dashboard->last_withdraw }}</h3>

						<p>Última transferência</p>
					</div>
					<div class="icon">
						<i class="ion ion-stats-bars"></i>
					</div>
					<a href="{{ route('can.relatoriofinanceiro') }}" class="small-box-footer">Relatório financeiro <i class="fa fa-arrow-circle-right"></i></a>
				</div>
			</div>
			<div class="col-lg-3 col-xs-6">
				<!-- small box -->
				<div class="small-box bg-green">
					<div class="inner">
						<h3>{{ $dashboard->this_month_invoice_created_total }}</h3>

						<p>Total em faturas emitidas no mês</p>
					</div>
					<div class="icon">
						<i class="ion ion-stats-bars"></i>
					</div>
					<a href="#" class="small-box-footer">Ver assinantes<i class="fa fa-arrow-circle-right"></i></a>
				</div>
			</div>
			<div class="col-lg-3 col-xs-6">
				<!-- small box -->
				<div class="small-box bg-green">
					<div class="inner">
						<h3>{{ $dashboard->volume_this_month }}</h3>

						<p>Recebimento este mês</p>
					</div>
					<div class="icon">
						<i class="ion ion-stats-bars"></i>
					</div>
					<a href="{{ route('cus.faturas' )}}" class="small-box-footer">Ver faturas <i class="fa fa-arrow-circle-right"></i></a>
				</div>
			</div>
			<div class="col-lg-3 col-xs-6">
				<!-- small box -->
				<div class="small-box bg-green">
					<div class="inner">
						<h3>{{ $dashboard->volume_last_month }}</h3>

						<p>Recebimento mês anterior</p>
					</div>
					<div class="icon">
						<i class="ion ion-stats-bars"></i>
					</div>
					<a href="{{ route('cus.faturas' )}}" class="small-box-footer">Ver faturas <i class="fa fa-arrow-circle-right"></i></a>
				</div>
			</div>									
			
		</div>		
	</div>
</div>
<h3>Saldos e informações da conta</h3>
<div class='box-content'>
	<table class='table table-striped'>
		<thead>
			<tr>
				<th class='col-md-3'>Saldo</th>
				<th class='col-md-3'>Saldo disponível</th>
				<th class='col-md-3'>Saldo em trânsito</th>
				<th class='col-md-3'>À receber</th>				
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>{{ $dashboard->balance }}</td>
				<td>{{ $dashboard->balance_available_for_withdraw }}</td>
				<td>{{ $dashboard->protected_balance }}</td>
				<td>{{ $dashboard->receivable_balance }}</td>
			</tr>
		</tbody>
	</table>	
	@can('isAdmin')
	<table class='table table-striped'>
		<thead>
			<tr>
				<th class='col-md-3'>Comissões</th>
				<th class='col-md-3'>Tarifas</th>
				<th class='col-md-3'>Taxas do mês</th>
				<th class='col-md-3'>Taxas mês anterior</th>
			</tr>
		</thead>
		<tbody>
			<tr>				
				<td>{{ $dashboard->commission_balance }}</td>
				<td>R$ 0,00</td>
				<td>{{ $dashboard->taxes_paid_this_month }}</td>
				<td>{{ $dashboard->taxes_paid_last_month }}</td>
			</tr>
		</tbody>
	</table>	
	@endcan
</div>
<div class="row">
	<br/>
	<div class='col-md-12'>
		<a href="{{ route ('withdraw')}}" class='btn btn-success'>Solicitar transferência</a>		
	</div>
</div>
@stop

@section('css')
<link rel="stylesheet" href='{{ url("/") }}/css/cantarino.css'>
@stop