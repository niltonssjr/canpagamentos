<div>
	<table class='table table-stripped' id="standard_table">
		<thead>
			<tr>
			@foreach($titulos as $titulo)		
				<th>{{$titulo}}</th>
			@endforeach
			</tr>
		</thead>	
		<tbody>
			@foreach($linhas as $linha)
			<tr>
				@foreach($linha as $coluna)
					<td>
						{!! $coluna !!}
					</td>
				@endforeach
			</tr>
			@endforeach
		</tbody>
	</table>
</div>