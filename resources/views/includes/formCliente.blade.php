<div class=''>
	<div class="row">		
		<div class='col-md-8'>
			<div class='box-content'>
				@include('includes.alerts')		
				{!! csrf_field(); !!}
				<input type="hidden" name="ctrl_account_id" id="ctrl_account_id" value="{{ old('ctrl_account_id',$customer_data->account_id) }}">
				<div class="form-group">
					<label for='cus_customer_name'>Nome:</label>
					<input type="text" class='form-control' name="cus_customer_name" placeholder='Nome do cliente' value="{{ old('cus_customer_name', $customer_data->name )}}"/>
				</div>
				<div class="form-group">
					<label for='cus_customer_email'>Email:</label>
					<input type="email" class='form-control' name="cus_customer_email" placeholder='Email do cliente' value="{{ old('cus_customer_email', $customer_data->email)}}"/>
				</div>			
				<div class="form-group">
					<label for='cus_customer_phone_prefix'>Telefone:</label>
					<div class="row">
						<div class='col-md-2'>
							<input type="text" class='form-control as_mask_phone_prefix' name="cus_customer_phone_prefix" placeholder='99' value="{{ old('cus_customer_phone_prefix', $customer_data->phone_prefix)}}"/>					
						</div>
						<div class='col-md-4'>
							<input type="text" class='form-control as_mask_phone_number' name="cus_customer_phone_number" placeholder='9-9999-9999' value="{{ old('cus_customer_phone_number', $customer_data->phone_number)}}"/>					
						</div>					
					</div>
				</div>
				<div class="form-group">
					<div class="row">
						<div class='col-md-3'>
							<div class="form-group">
							<label for='cus_customer_person_type'>Tipo de pessoa:</label>
							<select class='form-control' name='cus_customer_person_type' id='cus_customer_person_type'>
									<option value='fis'>Pessoa física</option>
									<option value='jur' @if(old('cus_customer_person_type',$customer_data->person_type)=='jur') SELECTED @endif>Pessoa jurídica</option>							
								</select>					
							</div>					
						</div>			
						<div class='col-md-3' id="cus_customer_cpf_div">
							<div class="form-group">
							<label for='cus_customer_cpf'>CPF:</label>
							<input type="text" name="cus_customer_cpf" id="cus_customer_cpf" class='form-control as_mask_cpf' value="{{ old('cus_customer_cpf',$customer_data->cpf) }}">
							</div>
						</div>
						<div class='col-md-3' id="cus_customer_cnpj_div">
							<div class="form-group">
							<label for='cus_customer_cnpj'>CNPJ:</label>
							<input type="text" name="cus_customer_cnpj" id="cus_customer_cnpj" class='form-control as_mask_cnpj' value="{{ old('cus_customer_cnpj',$customer_data->cnpj) }}">
							</div>
						</div>							
					</div>
				</div>				
			</div>
		</div>
	</div>	
</div>
<div>
	<h3>Endereço:</h3>
</div>
<div class=''>
	<div class="row">		
		<div class='col-md-8'>
			<div class="box-content">
				
				
				<div class='row'>
					<div class='col-md-4'>		
						<div class="form-group">
							<label for='cus_customer_address_zip_code'>CEP:</label>
							<div class="input-group">						
								<input type="text" class="form-control as_mask_zip_code" name='cus_customer_address_zip_code' id='cus_customer_address_zip_code' placeholder="00000-000" value="{{ old('cus_customer_address_zip_code', $customer_data->zip_code) }}">
								<span class="input-group-btn">
									<button class="btn btn-primary" id="btn_search_zip" type="button">Pesquisar</button>
								</span>
							</div>							
						</div>
					</div>
				</div>
				<div class='row'>
					<div class='col-md-10'>
						<div class="form-group">
							<label for='cus_customer_address_street'>Logradouro:</label>
							<input type="text" class='form-control' name="cus_customer_address_street" id="cus_customer_address_street" placeholder="Endereço" value="{{ old('cus_customer_address_street', $customer_data->street) }} ">
						</div>					
					</div>
					<div class='col-md-2'>
						<div class="form-group">
							<label for='cus_customer_address_number'>Nº:</label>
							<input type="text" class='form-control' name="cus_customer_address_number" id="cus_customer_address_number" placeholder="" value=" {{ old('cus_customer_address_number', $customer_data->number) }} ">
						</div>					
					</div>				
				</div>
				<div class="form-group">
					<label for='cus_customer_address_others'>Complemento:</label>
					<input type="text" class='form-control' name="cus_customer_address_others" id="cus_customer_address_others" placeholder="Complemento" value=" {{ old('cus_customer_address_others', $customer_data->others) }} ">
				</div>
				<div class="form-group">
					<label for='cus_customer_address_district'>Bairro:</label>
					<input type="text" class='form-control' name="cus_customer_address_district" id="cus_customer_address_district" placeholder="Complemento" value=" {{ old('cus_customer_address_district', $customer_data->district) }} ">
				</div>				
				<div class='row'>
					<div class='col-md-10'>
						<div class="form-group">
							<label for='cus_customer_address_city'>Cidade:</label>
							<input type="text" class='form-control' name="cus_customer_address_city" id="cus_customer_address_city" placeholder="Cidade" value=" {{ old('cus_customer_address_city', $customer_data->city) }} ">
						</div>					
					</div>
					<div class='col-md-2'>
						<div class="form-group">
							<label for='cus_customer_address_state'>UF:</label>
							<input type="text" class='form-control' name="cus_customer_address_state" id="cus_customer_address_state" placeholder="" value="{{ old('cus_customer_address_state', $customer_data->state) }}">
						</div>					
					</div>				
				</div>	
			</div>	
		</div>			
	</div>	
</div>
<br/><input type="submit" class='btn btn-primary' name="" value='Gravar'/>