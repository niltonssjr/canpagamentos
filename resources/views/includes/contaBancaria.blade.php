			<div class="row">
				<div class='col-md-4'>
					<label for='subconta_bank'>Banco:</label>
					<select class='form-control' name='subconta_bank'>
						<option value='341'>Itaú</option>
						<option value='237' @if(old('subconta_bank')=='Bradesco') SELECTED @endif>Bradesco</option>
						<option value='104' @if(old('subconta_bank')=='Caixa Econômica') SELECTED @endif>Caixa Econômica Federal</option>
						<option value='001' @if(old('subconta_bank')=='Banco do Brasil') SELECTED @endif>Banco do Brasil</option>
						<option value='033' @if(old('subconta_bank')=='Santander') SELECTED @endif>Santander</option>
						<option value='041' @if(old('subconta_bank')=='Banrisul') SELECTED @endif>Banrisul</option>
						<option value='748' @if(old('subconta_bank')=='Sicredi') SELECTED @endif>Sicredi</option>
						<option value='756' @if(old('subconta_bank')=='Sicoob') SELECTED @endif>Sicoob</option>
						<option value='077' @if(old('subconta_bank')=='Inter') SELECTED @endif>Inter</option>
						<option value='070' @if(old('subconta_bank')=='BRB') SELECTED @endif>BRB</option>
					</select>
				</div>	
				<div class='col-md-2'>
					<label for='subconta_bank_type'>Tipo:</label>
					<select class='form-control' name='subconta_bank_type'>
						<option value='Corrente'>Corrente</option>
						<option value='Poupança' @if(old('subconta_bank_type')=='Poupança') SELECTED @endif>Poupança</option>
					</select>
				</div>				
				<div class='col-md-2'>
					<label for='subconta_bank_ag'>Agência:</label>
					<input type="text" class='form-control' name="subconta_bank_ag" maxlength=6 value="{{ old('subconta_bank_ag') }}"/>
				</div>
				<div class='col-md-4'>
					<label for='subconta_bank_cc'>Conta:</label>
					<input type="text" class='form-control' name="subconta_bank_cc" maxlength=12 value="{{ old('subconta_bank_cc') }}"/>
				</div>

			</div>