<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UserAdditionalData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('customers',function(Blueprint $table){
            $table->integer('comm_cents')->default(0);
            $table->float('comm_perc')->default(0);
            $table->string('cpf_cnpj');
            $table->string('business_descr');
            $table->string('company_name');
            $table->string('zip_code');
            $table->string('address_street');
            $table->string('address_number');
            $table->string('address_city');
            $table->string('address_state');
            $table->string('phone');
            $table->string('responsible_name');
            $table->string('responsible_cpf');
            $table->integer('account_agency')->unsigned();
            $table->integer('account_number')->unsigned();
            $table->string('account_digit');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
