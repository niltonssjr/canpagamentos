<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CustomerNullableColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('customers',function(Blueprint $table){
            $table->string('cpf_cnpj')->nullable()->change();
            $table->string('business_descr')->nullable()->change();
            $table->string('company_name')->nullable()->change();
            $table->string('zip_code')->nullable()->change();
            $table->string('address_street')->nullable()->change();
            $table->string('address_number')->nullable()->change();
            $table->string('address_city')->nullable()->change();
            $table->string('address_state')->nullable()->change();
            $table->string('phone')->nullable()->change();
            $table->string('responsible_name')->nullable()->change();
            $table->string('responsible_cpf')->nullable()->change();
            $table->string('account_digit')->nullable()->change(); 
            $table->string('phone_prefix')->nullable()->change(); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
