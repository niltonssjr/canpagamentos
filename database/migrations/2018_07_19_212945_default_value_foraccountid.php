<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DefaultValueForaccountid extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('customers',function(Blueprint $table){
            $table->string('account_id')->nullable()->change();
            $table->string('name')->nullable()->change();
            $table->string('live_api_token')->nullable()->change();
            $table->string('test_api_token')->nullable()->change();
            $table->string('user_token')->nullable()->change();
            $table->string('oper_mode')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
