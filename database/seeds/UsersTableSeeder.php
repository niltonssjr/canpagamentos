<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
        		'name'		=> 'Cantarino',
        		'email'		=> 'jackson@cantarino.xyz',
        		'password'	=> bcrypt('Cantarino2018'),
        		'level'		=> 'Admin',
        		'status'	=> 'Active',
        		'oper_mode'	=> 'test',
                'paguecan_api_token' => ""
        	]);        

        $new_user = User::create([
    		'name'		=> 'Augesystems',
    		'email'		=> 'augesystems@gmail.com',
    		'password'	=> bcrypt('auge@can2018'),
    		'level'		=> 'Admin',
    		'status'	=> 'Active',
    		'oper_mode'	=> 'test',
            'paguecan_api_token' => ""
    	]);         

        $new_user = User::create([
            'name'      => 'Nilton Júnior',
            'email'     => 'niltonbrisa@gmail.com',
            'password'  => bcrypt('niltonbrisa'),
            'level'     => 'customer',
            'status'    => 'Active',
            'oper_mode' => 'test',
            'paguecan_api_token' => ""
        ]);            

    }

}

