<?php

use Illuminate\Database\Seeder;
use App\Models\Customer;

class CustomersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Customer::create([
                'user_id'           => 1,
                'created_by'        => 1,
                'name'              => 'Cantarino',
                'oper_mode'         => 'Test',
                'comm_cents'        => 0,
                'comm_perc'         => 0,
                'cpf_cnpj'          => '27557949000143',
                'business_descr'    => 'serviços de tecnologia',
                'company_name'      => 'Cantarino tecnologia',
                'zip_code'          => '22735130',
                'address_street'    => 'Rua Henriqueta',
                'address_number'    => '64',
                'address_city'      => 'Rio de Janeiro',
                'address_state'     => "RJ",
                'phone'             => "02112341234",
                'responsible_name'  => 'Jackson',
                'responsible_cpf'   => '67128185321',
                'account_agency'    => '1234',
                'account_number'    => '123456',
                'account_digit'     => '1',
                'test_api_token'	=> '749d388949477a247f7c161d2b0d0864',
                'live_api_token'	=> "e731cbf504a89fe999409c5c33f8704b",
                'user_token'        => "",
                'account_id'        => "4ACEE75D1FEA492F808D52C479F9E8CA",
                "parent_account_id" => ""
        ]);

        Customer::create([
                'user_id'           => 2,
                'created_by'        => 1,
                'name'              => 'Augesystems',
                'oper_mode'         => 'Test',
                'comm_cents'        => 0,
                'comm_perc'         => 0,
                'cpf_cnpj'          => '28094314000110',
                'business_descr'    => 'serviços de tecnologia',
                'company_name'      => 'Augesystems',
                'zip_code'          => '02969130',
                'address_street'    => 'Rua Henriqueta',
                'address_number'    => '64',
                'address_city'      => 'Rio de Janeiro',
                'address_state'     => "RJ",
                'phone'             => "02112341234",
                'responsible_name'  => 'Nilton',
                'responsible_cpf'   => '67128185321',
                'account_agency'    => '1234',
                'account_number'    => '123456',
                'account_digit'     => '1',
                'test_api_token'	=> '749d388949477a247f7c161d2b0d0864',
                'live_api_token'	=> "e731cbf504a89fe999409c5c33f8704b",
                'user_token'        => "",
                'account_id'        => "4ACEE75D1FEA492F808D52C479F9E8CA",
                "parent_account_id" => ""
        ]);

        Customer::create([
                'user_id'           => 3,
                'created_by'        => 1,
                'name'              => 'Nilton Júnior',
                'oper_mode'         => 'Test',
                'comm_cents'        => 0,
                'comm_perc'         => 0,
                'cpf_cnpj'          => '28094314000110',
                'business_descr'    => 'serviços de tecnologia',
                'company_name'      => 'Nilton Júnior',
                'zip_code'          => '22735130',
                'address_street'    => 'Rua Henriqueta',
                'address_number'    => '64',
                'address_city'      => 'Rio de Janeiro',
                'address_state'     => "RJ",
                'phone'             => "02112341234",
                'responsible_name'  => 'Nilton',
                'responsible_cpf'   => '67128185321',
                'account_agency'    => '1234',
                'account_number'    => '123456',
                'account_digit'     => '1',
                'account_id'		=> '32AA077C36244839B28466726B2640BF',
                'test_api_token'	=> 'c950e3bc936f3b70b829338b3fe4970c',
                'live_api_token'	=> "a462583d2833f9960147686481c191c6",
                'user_token'        => "e27c7900bfeb884ccfe90346855b96ca",
                "parent_account_id" => ""
        ]);                
    }
}
     